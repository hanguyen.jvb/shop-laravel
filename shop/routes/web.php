<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//backend
 Route::group(['prefix' => 'admin'], function () {
     Route::get('/login', 'backend\LoginController@list')->name('login.list')->middleware('CheckLogout');
     Route::post('/login', 'backend\LoginController@process')->name('login.process');
 });

Route::group(['prefix' => 'home', 'middleware' => 'CheckLogin'], function () {
    Route::get('', 'backend\HomeController@list')->name('backend.home.list');
    // Route::get('', 'backend\HomeController@listCategories')->name('backend.home.listCategories');
    Route::get('logout', 'backend\HomeController@logout')->name('backend.home.logout');
    Route::get('setting/{id}', 'backend\HomeController@setAcount')->name('backend.home.setting');
    Route::post('setting/{id}', 'backend\HomeController@updateAcount');
    Route::get('admin/{id}', 'backend\HomeController@listProfile')->name('backend.home.admin');

    Route::group(['prefix' => 'users'], function () {
        Route::get('', 'backend\UserController@list')->name('backend.user.list');
        Route::get('add', 'backend\UserController@add')->name('backend.user.add');
        Route::post('add', 'backend\UserController@store')->name('backend.user.store');
        Route::get('edit/{id}', 'backend\UserController@edit')->name('backend.user.edit');
        Route::post('edit/{id}', 'backend\UserController@update')->name('backend.user.update');
        Route::post('delete', 'backend\UserController@destroy')->name('backend.user.delete');
        Route::post('search-user', 'backend\UserController@searchUser')->name('backend.user.searchUser');
        Route::get('search-all/{keyName}', 'backend\UserController@search')->name('backend.user.search');
        Route::get('set-admin/{id}', 'backend\UserController@setIsAdmin')->name('backend.user.setIsAdmin');
        Route::get('remove-admin/{id}', 'backend\UserController@removeAdmin')->name('backend.user.removeAdmin');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'backend\CategoryController@list')->name('backend.category.list');
        Route::post('', 'backend\CategoryController@store')->name('backend.category.store');
    });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/{id}', 'backend\ProductController@list')->name('backend.product.list');
        Route::get('/{id}/add', 'backend\ProductController@add')->name('backend.product.add');
        Route::post('/{id}/add', 'backend\ProductController@store')->name('backend.product.store');
        Route::get('/{idCategory}/edit/{idProduct}', 'backend\ProductController@edit')->name('backend.product.edit');
        Route::post('/{idCategory}/edit/{idProduct}', 'backend\ProductController@update')->name('backend.product.update');
        Route::post('/{idCategory}/delete', 'backend\ProductController@destroy')->name('backend.product.delete');
        Route::get('/search-all-product/{keyProduct}', 'backend\ProductController@searchAllProduct')->name('backend.product.searchAllProduct');
        Route::post('/search-product', 'backend\ProductController@searchProduct')->name('backend.product.searchProduct');
        Route::get('/detail-product/{id}', 'backend\ProductController@detailProduct')->name('backend.product.detailProduct');
        Route::post('/{idCategory}/upload-img/{idProduct}', 'backend\ProductController@uploadMultipleImg')->name('backend.product.uploadMultipleImg');
        Route::get('/{idCategory}/product-images/{idProduct}', 'backend\ProductController@showProductImages')->name('backend.product.showProductImages');
        Route::post('/add-attr', 'backend\ProductController@addAttribute')->name('backend.product.attribute.add');
        Route::post('/add-value', 'backend\ProductController@addValue')->name('backend.product.attribute.value.add');
    });

    Route::group(['prefix' => 'image'], function () {
        Route::post('/{idProduct}/edit/{idImage}', 'backend\ProductController@updateImages')->name('backend.product.image.updateImages');
        Route::post('/{idProduct}/delete', 'backend\ProductController@destroyImages')->name('backend.product.image.deleteImages');
    });

    Route:Route::group(['prefix' => 'order'], function () {
        Route::get('', 'backend\OrderController@list')->name('backend.order.list');
        Route::get('/process/{idOrder}', 'backend\OrderController@process')->name('backend.order.process');
        Route::get('/list-processed', 'backend\OrderController@listProcessed')->name('backend.order.listProcessed');
        Route::get('/processed/{idOrder}', 'backend\OrderController@processed')->name('backend.order.processed');
        Route::post('/delete', 'backend\OrderController@destroy')->name('backend.order.delete');
        Route::post('/delete-processed', 'backend\OrderController@destroyProcessed')->name('backend.processed.destroyProcessed');
        Route::get('/complete-ordering/{idOrder}', 'backend\OrderController@completeOrdering')->name('backend.order.completeOrdering');
        Route::get('/completed', 'backend\OrderController@completedOrder')->name('backend.order.completedOrder');
        Route::post('/delete-completed', 'backend\OrderController@destroyCompleteOrder')->name('backend.order.destroyCompleteOrder');
        Route::post('/search-order', 'backend\OrderController@searchOrder')->name('backend.order.searchOrder');
        Route::get('/search-all-order/{orderName}', 'backend\OrderController@searchAllOrder')->name('backend.order.searchAllOrder');
        Route::get('/cancel-order/{orderId}', 'backend\OrderController@cancelProcess')->name('backend.order.cancelProcess');
    });

    Route::group(['prefix' => 'attribute'], function () {
        Route::get('', 'backend\ProductController@listAttribute')->name('backend.product.attribute.list');
    });
});


//frontend
Route::group(['prefix' => '/'], function () {
    //home
    Route::get('', 'frontend\HomeController@list')->name('frontend.home.list');
    Route::post('search-product', 'frontend\HomeController@search')->name('frontend.home.search');
    Route::get('search-all/{keyName}', 'frontend\HomeController@searchAll')->name('frontend.home.searchAll');
    Route::get('flash-sale', 'frontend\HomeController@saleAll')->name('frontend.home.saleAll');

    //product
    Route::get('{productSlug}.htm', 'frontend\ProductController@detail')->name('frontend.home.product.detail');
    Route::group(['prefix' => 'product'], function () {
        Route::get('/sort-z-a/{product}/{id}', 'frontend\ProductController@sortDown')->name('frontend.product.sortDown');
        Route::get('/sort-a-z/{product}/{id}', 'frontend\ProductController@sortUp')->name('frontend.product.sortUp');
        Route::get('/price-increase/{product}/{id}', 'frontend\ProductController@priceIncrease')->name('frontend.product.priceIncrease');
        Route::get('/price-decrease/{product}/{id}', 'frontend\ProductController@priceDecrease')->name('frontend.product.priceDecrease');
        Route::get('/latest-product/{product}/{id}', 'frontend\ProductController@latestProduct')->name('frontend.product.latestProduct');
    });

    //productByCategory
    Route::group(['prefix' => 'category'], function () {
        Route::get('/{category}/{id}', 'frontend\ProductController@listProductCategory')->name('frontend.home.category.ProductCategory');
        Route::get('sub/{category}/{id}', 'frontend\ProductController@listProductSubCategory')->name('frontend.home.category.ProductSubCategory');
    });

    //cart
    Route::group(['prefix' => 'cart'], function () {
        Route::get('/list', 'frontend\CartController@list')->name('home.cart.list');
        Route::post('/add', 'frontend\CartController@create')->name('home.cart.add');
        Route::get('remove/{rowId}', 'frontend\CartController@remove')->name('home.cart.remove');
        Route::get('update-cart/{rowId}/{qty}', 'frontend\CartController@updateCart')->name('home.cart.updateCart');
    });

    //checkout
    Route::group(['prefix' => 'checkout', 'middleware' => 'CheckCart'], function () {
        Route::get('', 'frontend\CheckoutController@create')->name('home.cart.checkout.create');
        Route::post('', 'frontend\CheckoutController@store')->name('home.cart.checkout.store');
        Route::get('complete', 'frontend\CheckoutController@complete')->name('home.cart.checkout.complete');
    });

    //Acount
    Route::group(['prefix' => 'acount'], function () {
        Route::get('/login-user', 'frontend\AcountController@loginView')->name('home.acount.loginView');
        Route::post('/login-user', 'frontend\AcountController@dataLogin')->name('home.acount.dataLogin');
        Route::get('/register', 'frontend\AcountController@registerView')->name('home.acount.registerView');
        Route::post('/register', 'frontend\AcountController@register')->name('home.acount.register');
        Route::get('/profile', 'frontend\AcountController@profileView')->name('home.acount.profileView');
        Route::get('/logout-user', 'frontend\AcountController@logout')->name('home.acount.logout');
//        Route::post('', 'frontend\CheckoutController@store')->name('home.cart.checkout.store');
//        Route::get('complete', 'frontend\CheckoutController@complete')->name('home.cart.checkout.complete');
    });

    Route::group(['prefix' => 'about'], function () {
        Route::get('/', 'frontend\HomeController@about')->name('home.about.list');
    });

    Route::group(['prefix' => 'store'], function () {
        Route::get('/', 'frontend\HomeController@infoShop')->name('home.store.infoShop');
    });
});

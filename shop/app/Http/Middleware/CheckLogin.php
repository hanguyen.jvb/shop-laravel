<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
//            dd(Auth::user());
            if (Auth::guard('client')->user()) {
//                dd(Auth::user());
                return redirect('/');
            } else {
                return $next($request);
            }
        } else {
            return redirect('admin/login');
        }
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class addUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileToUpload' => 'required|mimes:jpeg,jpg,png|max:5120',
            'fullname' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|regex:/^[0-9]{10,11}/',
            'password' => 'required|regex:/^[0-9a-zA-Z]{6}/|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required',
            'address' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'fileToUpload.mimes' => 'Just only upload files(png, jpg, jpeg)',
            'fileToUpload.max' => 'Maximum file size is 5MB',
            'email.unique' => 'Email already exists',
            'email.email' => 'Email must be in proper format',
            'phone.required' => 'Please enter phone number',
            'phone.regex' => 'phone numbers from 10 to 11 numbers',
            'phone.unique' => 'Phone numbers cannot be duplicated',
            'password.required' => 'Please enter password',
            'password.regex' => 'Password from 6 characters',
            'confirm_password.required' => 'Please confirm the password',
            'address.required' => 'Please enter address',
        ];
    }
}

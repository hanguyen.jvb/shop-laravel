<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAcount extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fullname' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|regex:/^[0-9a-zA-Z]{8}/|required_with:password_confirm|same:password_confirm',
            'confirm_password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Vui lòng nhập họ tên',
            'username.required' => 'Vui lòng nhập tên đăng nhập',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Email chưa đúng định dạng',
            'email.unique' => 'Email đã tồn tại',
            'password.required' => 'Vui lòng nhập password',
            'password.regex' => 'Mật khẩu từ 8 ký tự',
            'password_confirm.required' => 'Vui lòng xác nhận mật khẩu',
            'password.confirmed' => 'Mật khẩu xác nhận không trùng khớp',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileToUpload' => 'required|mimes:jpeg,jpg,png|max:5120',
            'fullname' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^[0-9]{10,11}/',
            'password' => 'required|regex:/^[0-9a-zA-Z]{6}/|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'required',
            'address' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'fileToUpload.mimes' => 'Just only upload files(png, jpg, jpeg)',
            'fileToUpload.required' => 'Please update avatar',
            'fileToUpload.max'  => 'Maximum file size is 5MB',
            'fullname.required' => 'Please update fullname',
            'username.required' => 'Please update username',
            'phone.required' => 'Please update your phone number',
            'phone.regex' => 'phone numbers from 10 to 11 numbers',
            'password.required' => 'Please update your password',
            'password.regex' => 'Password from 6 characters',
            'confirm_password.required' => 'Please confirm the password',
            'password.confirmed' => 'Confirmation password does not match',
            'address.required' => 'Please update your address',
        ];
    }
}

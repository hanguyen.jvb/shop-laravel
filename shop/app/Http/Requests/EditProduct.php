<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileUpload' => 'required|mimes:jpeg,jpg,png|max:5120',
            'name' => 'required',
            'code' => 'required',
            'price' => 'required|regex:/^[0-9]/',
            'promotion' => 'required|regex:/^[0-9]/',
            'description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fileToUpload.mimes' => 'Just only upload files(png, jpg, jpeg)',
            'fileToUpload.max' => 'Maximum file size is 5MB',
        ];
    }
}

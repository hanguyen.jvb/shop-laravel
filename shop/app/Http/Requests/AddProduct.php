<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fileToUpload' => 'required|mimes:jpeg,jpg,png|max:5120',
//            'uploadMultipleImage' => 'required|mimes:jpeg,jpg,png',
            'name' => 'required',
            'code' => 'required',
            'price' => 'required|regex:/^[0-9]/',
            'promotion' => 'required|regex:/^[0-9]/',
            'description' => 'required',
//            'attr' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'fileToUpload.mimes' => 'Just only upload files(png, jpg, jpeg)',
            'fileToUpload.max' => 'Maximum file size is 5MB',
//            'uploadMultipleImage.mimes' => 'Just only upload files(png, jpg, jpeg)',
        ];
    }
}

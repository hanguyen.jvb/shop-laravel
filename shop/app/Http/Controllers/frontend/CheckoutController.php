<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Requests\checkoutOrder;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\CustomerOrder;
use App\Models\OrderAttrs;
use Cart;
// use Illuminate\Support\Facades\Mail;
// use App\Mail\ShoppingMail;
use Mail;

class CheckoutController extends Controller
{
    public function create()
    {
//        dd(Cart::Content());
    	$data['cartsCheckout'] = Cart::Content();
        return view('frontend.checkout.add', $data);
    }

    public function store(checkoutOrder $request)
    {
        $data = $request->all();
        $data['fullname'] = $request->fullname;
        $data['phone'] = $request->phone;
        $data['description'] = $request->description;
        $data['state'] = 0;
        $customer = Customer::create($data);
        $customerDetail = [];
        $customerDetails = [];

    	foreach (Cart::Content() as $key => $cartOrder) {
            $customerDetail['name'] = $cartOrder->name;
            $customerDetail['price'] = $cartOrder->price * ((100 - $cartOrder->options->promotion) / 100);
            $customerDetail['quantity'] = $cartOrder->qty;
            $customerDetail['img'] = $cartOrder->options->img;
            $customerDetail['customer_id'] = $customer->id;
            $customerDetails[$key] = CustomerOrder::create($customerDetail);
    	}

        Mail::send('frontend.mail.shopping', ['customer' => $customer], function ($m) use ($customer) {
            $m->to($customer->email);
            $m->from('viethanguyen510@gmail.com');
            $m->subject('email order');
        });
        return redirect()->route('home.cart.checkout.complete');
    }

    public function complete()
    {
//        die('1');
        Cart::destroy();
        $customerOrder = Customer::latest('id')->first();
//        dd($customerOrder);
        return view('frontend.checkout.complete', ['customerOrder' => $customerOrder]);

    }
}

<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function list()
    {
//        dd(Carbon::now());
        $saleProducts = Product::where([
            ['promotion', '<>', 0],
//            ['end_sale', '>=', Carbon::now()]
        ])
//            ->skip(0)
//            ->take(6)
            ->take(6)->get();
        $listCategories = Category::where([
            ['parent', '=', '0'],
        ])->get();
        $viewData = [
            'saleProducts' => $saleProducts,
            'listCategories' => $listCategories,
        ];
        return view('frontend.home', $viewData);
    }

    public function search(Request $request)
    {
        if ($request->get('keySearch')) {
            $keySearch = $request->get('keySearch');
            $products = Product::where('name', 'LIKE', '%' . $keySearch . '%')
                ->where(function ($query) {
                    return $query->where('featured', '=', 1);
                })
                ->orderBy('id', 'desc')
                ->take(4)
                ->get();
            $output = '<ul class="dropdown-menu" style="display: block; margin-left: 360px; margin-top: -10px"><a href="/search-all/' . $keySearch . '" style="margin-left: 6px; color: black">Hiển thị tất cả kết quả cho "<span style="color: #ff0000">' . $keySearch . '</span>"</a>';
            foreach ($products as $product) {
                $output .= '<li style="border-bottom: 1px solid #DCDCDC;">
                                <a href="/' . $product->slug . '-' . $product->id . '.htm" style="color: black"><img src="img/' . $product->img . '" width="100" height="90"> <span style="font-size: 15px">' . $product->name . '</span></a>
                            </li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }

    public function searchAll($keyName)
    {
        $categories = Category::all();
        $resultSearchAll = Product::where('name', 'LIKE', '%' . $keyName . '%')->get();
//        dd($resultSearchAll);
        return view('frontend.search.searchAll', [
            'resultSearchAll' => $resultSearchAll, 'keyName' => $keyName, 'categories' => $categories
        ]);
    }

    public function saleAll()
    {
        $listSaleAll = Product::where([
            ['promotion', '<>', 0],
        ])
            ->get();
        $categories = Category::all();
        return view('frontend.product.saleProductAll', ['listSaleAll' => $listSaleAll, 'categories' => $categories]);
    }

    public function about()
    {
        return view('frontend.about.list');
    }

    public function infoShop()
    {
        return view('frontend.about.shop');
    }
}

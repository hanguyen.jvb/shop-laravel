<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Cart;

class CartController extends Controller
{
    public function list()
    {
        $data['carts'] = Cart::Content();

        return view('frontend.cart.list', $data);
    }

    public function create(Request $request)
    {
        $idProduct = Product::find($request->product_id);
//        dd($idProduct->id);
        Cart::add([
        'id' => $idProduct->id,
        'name' => $idProduct->name,
        'qty' => $request->qty,
        'price' => $idProduct->price,
        'weight' => 0,
        'options' => ['promotion' => $idProduct->promotion, 'code' => $idProduct->code, 'img' => $idProduct->img, 'attr' => $request->attr],
        ]);

        return redirect('cart/list');
    }

    public function remove($rowId)
    {
        Cart::remove($rowId);

        return redirect('cart/list');
    }

    public function updateCart($rowId, $qty)
    {
        $updateCart = Cart::update($rowId, $qty);

        return response()->json(['updateCart' => $updateCart], 200);
    }
}

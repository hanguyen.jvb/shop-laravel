<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterAcount;
use App\Http\Requests\LoginClient;
use App\User;
use App\Client;
use Illuminate\Support\Facades\Auth;

class AcountController extends Controller
{
    public function loginView()
    {
        return view('frontend.acount.loginView');
    }

    public function dataLogin(Request $request)
    {
//        dd($request->all());
        $username = $request->username;
        $password = $request->password;
//        dd($username);

        if (Auth::guard('client')->attempt(['username' => $username, 'password' => $password])) {
//            dd(Auth::guard('client'));
            return redirect('/acount/profile');
        } else {
            return redirect()->back()->with('notifyLoginClient', 'Thông tin đăng nhập không đúng');
        }
    }

    public function registerView()
    {
        return view('frontend.acount.registerView');
    }

    public function register(RegisterAcount $request)
    {
//        dd($request->all());;
        $registerUser = new Client();
        $registerUser->fullname = $request->fullname;
        $registerUser->username = $request->username;
        $registerUser->email = $request->email;
        $registerUser->password = bcrypt($request->password);
//        $registerUser->level = 3;
        $registerUser->save();

        return redirect('acount/login-user');
    }

    public function profileView()
    {
        return view('frontend.acount.profile');
    }

    public function logout()
    {
        Auth::guard('client')->logout();
        return redirect('/');
    }
}

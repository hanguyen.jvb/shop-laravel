<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Attribute;
use App\Models\ValuesAttribute;

class ProductController extends Controller
{
    public function detail($productSlug)
    {
        $arrProductSlug = explode("-", $productSlug);
        $idProduct = array_pop($arrProductSlug);
        $data['productDetail'] = Product::find($idProduct);
        $data['valuesAttr'] = ValuesAttribute::where('attribute_id', 1)->get();

        return view('frontend.product.detail', $data);
    }

    public function listProductCategory($catName, $id)
    {
        $categoryId = Category::findOrFail($id);
        $category = Category::where('parent', $categoryId->id)->get();
        $categories = Category::all();

        return view('frontend.product.productCategory', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }

    public function listProductSubCategory($name, $id)
    {
        $categories = Category::all();
        $categoryId = Category::findOrFail($id);
        $products = Product::where('category_id', $id)->get();

        return view('frontend.product.productSubcategory', [
            'categoryId' => $categoryId, 'products' => $products, 'categories' => $categories
        ]);
    }

    public function sortDown($productName, $id)
    {
        $categoryId = Category::findOrFail($id);
//        dd($categoryId->id);
        $category = Category::where('parent', $categoryId->id)->get();

//        $products = Product::where('category_id', $categoryId)->get();
        $categories = Category::all();

        return view('frontend.product.productCategorySortDown', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }

    public function sortUp($productName, $id)
    {
        $categoryId = Category::findOrFail($id);
//        dd($categoryId->id);
        $category = Category::where('parent', $categoryId->id)->get();

//        $products = Product::where('category_id', $categoryId)->get();
        $categories = Category::all();

        return view('frontend.product.productCategorySortUp', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }

    public function priceIncrease($productName, $id)
    {
        $categoryId = Category::findOrFail($id);
//        dd($categoryId->id);
        $category = Category::where('parent', $categoryId->id)->get();

//        $products = Product::where('category_id', $categoryId)->get();
        $categories = Category::all();

        return view('frontend.product.productCategoryIncrease', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }

    public function priceDecrease($productName, $id)
    {
        $categoryId = Category::findOrFail($id);
//        dd($categoryId->id);
        $category = Category::where('parent', $categoryId->id)->get();

//        $products = Product::where('category_id', $categoryId)->get();
        $categories = Category::all();

        return view('frontend.product.productCategoryDecrease', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }

    public function latestProduct($productName, $id)
    {
        $categoryId = Category::findOrFail($id);
//        dd($categoryId->id);
        $category = Category::where('parent', $categoryId->id)->get();

//        $products = Product::where('category_id', $categoryId)->get();
        $categories = Category::all();

        return view('frontend.product.productCategoryLatest', [
            'category' => $category, 'categoryId' => $categoryId, 'categories' => $categories
        ]);
    }
}

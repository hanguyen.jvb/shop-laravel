<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\AddCategory;

class CategoryController extends Controller
{
    public function list()
    {
        $data['categories'] = Category::all();
        return view('backend.category.list', $data);
    }

    public function store(AddCategory $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->parent = $request->parent;
        $category->save();

        return redirect()->back()->with('notify-category', 'Add successfully');
    }
}

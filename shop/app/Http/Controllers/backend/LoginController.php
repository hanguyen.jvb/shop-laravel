<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Login;

class LoginController extends Controller
{
    public function list()
    {
        return view('backend.login.list');
    }

    public function process(Login $request)
    {
        $username = $request->username;
        $password = $request->password;

        if (Auth::guard('web')->attempt(['username' => $username, 'password' => $password])) {
//            dd(Auth::guard('web')->user());
            return redirect('home');
        } else {
            return redirect()->back()->with('notification', 'Username or password are incorrect')->withInput();
        }
    }
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\productOrder;
use  App\Models\Customer;
use  App\Models\CustomerOrder;
use Mail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderController extends Controller
{
    public function list()
    {
        $data['orders'] = Customer::where('state', 0)->orderBy('id', 'desc')->paginate(5);
        return view('backend.order.list', $data);
    }

    public function process($idOrder)
    {
        try {
            $orderId = Customer::findOrFail($idOrder);
            $productOrders = CustomerOrder::where('customer_id', $idOrder)->get();

            return view('backend.order.process', ['orderId' => $orderId, 'productOrders' => $productOrders]);
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    public function processed($idOrder)
    {
        try {
            $orderId = Customer::findOrFail($idOrder);
//            dd($orderId);
            $data['stateOrder'] = Customer::where('id', $idOrder)->update(['state' => 1]);
            $data['orderShipping'] = CustomerOrder::where('customer_id', $idOrder)->get();
            Mail::send('frontend.mail.orderShipping', ['orderId' => $orderId], function ($m) use ($orderId) {
                $m->to($orderId->email);
                $m->from('viethanguyen510@gmail.com');
                $m->subject('order shipping');
            });

            return redirect('/home/order/list-processed')->with('notification', $orderId->fullname."'s order is shipping");
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    public function listProcessed()
    {
        $data['orderProcessed'] = Customer::where('state', 1)->orderBy('id', 'desc')->paginate(3);
        return view('backend.order.orderProcessed', $data);
    }

    public function completeOrdering($idOrder)
    {
        try {
            $orderId = Customer::findOrFail($idOrder);
//            dd($orderId);
            $data['stateOrder'] = Customer::where('id', $idOrder)->update(['state' => 2]);
            $data['orderShipping'] = CustomerOrder::where('customer_id', $idOrder)->get();

            return redirect('/home/order/completed')->with('notification', $orderId->fullname."'s order is complete");
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    public function completedOrder()
    {
        $data['completedOrder'] = Customer::where('state', 2)->orderBy('id', 'desc')->paginate(3);
        return view('backend.order.completedOrder', $data);
    }

    public function destroy(Request $request)
    {

        $idCheckDelete = $request->deleteOrder;
        if ($idCheckDelete == "") {
            return redirect('home/order')->with('error-warning', 'Select order you wanna delete');
        } else {
            foreach ($idCheckDelete as $idCheck) {
                Customer::where('id', $idCheck)->delete();
            }
        }

        return redirect('home/order')->with('notification', 'Delete order successfully');
    }

    public function destroyProcessed(Request $request)
    {
        $idCheckDelete = $request->deleteProcessed;
        if ($idCheckDelete == "") {
            return redirect('/home/order/list-processed')->with('error-warning', 'Select processed order you wanna delete');
        } else {
            foreach ($idCheckDelete as $idCheck) {
                Customer::where('id', $idCheck)->delete();
            }
        }

        return redirect('/home/order/list-processed')->with('notification', 'Delete order shipping successfully');
    }

    public function destroyCompleteOrder(Request $request)
    {
        $idCheckDelete = $request->deleteCompleteOrder;
        if ($idCheckDelete == "") {
            return redirect('/home/order/list-processed')->with('error-warning', 'Select order you wanna delete');
        } else {
            foreach ($idCheckDelete as $idCheck) {
                Customer::where('id', $idCheck)->delete();
            }
        }

        return redirect('/home/order/completed')->with('notification', 'Delete order successfully');
    }

    public function searchOrder(Request $request)
    {
        if ($request->get('keySearchOrder')) {
            $keySearch = $request->get('keySearchOrder');
//            dd($keySearch);
            $customers = Customer::where('fullname', 'LIKE', '%' . $keySearch . '%')
                ->orWhere('updated_at', 'LIKE', '%' . $keySearch . '%')
                ->orWhere('address', 'LIKE', '%' . $keySearch . '%')
                ->orWhere('email', 'LIKE', '%' . $keySearch . '%')
                ->orWhere('phone', 'LIKE', '%' . $keySearch . '%')
                ->orderBy('id', 'desc')
                ->take(4)
                ->get();
            $output = '<ul class="dropdown-menu" style="display: block; margin-left: -15px; width: 250px"><a href="/home/order/search-all-order/' . $keySearch . '" style="margin-left: 6px; color: black">Hiển thị tất cả kết quả cho "<span style="color: #ff0000">' . $keySearch . '</span>"</a>';
            foreach ($customers as $customer) {
                $output .= '<li style="border-bottom: 1px solid #DCDCDC;">
                                <a href="/home/order/process/' . $customer->id . '" style="color: black"><span style="font-size: 17px">' . $customer->fullname . '</span> <span class="label label-danger">ORDER</span></a>
                            </li>';
            }
            $output .= '</ul>';
            return $output;
        }
    }

    public function searchAllOrder($keyOrder)
    {
        $resultSearchAllOrder = Customer::where('fullname', 'LIKE', '%' . $keyOrder . '%')
            ->orWhere('address', 'LIKE', '%' . $keyOrder . '%')
            ->orWhere('updated_at', 'LIKE', '%' . $keyOrder . '%')
            ->orWhere('email', 'LIKE', '%' . $keyOrder . '%')
            ->orWhere('phone', 'LIKE', '%' . $keyOrder . '%')
            ->orderBy('id', 'desc')
            ->paginate(3);
//        dd($resultSearchAll);
        return view('backend.order.searchAllOrder', [
            'resultSearchAllOrder' => $resultSearchAllOrder, 'keyOrder' => $keyOrder
        ]);
    }

    public function cancelProcess($orderId)
    {
        $idOrder = Customer::findOrFail($orderId);
//        dd($idOrder);
//        dd($orderId['email']);
        $deleteOrderProcess = Customer::where('id', $orderId)->delete();
        Mail::send('frontend.mail.cancelOrder', ['idOrder' => $idOrder], function ($m) use ($idOrder) {
            $m->to($idOrder->email);
            $m->from('viethanguyen510@gmail.com');
            $m->subject('cancel order');
        });

        return redirect('home/order')->with('notification',  $idOrder->fullname ."'s order has been canceled");
    }
}

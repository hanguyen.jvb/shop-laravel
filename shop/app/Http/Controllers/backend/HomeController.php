<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Product;
use App\User;
use App\Models\Order;
use App\Models\Customer;
use App\Models\CustomerOrder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\UpdateAdmin;
use Illuminate\Support\Str;
use Carbon\Carbon;

class HomeController extends Controller
{
    public function list()
    {
        $timerNow = Carbon::now();
        $users = User::all();
        $customersOrdered = Customer::where('state', 2)->get();
        $customersShipping = Customer::where('state', 1)->get();
        $orders = Customer::where('state', 0)->get();
        $orderCustomer = CustomerOrder::all();
        $customerOrder = Customer::where('state', 0)->orderBy('id', 'desc')->take(4)->get();
//        dd($customerOrder);
//        dd($customersOrdered);

        return view('backend.home', [
            'users' => $users, 'customersOrdered' => $customersOrdered,
            'customersShipping' => $customersShipping, 'orders' => $orders,
            'orderCustomer' => $orderCustomer, 'timerNow' => $timerNow, 'customerOrder' => $customerOrder,
        ]);
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('admin/login');
    }

    public function setAcount($idAdmin)
    {
        try {
            $data['idAdmin'] = User::findOrFail($idAdmin);
            return view('backend.acount.update', $data);
        } catch (ModelNotFoundException  $e) {
            return view('backend.error.404');
        }
    }

    public function updateAcount(UpdateAdmin $request, $idAdmin)
    {
        $admin = User::find($idAdmin);
        $admin->fullname = $request->fullname;
        $admin->slug = Str::slug($request->fullname, '-');
        $admin->username = $request->username;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);
        $admin->phone = $request->phone;
        $admin->address = $request->address;

        if ($request->hasFile('fileToUpload')) {
            $file = $request->fileToUpload;
            $fileName = Str::slug($request->fullname, '-') . '.' . $file->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $admin->img = $fileName;
            $admin->save();

            return redirect()->back()->with('notification', 'update admin successfully')->withInput();
        }
    }

    public function listProfile()
    {
        return view('backend.acount.listAdmin');
    }
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\addUser;
use App\Http\Requests\editUser;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function list()
    {
//        dd(Auth::guard('web')->user()->id);
        $data['users'] = User::orderBy('id', 'desc')->paginate(3);
        return view('backend.user.list', $data);
    }

    public function add()
    {
        return view('backend.user.add');
    }

    public function store(addUser $request)
    {
        $user = new User();
        $user->fullname = $request->fullname;
        $user->username = $request->username;
        $user->slug = Str::slug($request->fullname, '-');
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->level = $request->role;

        if ($request->hasFile('fileToUpload')) {
            $file = $request->fileToUpload;
            $fileName = Str::slug($request->fullname, '-') . '.' . $file->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $user->img = $fileName;
        }

        $user->save();

        return redirect('/home/users')->with('notification', 'Add ' . $request->fullname . ' successfully')->withInput();
    }

    public function edit($idUser)
    {
        try {
            $data['idUser'] = User::findOrFail($idUser);
            return view('backend.user.edit', $data);
        } catch (ModelNotFoundException  $e) {
            return view('backend.error.404');
        }
    }

    public function update(editUser $request, $idUser)
    {
        $user = User::find($idUser);
        $user->fullname = $request->fullname;
        $user->username = $request->username;
        $user->slug = Str::slug($request->fullname, '-');
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->level = $request->role;
        $user->img = $request->fileUpload;

        if ($request->hasFile('fileUpload')) {
            $file = $request->fileUpload;
            $fileName = Str::slug($request->fullname, '-') . '.' . $file->getClientOriginalExtension();
            $file->move('img/', $fileName);
            $user->img = $fileName;
        }

        $user->save();

        return redirect('home/users')->with('notification', 'Edit user ' . $user->fullname . ' successfully');
    }

    public function destroy(Request $request)
    {
            $idCheckDelete = $request->deleteUser;
//            dd($idCheckDelete);
            if ($idCheckDelete == "") {
                return redirect('home/users')->with('error-warning', 'Select user you wanna delete');
            } elseif ($idCheckDelete === (Auth::guard('web')->user()->id)) {
                return redirect('home/users')->with('error-warning', 'Cannot remove Admin');
            } else {
                foreach ($idCheckDelete as $idCheck) {
                    User::where('id', $idCheck)->delete();
                }
            }

            return redirect('home/users')->with('notification', 'Delete user successfully');
    }

    public function searchUser(Request $request)
    {
//        dd($request->all());
        if ($request->get('keySearchUser')) {
            $keySearch = $request->get('keySearchUser');
//            dd($keySearch);
            $users = User::where('username', 'LIKE', '%' . $keySearch . '%')
                ->orderBy('id', 'desc')
                ->take(4)
                ->get();
            $output = '<ul class="dropdown-menu" style="display: block;"><a href="/users/search-all/' . $keySearch . '" style="margin-left: 6px; color: black">Hiển thị tất cả kết quả cho "<span style="color: #ff0000">' . $keySearch . '</span>"</a>';
            foreach ($users as $user) {
                $output .= '<li style="border-bottom: 1px solid #DCDCDC;">
                                <a href="/' . $user->username . '-' . $user->id . '" style="color: black"><img src="/img/' . $user->img . '" width="100" height="90"> <span style="font-size: 15px">' . $user->fullname . '</span></a>
                            </li>';
            }
            $output .= '</ul>';
            return $output;
        }
    }

//    public function search(Request $request)
//    {
//        if ($request->get('key_search') == '') {
//            return redirect('home/users')->with('error-warning', 'enter search keywords');
//        } else {
//            $keySearch = $request->get('key_search');
//            $data['search'] = User::where('fullname', 'like', '%' . $keySearch . '%')
//            ->orWhere('username', 'like', '%' . $keySearch . '%')
//            ->orWhere('email', 'like', '%' . $keySearch . '%')
//            ->orWhere('phone', 'like', '%' . $keySearch . '%')
//            ->orWhere('address', 'like', '%' . $keySearch . '%')
//            ->get();
//
//            return view('backend.user.resultSearch', $data);
//        }
//    }

    public function setIsAdmin($idUser)
    {
        try {
            $id = User::find($idUser);
            if ($id->level == 1) {
                return redirect('home/users')->with('error-warning', $id->username . ' is already a admin');
            } else {
                $data['setAdmin'] = User::where('id', $idUser)->update(['level' => 1]);
                return redirect('home/users')->with('notification', 'Set ' . $id->username . ' is admin successfully' );
            }
        } catch (ModelNotFoundException  $e) {
            return view('backend.error.404');
        }
    }

    public function removeAdmin($idUser)
    {
        try {
            $id = User::find($idUser);
            if ($id->level == 2) {
                return redirect('home/users')->with('error-warning', $id->username . ' is already a user');
            } else {
                $data['removeAdmin'] = User::where('id', $idUser)->update(['level' => 2]);
                return redirect('home/users')->with('notification', 'Has removed admin ' . $id->username);
            }
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Images;
use App\Models\Attribute;
use App\Models\ValuesAttribute;
use App\Http\Requests\AddAttribute;
use App\Http\Requests\AddValueAttribute;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Symfony\Component\CssSelector\Node\FunctionNode;
use App\Http\Requests\AddProduct;
use App\Http\Requests\EditProduct;

class ProductController extends Controller
{
    /** List product */
    public function list($idCategory)
    {

        try {
            // dd(valueAttribute(Product::find(7)->valuesAttribute()->get()));
            $data['idCategory'] = Category::findOrFail($idCategory);
            $data['products'] = Product::where('category_id', $idCategory)->orderBy('id', 'desc')->paginate(2);

            return view('backend.product.list', $data);
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Get front-end add product*/
    public function add($idCategory)
    {
        try {
            $idCategory = Category::findOrFail($idCategory);
            $categories = Category::all();
            $attributes = Attribute::all();

            return view('backend.product.add', ['idCategory' => $idCategory, 'categories' => $categories, 'attributes' => $attributes]);
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Get data form add product, save data, upload images */
    public function store(AddProduct $request, $idCategory)
    {
//        dd($request->all());
        try {
            $idCategory = Category::findOrFail($idCategory);
            $product = new Product();
            $product->name = $request->name;
            $product->code = $request->code;
            $product->slug = Str::slug($request->name, '-');
            $product->price = $request->price;
            $product->promotion = $request->promotion;
            $product->featured = $request->featured;
            $product->status = $request->status;
            $product->start_sale = $request->start_sale;
            $product->end_sale = $request->end_sale;
            $product->description = $request->description;

            if ($request->select_category == 2) {
                $product->category_id = $idCategory->id;
            } else {
                $product->category_id = $request->select_category;
            }

            if ($request->hasFile('fileToUpload')) {
                $file = $request->fileToUpload;
                $fileName = Str::slug($request->name, '-') . '.' . $file->getClientOriginalExtension();
                $file->move('img/', $fileName);
                $product->img = $fileName;
            }

            $product->save();

            $arr = array();
            foreach ($request->attr as $value) {
                $arr[] = $value;
            }
            $product->valuesAttribute()->attach($arr);

            if ($request->hasFile('uploadMultipleImage')) {
                foreach ($request->uploadMultipleImage as $multipleImg) {
                    $fileNameMultipleImg = $multipleImg->getClientOriginalName();
                    $multipleImg->move('img/', $fileNameMultipleImg);

                    $multipleImg = new Images();
                    $multipleImg->image_product = $fileNameMultipleImg;
                    $multipleImg->product_id = $product->id;

                    $multipleImg->save();
                }
            }

            return redirect('home/product/'.$idCategory->id)->with('notification', 'Add product successfully')->withInput();
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Get front-end edit product, old data product */
    public function edit($idCategory, $idProduct)
    {
        try {
            $data['idCategory'] = Category::findOrFail($idCategory);
            $data['idProduct'] = Product::findOrFail($idProduct);
            $data['categories'] = Category::all();

            return view('backend.product.edit', $data);
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Get data edit product, update product*/
    public function update(EditProduct $request, $idCategory, $idProduct)
    {
        try {
            $idCategory = Category::findOrFail($idCategory);
            $idProduct = Product::findOrFail($idProduct);

            $idProduct->name = $request->name;
            $idProduct->code = $request->code;
            $idProduct->slug = Str::slug($request->name, '-');
            $idProduct->price = $request->price;
            $idProduct->promotion = $request->promotion;
            $idProduct->featured = $request->featured;
            $idProduct->status = $request->status;
            $idProduct->description = $request->description;

            if ($request->select_category == 2) {
                $idProduct->category_id = $idCategory->id;
            } else {
                $idProduct->category_id = $request->select_category;
            }

            if ($request->hasFile('fileUpload')) {
                $file = $request->fileUpload;
                $fileName = Str::slug($request->name, '-') . '.' . $file->getClientOriginalExtension();
                $file->move('img/', $fileName);
                $idProduct->img = $fileName;
            }

            $idProduct->save();

            return redirect('home/product/'. $idCategory->id)->with('notification', 'Update ' . $idProduct->name . ' successfully')->withInput();
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Destroy Product */
    public function destroy(Request $request, $idCategory)
    {
        try {
            $idCategory = Category::findOrFail($idCategory);
            $idCheckDelete = $request->deleteProduct;
            if ($idCheckDelete == "") {
                return redirect('home/product/'. $idCategory->id)->with('error-warning', 'Select product you wanna delete');
            } else {
                foreach ($idCheckDelete as $idCheck) {
                    Product::where('id', $idCheck)->delete();
                }
            }

            return redirect('home/product/'. $idCategory->id)->with('notification', 'Delete product successfully');
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Search all product */
    public function searchAllProduct($keyProduct)
    {
//        $categories = Category::all();
        $resultSearchAllProduct = Product::where('name', 'LIKE', '%' . $keyProduct . '%')->paginate(3);
//        dd($resultSearchAll);
        return view('backend.product.searchAllProduct', [
            'resultSearchAllProduct' => $resultSearchAllProduct, 'keyProduct' => $keyProduct
        ]);
    }

    /** Search product with ajax */
    public function searchProduct(Request $request)
    {
        if ($request->get('keySearchProduct')) {
            $keySearch = $request->get('keySearchProduct');
//            dd($keySearch);
            $products = Product::where('name', 'LIKE', '%' . $keySearch . '%')
                ->orderBy('id', 'desc')
                ->take(4)
                ->get();
            $output = '<ul class="dropdown-menu" style="display: block; margin-top: -480px; width: 40%"><a href="/home/product/search-all-product/' . $keySearch . '" style="margin-left: 6px; color: black">Hiển thị tất cả kết quả cho "<span style="color: #ff0000">' . $keySearch . '</span>"</a>';
            foreach ($products as $product) {
                $output .= '<li style="border-bottom: 1px solid #DCDCDC;">
                                <a href="/home/product/detail-product/' . $product->id . '" style="color: black"><img src="/img/' . $product->img . '" width="70" height="60"> <span style="font-size: 13px">' . $product->name . '</span></a>
                            </li>';
            }
            $output .= '</ul>';
            return $output;
        }
    }

    public function detailProduct($id)
    {
        $productId = Product::find($id);
        return view('backend.product.detail', ['productId' => $productId]);
    }

    /** Upload images for product*/
    public function uploadMultipleImg(Request $request, $idCategory, $idProduct)
    {
        try {
            $idCategory = Category::findOrFail($idCategory);
            $idProduct = Product::findOrFail($idProduct);

            if ($request->hasFile('uploadMultipleImg')) {
                foreach ($request->uploadMultipleImg as $multipleImg) {
                    $fileNameMultipleImg = $multipleImg->getClientOriginalName();
                    $multipleImg->move('img/', $fileNameMultipleImg);

                    $multipleImg = new Images();
                    $multipleImg->image_product = $fileNameMultipleImg;
                    $multipleImg->product_id = $idProduct->id;

                    $multipleImg->save();
                }
            }

            return redirect()->back()->with('notification', 'Upload photos of ' . $idProduct->name . ' product successfully');
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Get all images product */
    public function showProductImages($idCategory, $idProduct)
    {
        try {
            $produts = Product::all();
            $idCategory = Category::findOrFail($idCategory);
            $images = Images::where('product_id', $idProduct)->get();

            return view('backend.product.productImages', ['idCategory' => $idCategory, 'idProduct' => $idProduct, 'images' => $images, 'products' => $produts]);
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Update Images */
    public function updateImages(Request $request, $idProduct, $idImage)
    {
        try {
            $idImage = Images::findOrFail($idImage);
            $idImage->name = $request->image_name;

            if ($request->product_name == 0) {
                $idImage->product_id = $idProduct;
            } else {
                $idImage->product_id = $request->product_name;
            }

            if ($request->hasFile('fileUpload')) {
                $file = $request->fileUpload;
                $fileName = Str::slug($request->image_name, '-') . '.' . $file->getClientOriginalExtension();
                $file->move('img/', $fileName);
                $idImage->image_product = $fileName;
            }

            $idImage->save();

            return back()->with('notification', 'Update img successfully');
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** Destroy images */
    public function destroyImages(Request $request, $idProduct)
    {
        try {
            $idProduct = Product::findOrFail($idProduct);

            $idCheckDelete = $request->deleteImages;
            if ($idCheckDelete == "") {
                return redirect()->back()->with('error-warning', 'Select image you wanna delete');
            } else {
                foreach ($idCheckDelete as $idCheck) {
                    Images::where('id', $idCheck)->delete();
                }
            }

            return redirect()->back()->with('notification', 'Delete image successfully');
        } catch (ModelNotFoundException $e) {
            return view('backend.error.404');
        }
    }

    /** List attributes */
    public function listAttribute()
    {
        $data['attributes'] = Attribute::all();
        return view('backend.attribute.list', $data);
    }

    /** Add attribute */
    public function addAttribute(AddAttribute $request)
    {
        $attribute = new Attribute();
        $attribute->name = $request->attr_name;
        $attribute->save();

        return redirect()->back()->with('notification', 'Add ' . $request->attr_name . ' successfully');
    }

    /** Add value attribute */
    public function addValue(AddValueAttribute $request)
    {
        $value = new ValuesAttribute();
        $value->attribute_id = $request->attr_id;
        $value->value = $request->value_name;
        $value->save();

        return redirect()->back()->with('notification', 'Add ' . $request->value_name . ' successfully');
    }
}

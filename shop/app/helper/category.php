<?php

    use App\Models\Category;
    use App\models\Product;
    function showCategories($arr, $parent, $tab, $selected)
    {
        foreach ($arr as $row) {
            if ($row['parent'] == $parent) {
                if ($row['id'] == $selected) {
                    echo '<option value="' . $row['id'] . '">' . $tab . $row['name'] . '</option>';
                }
                echo '<option value="' . $row['id'] . '">' . $tab . $row['name'] . '</option>';

                showCategories($arr, $row['id'], $tab. '--|', $selected);
            }
        }
    }

    function valueAttribute($arr)
    {
        $result = array();
        foreach ($arr as $value) {
            $attribute = $value->attribute->name;
            $result[$attribute][] = $value->value;
        }
        return $result;
    }

    function listCategory($parentId)
    {
        if (isset($parentId)) {
            $category = Category::where('parent', $parentId)->get();
            return $category;
        }
    }

    function listProducts($categoryId)
    {
        if (isset($categoryId)) {
            $products = Product::where('category_id', $categoryId)->get();
            return $products;
        }
    }

    function listLimitProduct($catId)
    {
        if (isset($catId)) {
            $limitProduct = Product::where('category_id', $catId)
                ->where(function ($query) {
                    return $query->where('featured', '=', 1);
                })
                ->get();
            return $limitProduct;
        }
    }

function listProductsSortDown($categoryId)
{
    if (isset($categoryId)) {
        $products = Product::where('category_id', $categoryId)->orderBy('name', 'desc')->get();
        return $products;
    }
}

function listProductsSortUp($categoryId)
{
    if (isset($categoryId)) {
        $products = Product::where('category_id', $categoryId)->orderBy('name', 'asc')->get();
        return $products;
    }
}

function listProductsPriceIncrease($categoryId)
{
    if (isset($categoryId)) {
        $products = Product::where('category_id', $categoryId)->orderBy('price', 'asc')->get();
        return $products;
    }
}

function listProductsPriceDecrease($categoryId)
{
    if (isset($categoryId)) {
        $products = Product::where('category_id', $categoryId)->orderBy('price', 'desc')->get();
        return $products;
    }
}

function listProductsLatestProduct($categoryId)
{
    if (isset($categoryId)) {
        $products = Product::where([
            ['category_id', $categoryId],
            ['featured', '=', '1']
        ])
            ->orderBy('id', 'desc')->get();
        return $products;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function productOrder()
    {
        return $this->hasMany('App\Models\productOrder', 'order_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['fullname', 'email', 'phone', 'address', 'state', 'description', 'created_at'];
    protected $table = 'cutomers';

    public function customerOrdering()
    {
        return $this->hasMany('App\Models\CustomerOrder', 'customer_id', 'id');
    }
}

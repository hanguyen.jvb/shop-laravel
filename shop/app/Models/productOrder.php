<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class productOrder extends Model
{
    protected $table = 'productorders';

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }
}

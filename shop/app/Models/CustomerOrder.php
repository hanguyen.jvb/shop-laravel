<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerOrder extends Model
{
    protected $fillable = ['name', 'price', 'quantity', 'img', 'customer_id'];
    protected $table = 'customer_orders';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function categoryy()
    {
        return $this->belongsTo('App\Models\Category', 'category_id', 'id');
    }

    public function image()
    {
        return $this->hasMany('App\Models\Images', 'product_id', 'id');
    }

    public function valuesAttribute()
    {
        return $this->belongsToMany('App\Models\ValuesAttribute', 'values_product', 'product_id', 'value_id');
    }
}

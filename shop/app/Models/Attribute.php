<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';
    public $timestamps = false;

    public function values()
    {
        return $this->hasMany('App\Models\ValuesAttribute', 'attribute_id', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValuesAttribute extends Model
{
    protected $table = 'values_attribute';
    public $timestamps = false;

    public function attribute()
    {
        return $this->belongsTo('App\Models\Attribute', 'attribute_id', 'id');
    }
}

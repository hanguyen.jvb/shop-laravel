<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Customer;
use App\Models\CustomerOrder;

class ShoppingMail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $customerOrder = [];
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, $customerOrder)
    {
        $this->customer = $customer;
        $this->customerOrder = $customerOrder;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return view('frontend.mail.shopping');
    }
}

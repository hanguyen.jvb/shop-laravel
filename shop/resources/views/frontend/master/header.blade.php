<header class="header_area">
    <div class="top_menu row m0">
        <div class="container">
         <div class="float-left">
             <a href="mailto:support@colorlib.com">listashopvhn@gmail.com</a>
             <a style="color: #333333" href="{{ route('home.about.list') }}"><i class="fa fa-map-marker"></i> Hệ thống cửa hàng</a>
             <a href="#">Mở cửa: 8h30 - 22h00, thứ 2 - CN hàng tuần</a>
         </div>
         <div class="float-right">
            <ul class="header_social">
                 <li><a href="https://www.facebook.com/haviet0610" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                 <li><a href="https://www.instagram.com/" title="Instagram"><i class="fa fa-instagram"></i></a></li>
            </ul>
         </div>
        </div>
    </div>
 <div class="main_menu">
     <nav class="navbar navbar-expand-lg navbar-light main_box">
         <div class="container">
             <!-- Brand and toggle get grouped for better mobile display -->
             <a class="navbar-brand logo_h" href="{{ route('frontend.home.list') }}"><img src="frontend/img/logo.png" alt=""></a>
             <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
{{--             <div class="form-group" style="margin-left: 500px; margin-top: 250px">--}}
             <form action="{{ route('frontend.home.search') }}" method="post" id="form-search-item">
                 @csrf
                 <input class="form-control mr-sm-2" type="text" name="key_search" placeholder="Search..." id="key_search" style="width: 219px; height: 39px; margin-left: 170px">
                 <div id="resultSearch"></div>
             </form>
{{--             </div>--}}
             <!-- Collect the nav links, forms, and other content for toggling -->
             <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                 <ul class="nav navbar-nav menu_nav ml-auto">
{{--                     <div class="form-group" style="margin-left: 500px; margin-top: 250px">--}}
{{--                         <input class="form-control mr-sm-2" type="text" name="key_search" placeholder="Search..." id="key_search" style="width: 219px; height: 39px">--}}
{{--                         <div id="resultSearch"></div>--}}
{{--                     </div>--}}
{{--                     @csrf  --}}
{{--                     <li class="nav-item">--}}
                 {{--                         <div class="form-group">--}}
                 {{--                             <input class="form-control mr-sm-2" type="text" name="key_search" placeholder="Search..." id="key_search" style="margin-top: 20px">--}}
                 {{--                             <div id="resultSearch"></div>--}}
                 {{--                         </div>--}}
                 {{--                         @csrf--}}
                 {{--                     </li>--}}
                     <li class="nav-item active"><a class="nav-link" href="{{ route('frontend.home.list') }}">Home</a></li>
                     @foreach ($categories as $category)
                         @if ($category->parent == 0)
                         <li class="nav-item submenu dropdown">
                            <a href="{{ route('frontend.home.category.ProductCategory', ['name' => $category->name, 'id' => $category->id]) }}" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ $category->name }}</a>
                            <ul class="dropdown-menu">
                                @foreach ($categories as $item)
                                    @if ($item->parent == $category->id)
                                    <li class="nav-item"><a class="nav-link" href="{{ route('frontend.home.category.ProductSubCategory', ['name' => $item->name, 'id' => $item->id]) }}">{{ $item->name }}</a>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                         @endif
                     @endforeach
{{--                     <li class="nav-item submenu dropdown">--}}
{{--                         <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Acount</a>--}}
{{--                         <ul class="dropdown-menu">--}}
{{--                             @if(!Auth::guard('client')->user())--}}
{{--                             <li class="nav-item"><a class="nav-link" href="{{ route('home.acount.loginView') }}">Đăng nhập</a>--}}
{{--                             <li class="nav-item"><a class="nav-link" href="{{ route('home.acount.registerView') }}">Đăng ký</a>--}}
{{--                             @endif--}}
{{--                             @if(Auth::guard('client')->user())--}}
{{--                                 <li class="nav-item"><a class="nav-link" href="{{ route('home.acount.profileView') }}">Tài khoản</a>--}}
{{--                                 <li class="nav-item"><a class="nav-link" href="{{ route('home.acount.logout') }}">Đăng xuất</a>--}}
{{--                             @endif--}}
{{--                         </ul>--}}
{{--                     </li>--}}
                     <li class="nav-item"><a class="nav-link" href="{{ route('home.store.infoShop') }}">Giới thiệu</a></li>
                 </ul>
                 <ul class="nav navbar-nav navbar-right">
                     <li class="nav-item">
                            <a data-toggle="tooltip" data-placement="top" href="/cart/list" class="cart"><i class="lnr lnr lnr-cart" style="font-size: 20px"></i>(<span style="color: red">{{ Cart::Content()->count() }}</span>) Sản phẩm</a>
                     </li>
                 </ul>
             </div>
         </div>
     </nav>
 </div>
</header>


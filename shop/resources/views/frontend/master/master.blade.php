<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="icon" href="frontend/img/favicon.png" type="image/png">
    <title>ListAshop</title>
    <!-- Bootstrap CSS -->
    <base href="{{ asset('/') }}">
    <link rel="stylesheet" href="frontend/css/bootstrap.css">
    <link rel="stylesheet" href="frontend/vendors/linericon/style.css">
    <link rel="stylesheet" href="frontend/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="frontend/vendors/owl-carousel/owl.carousel.min.css">
    <link rel="stylesheet" href="frontend/vendors/lightbox/simpleLightbox.css">
    {{-- <link rel="stylesheet" href="frontend/vendors/nice-select/css/nice-select.css"> --}}
    <link rel="stylesheet" href="frontend/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="frontend/vendors/jquery-ui/jquery-ui.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <!-- main css -->
    <link rel="stylesheet" href="frontend/css/style.css">
    <link rel="stylesheet" href="frontend/css/responsive.css">
    <link rel="stylesheet" href="frontend/css/header/header.css">
    <link rel="stylesheet" href="frontend/css/home/footer.css">
    @stack('css')
</head>
<body>
    <!--header-->
    @include('frontend.master.header')
    <!--end header-->
    @yield('content')
    @include('frontend.master.footer')

    <script src="frontend/js/jquery-3.2.1.min.js"></script>
    <script src="frontend/js/jquery-ui.js"></script>
    <script src="frontend/js/popper.js"></script>
    <script src="frontend/js/bootstrap.min.js"></script>
    <script src="frontend/js/jquery.fancybox.min.js"></script>
    <script src="frontend/js/stellar.js"></script>
    <script src="frontend/js/countdown-timer/jquery.countdown.min.js"></script>
    <script src="frontend/js/jquery.validate.min.js"></script>
    <script src="frontend/vendors/lightbox/simpleLightbox.min.js"></script>
    <script src="frontend/vendors/nice-select/js/jquery.nice-select.js"></script>
    <script src="frontend/vendors/isotope/imagesloaded.pkgd.min.js"></script>
    <script src="frontend/vendors/isotope/isotope-min.js"></script>
    <script src="frontend/vendors/owl-carousel/owl.carousel.min.js"></script>
    <script src="frontend/js/jquery.ajaxchimp.min.js"></script>
    <script src="frontend/vendors/counter-up/jquery.waypoints.min.js"></script>
     <script src="frontend/vendors/flipclock/timer.js"></script>
    <script src="frontend/vendors/counter-up/jquery.counterup.js"></script>
    <script src="frontend/js/mail-script.js"></script>
    <script src="frontend/js/theme.js"></script>
    <script src="frontend/js/home/validateFormSearch.js"></script>
    @stack('script')
</body>
</html>

<script>
    $(document).ready(function () {
        $("#key_search").keyup(function () {
            var keySearch = $(this).val();
            if (keySearch != '' && keySearch.trim() != "") {
                var _token = $('input[name="_token"]').val();

                $.ajax({
                    url: "{{ route('frontend.home.search') }}",
                    method: "POST",
                    data: {keySearch:keySearch, _token:_token},
                    success: function (data) {
                        $("#resultSearch").fadeIn();
                        $("#resultSearch").html(data);
                        // console.log(data);
                    }
                });
            } else {
                $("#resultSearch").fadeOut();
            }
        });
    });
</script>

{{--<script>--}}
{{--    $('[data-countdown]').each(function() {--}}
{{--        var $this = $(this), finalDate = $(this).data('countdown');--}}
{{--        $this.countdown(finalDate, function(event) {--}}
{{--            $this.html(event.strftime('%D ngày %H:%M:%S'));--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}

<script type="text/javascript">
    $('#count-timer-animate').countdown('2020/06/06', function(event) {
        $(this).html(event.strftime('%d ngày %H:%M:%S'));
    });
</script>


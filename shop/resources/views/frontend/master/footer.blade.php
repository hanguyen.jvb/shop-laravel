{{--<!--================ start footer Area  =================-->--}}
{{--<footer class="footer-area">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-3  col-md-6 col-sm-6">--}}
{{--                <div class="single-footer-widget">--}}
{{--                    <h6 class="footer_title">Chính sách</h6>--}}
{{--                    <p><span style="color: red">ListAshop</span> cửa hàng kinh doanh sneakers luôn mang tới sự hài lòng cho khách hàng về chất--}}
{{--                        lượng sản phẩm, sneakers--}}
{{--                        đều mang thương hiệu made in Việt Nam và tạo nên chất riêng của Street Wear--}}
{{--                    </p>--}}
{{--                    <div id="map" style="width:400px; height:400px; margin-top: 50px">--}}
{{--                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.6432793211875!2d105.76653951440633!3d20.966835895266488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313452d9cb34e637%3A0x9b3429fe8f015612!2zVW5pbWF4LCBRdWFuZyBUcnVuZywgSMOgIMSQw7RuZywgSMOgIE7hu5lpLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1590975321851!5m2!1sen!2s" width="400" height="250" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-6">--}}
{{--                <div class="single-footer-widget">--}}
{{--                    <h6 class="footer_title">Đăng ký nhận tin khuyến mãi</h6>--}}
{{--                    --}}{{--                    <p>Stay updated with our latest trends</p>--}}
{{--                    <div id="mc_embed_signup">--}}
{{--                        <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">--}}
{{--                            <div class="input-group d-flex flex-row">--}}
{{--                                <input name="EMAIL" placeholder="Nhâp Email của bạn" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">--}}
{{--                                <button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>--}}
{{--                            </div>--}}
{{--                            <div class="mt-10 info"></div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--                        <div class="col-lg-3 col-md-6 col-sm-6">--}}
{{--                            <div class="single-footer-widget instafeed">--}}
{{--                                <h6 class="footer_title">Instagram Feed</h6>--}}
{{--                                <ul class="list instafeed d-flex flex-wrap">--}}
{{--                                    <li><img src="frontend/img/instagram/Image-01.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-02.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-03.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-04.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-05.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-06.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-07.jpg" alt=""></li>--}}
{{--                                    <li><img src="frontend/img/instagram/Image-08.jpg" alt=""></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--            <div class="col-lg-2 col-md-6 col-sm-6">--}}
{{--                <div class="single-footer-widget f_social_wd">--}}
{{--                    <h6 class="footer_title">Kết nối vói chúng tôi</h6>--}}
{{--                    <p>Let us be social</p>--}}
{{--                    <div class="f_social">--}}
{{--                        <a href="#" title="Facebook"><i class="fa fa-facebook"></i></a>--}}
{{--                        <a href="#" title="Instagram"><i class="fa fa-instagram"></i></a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row footer-bottom d-flex justify-content-between align-items-center">--}}
{{--            <p class="col-lg-12 footer-text text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->--}}
{{--                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Bản quyền thuộc về <a href="https://colorlib.com" target="_blank">ListAshop Vietnam</a>--}}
{{--                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</footer>--}}
{{--<!--================ End footer Area  =================-->--}}

<footer class="footer-area">
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-3 colorlib-widget">
                <h4>Giới thiệu</h4>
               <div>
                   <ul class="colorlib-footer-links">
                       <li><a href="#">Trang chủ </a></li>
                       <li><a href="#">Giới thiệu</a></li>
                       <li><a href="#">Sản phẩm</a></li>
                   </ul>
               </div>

            </div>
            <div class="col-md-3 colorlib-widget">
                <h4>Chăm sóc khách hàng</h4>
                    <ul class="colorlib-footer-links">
                        <li><a href="#">Liên hệ </a></li>
                        <li><a href="#">Giao hàng/ Đổi hàng</a></li>
                        <li><a href="#">Mã giảm giá</a></li>
                        <li><a href="#">Sản phẩm yêu thích</a></li>
                        <li><a href="#">Đặc biệt</a></li>
                    </ul>

            </div>
            <div class="col-md-2 colorlib-widget">
                <h4>Thông tin</h4>

                <ul class="colorlib-footer-links">
                    <li><a href="#">Về chúng tôi</a></li>
                    <li><a href="#">Thông tin vận chuyển</a></li>
                    <li><a href="#">Chính sách bảo mật</a></li>
                    <li><a href="#">Hỗ trợ</a></li>

                </ul>

            </div>
            <div class="col-md-4  colorlib-widget">
                <h4>Thông tin liên hệ</h4>
                <ul class="colorlib-footer-links">
                    <li>Unimax 210 Quang Trung, Hà Đông, Hà Nội</li>
                    <li><a href="tel://1234567920">0399881175</a></li>
                    <li><a href="mailto:info@yoursite.com">ListAshop@gmail.com</a></li>
                    <li><a href="#">http://ListAshop.vn.com</a></li>
                </ul>
            </div>
            <img src="frontend/img/boct_1.png" alt="">
        </div>
        <div class="row footer-bottom d-flex justify-content-between align-items-center">
                        <p class="col-lg-12 footer-text text-center"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Bản quyền thuộc về <a href="https://colorlib.com" target="_blank">ListAshop Vietnam</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
    </div>
</footer>

@push('css')
    <link rel="stylesheet" href="frontend/css/product/product-category.css">
@endpush
@extends('frontend.master.master')
@section('title', $keyName . ' - ListAshop')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content text-center">
                    <h2>Result Search Page</h2>
{{--                    <div class="page_link">--}}
{{--                        <a href="index.html">Home</a>--}}
{{--                        <a href="category.html">Category</a>--}}
{{--                        <a href="category.html">Women Fashion</a>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area p_120">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-9">
                    <div class="product_top_bar">
                        <div class="left_dorp">
                           <div style="margin-top: 6px">
                               <a href="{{ route('frontend.home.list') }}"> <span style="font-size: 18px; color: black">Home</span></a> <span class="lnr lnr-chevron-right"></span>
                               <span style="color: black">Kết quả tìm kiếm với từ khóa " {{ $keyName }} "</span> <span class="lnr lnr-chevron-right"></span>
                               <span style="color: black">
                                   @if(count($resultSearchAll) > 0)
                                       Có {{ count($resultSearchAll) }} kết quả tìm kiếm phù hợp
                                   @else
                                       KHÔNG TÌM THẤY BẤT KỲ KẾT QUẢ NÀO VỚI TỪ KHÓA TRÊN.
                                   @endif
                               </span>
                           </div>
{{--                            <select class="sorting">--}}
{{--                                <option value="1">Default sorting</option>--}}
{{--                                <option value="2">Default sorting 01</option>--}}
{{--                                <option value="4">Default sorting 02</option>--}}
{{--                            </select>--}}
{{--                            <select class="show">--}}
{{--                                <option value="1">Show 12</option>--}}
{{--                                <option value="2">Show 14</option>--}}
{{--                                <option value="4">Show 16</option>--}}
{{--                            </select>--}}
                        </div>
{{--                        <div class="right_page ml-auto">--}}
{{--                            <nav class="cat_page" aria-label="Page navigation example">--}}
{{--                                <ul class="pagination">--}}
{{--                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a></li>--}}
{{--                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
{{--                                    <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
{{--                                    <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                                    <li class="page-item blank"><a class="page-link" href="#">...</a></li>--}}
{{--                                    <li class="page-item"><a class="page-link" href="#">6</a></li>--}}
{{--                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>--}}
{{--                                </ul>--}}
{{--                            </nav>--}}
{{--                        </div>--}}
                    </div>
                    <div class="latest_product_inner row">
                        @foreach($resultSearchAll as $resultSearch)
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <div class="f_p_item">
                                    @if ($resultSearch->promotion != 0)
                                        <div class="box-sale">
                                                <span class="text-sale">
                                                    -{{ $resultSearch->promotion }}%
                                                </span>
                                        </div>
                                    @endif
                                    @if($resultSearch->featured != 0)
                                            <div class="box-new">
                                                <span class="text-new">
                                                    Mới
                                                </span>
                                            </div>
                                    @endif
                                    <div class="f_p_img">
                                        <a href="/{{$resultSearch->slug}}-{{$resultSearch->id}}.htm"><img class="img-fluid" src="img/{{ $resultSearch->img }}" alt=""></a>
                                        <div class="p_icon">
                                            <a href="#"><i class="lnr lnr-heart"></i></a>
                                            <a href="/{{$resultSearch->slug}}-{{$resultSearch->id}}.htm"><i class="lnr lnr-cart"></i></a>
                                        </div>
                                    </div>
                                    <a href="/{{$resultSearch->slug}}-{{$resultSearch->id}}.htm"><h4>{{ $resultSearch->name }}</h4></a>
                                        <h5>
                                            <span style="color: black">{{ number_format($resultSearch->price * (100 - $resultSearch->promotion) / 100, '0', '', '.') }} đ</span>
                                            <del class="price_promotion" style="color: #888888">
                                                {{ number_format($resultSearch->price, '0', '', '.') }} đ
                                            </del>
                                        </h5>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets cat_widgets">
                            <div class="l_w_title">
                                <h3>Browse Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    @foreach ($categories as $category)
                                        @if ($category->parent == 0)
                                            <li>
                                                <a href="#"><span class="lnr lnr-chevron-right"></span> {{ $category->name }}</a>
                                                <ul class="list">
                                                    @foreach ($categories as $item)
                                                        @if($item->parent == $category->id)
                                                            <li><a href="{{ route('frontend.home.category.ProductSubCategory', ['name' => $item->name, 'id' => $item->id]) }}"><span class="lnr lnr-location"></span> {{ $item->name }}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Category Product Area =================-->
@endsection

@extends('frontend.master.master')
@section('title', 'Hệ thống cửa hàng ListAshop')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="" style="min-height: 500px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->
    <div class="row" style="margin-top: -50px">
        <div class="container">
           <div class="col-lg-12">
               <a href="#"><span style="color: #333333; font-size: 18px">Home</span></a> <span class="lnr lnr-chevron-right"></span> <span style="color: #333333; font-size: 18px">Hệ thống cửa hàng</span>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div id="map" style="width:500px; height:500px; margin-top: 50px">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.6432793211875!2d105.76653951440633!3d20.966835895266488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313452d9cb34e637%3A0x9b3429fe8f015612!2zVW5pbWF4LCBRdWFuZyBUcnVuZywgSMOgIMSQw7RuZywgSMOgIE7hu5lpLCBWaWV0bmFt!5e0!3m2!1sen!2s!4v1590975321851!5m2!1sen!2s" width="500" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
            <div class="col-lg-6">
                <div style="margin-top: 40px">
                    <h2><span style="font-size: 22px; color: #323c3f">Hệ thống cửa hàng</span></h2>
                </div>
                <div style="margin-top: 15px" class="info-system-shop">
                    <h2 style="color: #080000; font-size: 15px">
                        <span>
                            <i class="fa fa-map-marker"></i>
                        </span>
                        <span>ListAshop VietNam</span>
                    </h2>
                    <ul style="padding: 0px">
                        <li><a href="">- Địa chỉ: Unimax - 210 Quang Trung - Hà Đông - Hà Nội</a></li>
                        <li><a href="">- Điện thoại: 0399881175</a></li>
                        <li><a href="">- Email: ListAshopvhn@gmail.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link rel="stylesheet" href="frontend/css/about/about.css">
@endpush

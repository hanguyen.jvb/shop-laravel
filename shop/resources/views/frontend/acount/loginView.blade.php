@extends('frontend.master.master')
@section('title', 'login')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="" style="min-height: 500px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Login Box Area =================-->
    <section class="login_box_area p_120" style="margin-top: -100px">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <img class="img-fluid" src="frontend/img/login.jpg" alt="">
                        <div class="hover">
                            <h4>Bạn chưa có tài khoản?</h4>
                            <p>Đăng nhập để được hưởng nhiều ưu đãi hấp dẫn</p>
                            <a class="main_btn" href="{{ route('home.acount.registerView') }}">Đăng ký</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner">
                        <h3>Đăng nhập</h3>
                        @if (session('notifyLoginClient'))
                            <div class="alert alert-danger" style="width: 65%; margin-left: 17%">
                                <strong>{{ session('notifyLoginClient') }}</strong>
                            </div>
                        @endif
                        <form class="row login_form" action="{{ route('home.acount.dataLogin') }}" method="post">
                            @csrf
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập">
                                @if ($errors->has('username'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
{{--                            <div class="col-md-12 form-group">--}}
{{--                                <div class="creat_account">--}}
{{--                                    <input type="checkbox" id="f-option2" name="selector">--}}
{{--                                    <label for="f-option2">Keep me logged in</label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="col-md-12 form-group">
                                <button type="submit" class="btn submit_btn">Đăng nhập</button>
{{--                                <a href="#">Forgot Password?</a>--}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->
@endsection

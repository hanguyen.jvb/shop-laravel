<?php
// dd($categoryId);
// $getCategories = getCategory($categoryId->id);
// foreach ($getCategories as $value) {
//     // foreach ($value as $products) {
//     //     dd(get);
//     // }
//     $getProducts = getProducts($value->id);
//     foreach ($getProducts as $product) {
//         echo $product->name ."<br/>";
//     }
// }

?>
@push('css')
    <link rel="stylesheet" href="frontend/css/product/product-category.css">
@endpush
@extends('frontend.master.master')
@section('title', $categoryId['name'])
@section('content')
    <!--================Home Banner Area =================-->
    {{--      <section class="banner_area">--}}
    {{--        <div class="banner_inner d-flex align-items-center">--}}
    {{--            <div class="container">--}}
    {{--                <div class="banner_content text-center">--}}
    {{--                    <h2>Shop {{ $categoryId->name }} Page</h2>--}}
    {{--                    <div class="page_link">--}}
    {{--                        <a href="{{ route('frontend.home.list') }}">Home</a>--}}
    {{--                        <a href="category.html">{{ $categoryId->name }}</a>--}}
    {{--                        <a href="category.html">All {{ $categoryId->name }}</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    <!--================End Home Banner Area =================-->
    <!--================Home Banner Area =================-->
    <section class="" style="min-height: 500px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area p_120" style="margin-top: -100px">
        <div class="container">
            <div class="row flex-row-reverse">
                <div class="col-lg-9">
                    <div class="product_top_bar">
                        <div class="left_dorp">
                            <div class="multiple-select" style="margin-left: 6px">
                                <a href="{{ route('frontend.home.list') }}"> <span style="font-size: 18px; color: black">Home</span></a> <span class="lnr lnr-chevron-right"></span>
                                All <a href="{{ route('frontend.home.category.ProductCategory', ['id' => $categoryId->id, 'name' => $categoryId->name]) }}"><span>{{ $categoryId->name }}</span></a> <span class="lnr lnr-chevron-right"></span> <span>Sắp xếp Z -> A</span>
                            </div>
                        </div>
                        <div class="right_page ml-auto">
                            <nav class="cat_page" aria-label="Page navigation example">
                                {{--                                <ul class="pagination">--}}
                                {{--                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-long-arrow-left" aria-hidden="true"></i></a></li>--}}
                                {{--                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
                                {{--                                    <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
                                {{--                                    <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
                                {{--                                    <li class="page-item blank"><a class="page-link" href="#">...</a></li>--}}
                                {{--                                    <li class="page-item"><a class="page-link" href="#">6</a></li>--}}
                                {{--                                    <li class="page-item"><a class="page-link" href="#"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>--}}
                                {{--                                </ul>--}}
                            </nav>
                        </div>
                    </div>
{{--                    <div style="margin-top: -43px">--}}
{{--                        <ul class="parent-multiple-select list-group" style="width: 30%">--}}
{{--                            <li id="visible-function">--}}
{{--                                <div class="block-select-mutiple"><a href="#">Tùy chọn <span class="lnr lnr-chevron-down"></span></a></div>--}}
{{--                                <ul class="list-group list-multiple-action" style="margin-top: 10px">--}}
{{--                                    <li class="list-group-item disabled"><a href="">A -> Z</a></li>--}}
{{--                                    <li class="list-group-item"><a href="#">Z -> A</a></li>--}}
{{--                                    <li class="list-group-item"><a href="">Gía tăng dần</a></li>--}}
{{--                                    <li class="list-group-item"><a href="">Gía giảm dần</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}

                    <div class="latest_product_inner row">
                        {{-- @php $getCategories = listCategory($categoryId->id); @endphp --}}
                        @foreach ($category as $value)
                            <div class="container">
                                <div class="col-lg-12 col-md-12 col-sm-12" style="border-bottom: 2px solid #000; padding: 0px; margin-bottom: 10px">
                                    <h3 style="font-size: 18px; background-color: #000000; padding: 10px 28px 10px 28px; display: inline-block; border-top-left-radius: 3px; border-top-right-radius: 3px; margin: 0px">
                                        <a href="{{ route('frontend.home.category.ProductSubCategory', ['name' => $value->name, 'id' => $value->id]) }}" style="color: #fff">{{ $value->name }}</a>
                                    </h3>
                                </div>
                            </div>
                            @php $getProducts = listProductsSortDown($value->id); @endphp
                            @foreach ($getProducts as $product)
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <div class="f_p_item">
                                        @if ($product->promotion != 0)
                                            <div class="box-sale">
                                                <span class="text-sale">
                                                    -{{ $product->promotion }}%
                                                </span>
                                            </div>
                                        @endif
                                        @if($product->featured != 0)
                                            <div class="box-new">
                                                <span class="text-new">
                                                    Mới
                                                </span>
                                            </div>
                                        @endif
                                        <div class="f_p_img">
                                            <a href="/{{$product->slug}}-{{$product->id}}.htm"><img class="img-fluid" src="img/{{ $product->img }}" alt=""></a>
                                            <div class="p_icon">
                                                <a href="#"><i class="fa fa-eye"></i></a>
                                                <a href="/{{$product->slug}}-{{$product->id}}.htm"><i class="lnr lnr-cart"></i></a>
                                            </div>
                                        </div>
                                        <a href="/{{$product->slug}}-{{$product->id}}.htm"><h4>{{ $product->name }}</h4></a>
                                        <h5>
                                            <span style="color: black">{{ number_format($product->price * (100 - $product->promotion) / 100, '0', '', '.') }} đ</span>
                                            <del class="price_promotion" style="color: #888888">
                                                {{ number_format($product->price, '0', '', '.') }} đ
                                            </del>
                                        </h5>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets cat_widgets">
                            <div class="l_w_title">
                                <h3>Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    @foreach ($categories as $category)
                                        @if ($category->parent == 0)
                                            <li>
                                                <a href="#"><span class="lnr lnr-chevron-right"></span> {{ $category->name }}</a>
                                                <ul class="list">
                                                    @foreach ($categories as $item)
                                                        @if($item->parent == $category->id)
                                                            <li><a href="{{ route('frontend.home.category.ProductSubCategory', ['name' => $item->name, 'id' => $item->id]) }}"><span class="lnr lnr-location"></span> {{ $item->name }}</a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Category Product Area =================-->
@endsection

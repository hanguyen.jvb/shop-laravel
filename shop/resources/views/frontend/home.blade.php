
@push('css')
<link rel="stylesheet" href="frontend/css/home/home.css">
@endpush
@extends('frontend.master.master')
@section('title', 'ListAshop')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="home_banner_area" style="min-height: 650px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    </ol>
                    <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href="#"><img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide"></a>
                    </div>
                    <div class="carousel-item">
                        <a href=""><img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide"></a>
                    </div>
                    <div class="carousel-item">
                        <a href=""><img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide"></a>
                    </div>
                    <div class="carousel-item">
                        <a href=""><img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide"></a>
                    </div>
                        <div class="carousel-item">
                            <a href=""><img class="d-block w-100" src="frontend/img/banner/banner-sale.jpg" height="365" alt="four slide"></a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Feature Product Area =================-->
    <section class="feature_product_area">
        <div class="main_box">
            <div class="container">
                <div class="row hot_product_inner">
                    <div class="col-lg-4">
                        <div class="hot_p_item">
                            <img class="img-fluid" src="frontend/img/banner/banner_3.png" alt="">
                            <div class="product_text">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="hot_p_item">
                            <a href="{{ route('frontend.home.saleAll') }}"><img class="img-fluid" src="frontend/img/banner/sale.jpg" style="width: 380px; height: 178px" alt=""></a>
                            <div class="product_text">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="hot_p_item">
                            <img class="img-fluid" src="frontend/img/banner/banner_4.png" alt="">
                            <div class="product_text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="feature_product_inner">
                    <div class="main_title">
                        <a href="{{ route('frontend.home.saleAll') }}"><h2 style="font-family: Impact, Charcoal, sans-serif"><img src="frontend/img/icon-arrow-left.png" width="62" height="40"> Hot Deal <img src="frontend/img/icon-arrow.png" width="62" height="40"></h2></a>
{{--                        <p>Who are in extremely love with eco friendly system.</p>--}}
{{--                        <div style="color: #222222; font-size: 30px; font-weight: bold; background-color: #F0FFFF; width: 350px; height: 70px; border: 5px solid #5F9EA0; margin-left: 185px; padding: 17px" id="count-timer-animate">--}}
{{--                        </div>--}}
                    </div>

                    <div class="feature_p_slider owl-carousel">
                        @foreach ($saleProducts as $saleProduct)
                        <div class="item">
                            <div class="f_p_item">
                                @if ($saleProduct->promotion != 0)
                                    <div class="box-sale">
                                        <span class="text-sale">
                                            -{{ $saleProduct->promotion }}%
                                        </span>
                                    </div>
                                @endif
                                <div class="f_p_img">
                                    <a href="/{{$saleProduct->slug}}-{{$saleProduct->id}}.htm"><img class="img-fluid" src="img/{{ $saleProduct->img }}" alt=""></a>
                                    <div class="p_icon">
                                        <a href="#"><i class="fa fa-eye"></i></a>
                                        <a href="/{{$saleProduct->slug}}-{{$saleProduct->id}}.htm"><i class="lnr lnr-cart"></i></a>
                                    </div>
                                </div>
                                <a href="/{{$saleProduct->slug}}-{{$saleProduct->id}}.htm"><h4 style="font-size: 16px">{{ $saleProduct->name }}</h4></a>
                                <h5 class="featured_product_price_home">
                                    <span>{{ number_format($saleProduct->price * (100 - $saleProduct->promotion) / 100, '0', '', '.') }} đ</span>
                                    <del class="price_promotion">
                                        {{ number_format($saleProduct->price, '0', '', '.') }} đ
                                    </del>
                                </h5>
                                <div>
                                    @if($saleProduct->end_sale != NULL)
{{--                                        <span style="color: red">Kết thúc sau </span><span id="clock" style="color: #4c110f" data-countdown="{{ date('D ngày H:M:S', strtotime($saleProduct->end_sale) - strtotime($saleProduct->start_sale)) }}"></span>--}}
{{--                                        {{ dd(date('d-m-y H:i:s', strtotime($saleProduct->end_sale) - strtotime($saleProduct->start_sale))) }}--}}
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Feature Product Area =================-->

    @foreach($listCategories as $category)
    <section class="feature_product_area">
        <div class="main_box">
            <div class="container">
                <div class="feature_product_inner">
                    <div class="main_title">
                        <h2><a href="{{ route('frontend.home.category.ProductCategory', ['id' => $category->id, 'name' => $category->name]) }}">{{$category->name}}</a> <span style="font-size: 18px"><span class="lnr lnr-chevron-right"></span> <a href="{{ route('frontend.home.category.ProductCategory', ['id' => $category->id, 'name' => $category->name]) }}">All {{ $category->name }}</a></span></h2>
                        <p>Who are in extremely love with eco friendly system.</p>
                    </div>
                    <div class="feature_p_slider owl-carousel">
                        @php $listCategory = listCategory($category->id) @endphp
                        @foreach ($listCategory as $item)
                            @php $limitProducts = listLimitProduct($item->id) @endphp
                            @foreach ($limitProducts as $limitProduct)
                                <div class="item">
                                    <div class="f_p_item">
                                        @if ($limitProduct->featured != 0)
                                            <div class="box-new">
                                                <span class="text-new">
                                                    Mới
                                                </span>
                                            </div>
                                        @endif
                                        <div class="f_p_img">
                                            <a href="/{{$limitProduct->slug}}-{{$limitProduct->id}}.htm"><img class="img-fluid" src="img/{{ $limitProduct->img }}" alt=""></a>
                                            <div class="p_icon">
                                                <a href="#"><i class="fa fa-eye"></i></a>
                                                <a href="/{{$limitProduct->slug}}-{{$limitProduct->id}}.htm"><i class="lnr lnr-cart"></i></a>
                                            </div>
                                        </div>
                                        <a href="/{{$limitProduct->slug}}-{{$limitProduct->id}}.htm"><h4 style="color: #38383d">{{ $limitProduct->name }}</h4></a>
                                        <h5 class="featured_product_price_home">
                                            <span>{{ number_format($limitProduct->price * (100 - $limitProduct->promotion) / 100, '0', '', '.') }} đ</span>
                                        </h5>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach
@endsection


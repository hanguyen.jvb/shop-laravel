@extends('frontend.master.master')
@section('title', 'confirmation')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="" style="min-height: 500px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

    <!--================Order Details Area =================-->
    <section class="order_details p_120" style="margin-top: -100px">
        <div class="container">
            <h3 class="title_confirmation">Cảm ơn bạn. Đơn hàng của bạn đặt thành công. Vui lòng Kiểm tra Email</h3>
            <div class="row order_d_inner">
                <div class="col-lg-6">
                    <div class="details_item">
                        <h4>Thông tin khách hàng</h4>
                        <ul class="list">
                            <li><a href="#"><span>Họ và tên </span> : {{ $customerOrder->fullname }}</a></li>
                            <li><a href="#"><span>SĐT</span> : {{ $customerOrder->phone }}</a></li>
                            <li><a href="#"><span>Địa chỉ</span> : {{ $customerOrder->address}}</a></li>
                            <li><a href="#"><span>Email</span> : {{ $customerOrder->email}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="details_item">
                        <h4>Trạng thái thanh toán</h4>
                        <ul class="list">
                            <li><a href="#"><span>Tên shop</span> : ListAshop</a></li>
                            <li>
                                <a href="#">
                                    <span>Trạng thái</span> :
                                    @if($customerOrder->state == 0)
                                        {{ 'Đặt hàng' }}
                                    @endif
                                </a>
                            </li>
                            <li><a href="#"><span>Phản hồi</span> : Trong vòng 24h</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="order_details_table">
                <h2>Chi tiết đơn hàng</h2>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Tên sản phẩm</th>
                                <th scope="col">Đơn giá</th>
                                <th scope="col">Số lượng</th>
                                <th scope="col">Thành tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $totalPriceComplete = 0; @endphp
                            @foreach($customerOrder->customerOrdering as $order)
                                @php $totalPriceComplete += ($order->price * $order->quantity) @endphp
                                <tr>
                                    <td>
                                        <h5>{{ $order->name }}</h5>
                                    </td>
                                    <td>
                                        <h5>{{ number_format($order->price, '0', '', '.') }}</h5>
                                    </td>
                                    <td>
                                        <h5>{{ $order->quantity }}</h5>
                                    </td>
                                    <td>
                                        <h5>{{ number_format($order->price * $order->quantity, '0', '', '.') }}</h5>
                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>
                                    <h4>Tổng tiền</h4>
                                </td>
                                <td>
                                    <h5></h5>
                                </td>
                                <td>
                                    <p>{{ number_format($totalPriceComplete, '0', '', '.') }}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--================End Order Details Area =================-->
@endsection

@push('css')
    <link rel="stylesheet" href="frontend/css/checkout/checkout.css">
@endpush
@extends('frontend.master.master')
@section('title', 'checkout')
@section('content')
    <!--================Home Banner Area =================-->
    <section class="" style="min-height: 500px">
        <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
            <div style="margin-top: 50px">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--================End Home Banner Area =================-->

<!--================Checkout Area =================-->

<section class="checkout_area p_120" style="margin-top: -100px">
    <div class="container">
    	<form method="post" id="formCustomerOrder">
    		@csrf
		        <div class="billing_details">
		            <div class="row">
		                <div class="col-lg-4">
		                    <h3>Thông tin mua hàng</h3>
                            <div class="col-md-12 form-group">
		                            <input type="text" class="form-control" id="" @if(Auth::guard('client')->user()) value="{{ Auth::guard('client')->user()->fullname }}" @endif name="fullname" placeholder="Họ và tên">
{{--		                            <span class="placeholder" data-placeholder="Full name"></span>--}}
                                    @if ($errors->has('fullname'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                            {{ $errors->first('fullname') }}
                                        </div>
                                    @endif
		                        </div>
                            <div class="col-md-12 form-group ">
                                    <input type="text" class="form-control" id="email" @if(Auth::guard('client')->user()) value="{{ Auth::guard('client')->user()->email }}" @endif name="email" placeholder="Email">
                                    {{--		                            <span class="placeholder" data-placeholder="Email Address"></span>--}}
                                    @if ($errors->has('email'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                            <div class="col-md-12 form-group ">
		                            <input type="number" class="form-control" id="phone" name="phone" placeholder="Số điện thoại">
{{--		                            <span class="placeholder" data-placeholder="Phone number"></span>--}}
                                    @if ($errors->has('phone'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                            {{ $errors->first('phone') }}
                                        </div>
                                    @endif
		                        </div>
                            <div class="col-md-12 form-group ">
		                            <input type="text" class="form-control" id="add1" name="address" placeholder="Địa chỉ">
{{--		                            <span class="placeholder" data-placeholder="Address"></span>--}}
                                    @if ($errors->has('address'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                            {{ $errors->first('address') }}
                                        </div>
                                    @endif
		                        </div>
{{--                            <div class="col-md-12 form-group ">--}}
{{--                                    <input type="text" class="form-control" name="city" placeholder="Tỉnh thành">--}}
{{--                                    --}}{{--		                            <span class="placeholder" data-placeholder="Address"></span>--}}
{{--                                    @if ($errors->has('city'))--}}
{{--                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">--}}
{{--                                            {{ $errors->first('city') }}--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
{{--                            <div class="col-md-12 form-group ">--}}
{{--                                    <input type="text" class="form-control" name="district" placeholder="Quận huyện">--}}
{{--                                    --}}{{--		                            <span class="placeholder" data-placeholder="Address"></span>--}}
{{--                                    @if ($errors->has('district'))--}}
{{--                                        <div class="alert alert-danger" role="alert" style="margin-top: 5px;">--}}
{{--                                            {{ $errors->first('district') }}--}}
{{--                                        </div>--}}
{{--                                    @endif--}}
{{--                                </div>--}}
                            <div class="col-md-12 form-group ">
                                    <textarea class="form-control" rows="3" placeholder="Ghi chú" name="description"></textarea>
                                </div>
		                </div>
                        <div class="col-lg-5">
                            <div class="order_box" style="margin-top: 60px">
                                <h2>Đơn hàng ({{ Cart::Content()->count() }} sản phẩm)</h2>
                                    <table class="table">
                                        <tbody>
                                            @php $totalPriceCheck = 0; @endphp
                                            @foreach($cartsCheckout as $cartCheck)
                                                @php $totalPriceCheck += ($cartCheck->price * ((100 - $cartCheck->options->promotion) / 100)) * $cartCheck->qty @endphp
                                            <tr>
                                                <td><img src="img/{{ $cartCheck->options->img }}" width="70" height="70"></td>
                                                <td>{{ $cartCheck->name }}</td>
                                                <td>{{ $cartCheck->qty }}</td>
                                                <td>{{ number_format($cartCheck->price * ((100 - $cartCheck->options->promotion) / 100) * $cartCheck->qty, '0', '', '.') }}đ</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                <ul class="list">
                                    <div >

                                    </div>
                                    <div style="width: 60%">

                                    </div>
                                    <li><span class="middle"></span> <span class="last"></span></li>
                                </ul>
                                <ul class="list list_2">
                                    <li>Tổng cộng <input type="text" name="totalPrice" value="" disabled style="border: none; margin-left: 70px">
                                        <span>{{ number_format($totalPriceCheck, '0', '', '.') }} đ</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
		                <div class="col-lg-3">
		                    <div class="order_box" style="margin-top: 60px">
		                        <h2>Đặt hàng</h2>
		                        <ul class="list">
		                            <li><i class="fa fa-envelope" style="color: red"></i> <span>Thông tin chi tiết đơn hàng sẽ gửi về Email của bạn</span>
		                            </li>
		                            <div >

		                            </div>
		                            <div style="width: 60%">

		                            </div>
		                            <li><span class="middle"></span> <span class="last"></span></li>
		                        </ul>
{{--		                        <ul class="list list_2">--}}
{{--		                            <li>Đặt hàng <input type="text" name="totalPrice" value="" disabled style="border: none; margin-left: 70px"></li>--}}
{{--		                        </ul>--}}
		                        	<a class="btn btn-default" href="{{ route('home.cart.list') }}" style="margin-left: -15px"> <span class="lnr lnr-chevron-left"></span> Quay về giỏ hàng</a>
                                    <button class="main_btn" type="submit">Đặt hàng</button>
		                    </div>
		                </div>
		            </div>
		        </div>
    	</form>
    </div>
</section>
<!--================End Checkout Area =================-->
@endsection

@push('script')
    <script src="frontend/js/checkout/validateFormCustomerOrder.js"></script>
@endpush

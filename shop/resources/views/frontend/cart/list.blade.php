@extends('frontend.master.master')
@section('title', 'list cart')
@section('content')
{{--<section class="banner_area">--}}
{{--    <div class="banner_inner d-flex align-items-center">--}}
{{--        <div class="container">--}}
{{--            <div class="banner_content text-center">--}}
{{--                <h2>Shopping Cart</h2>--}}
{{--                <div class="page_link">--}}
{{--                    <a href="index.html">Home</a>--}}
{{--                    <a href="cart.html">Cart</a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<!--================End Home Banner Area =================-->
<section class="" style="min-height: 500px">
    <div class="banner_inner d-flex align-items-center" style="min-height: 600px; height: 600px">
        <div style="margin-top: 50px">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="frontend/img/banner/slider_5.png" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="frontend/img/banner/slider_4.png" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="frontend/img/banner/slider_2.png" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="frontend/img/banner/slider_3.png" alt="four slide">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>
<!--================Cart Area =================-->
@if($carts->count() > 0)
    <section class="cart_area" style="margin-top: -100px">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Ảnh sản phẩm</th>
                            <th scope="col">Tên sản phẩm</th>
                            <th scope="col">Đơn giá</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">Thành tiền</th>
                            <th scope="col">Xóa</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php $totalPrice = 0; @endphp
                        @foreach ($carts as $cart)
                            @php $totalPrice += ($cart->price * ((100 - $cart->options->promotion) / 100)) * $cart->qty @endphp
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="img/{{ $cart->options->img }}" width="150" height="150" alt="product_cart">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5 style="font-size: 19px">{{ $cart->name }}</h5><br>
                                    <span>
                                        @if (is_array($cart->options->attr) || is_object($cart->options->attr))
                                            @foreach ($cart->options->attr as $key => $value)
                                                {{ $key }} : {{$value}} <br/>
                                            @endforeach
                                        @endif
                                    </span>
                                </td>
                                <td>
                                    <h5>
                                        {{-- {{ number_format($cart->price, '0', '', '.') }} đ --}}
                                        {{ number_format($cart->price * ((100 - $cart->options->promotion) / 100), '0', '', '.') }} đ
                                    </h5>
                                </td>
                                <td>
                                    <div class="product_count">
                                        <input onchange="updateQty('{{$cart->rowId}}', this.value)" type="number" name="qty" id="{{$cart->id}}" maxlength="12" value="{{ $cart->qty }}"title="Quantity:" class="input-text qty" {{--onchange="changeQuantity({!! $cart->id !!}, $(this).val(),{!!$cart->price!!})"--}}>
                                    </div>
                                </td>
                                <td>
                                    <h5 id="price-product{{$cart->id}}" class="price-product-change">
                                        {{-- {{ number_format($cart->price * $cart->qty, '0', '', '.') }} đ --}}
                                        {{ number_format($cart->price * ((100 - $cart->options->promotion) / 100) * $cart->qty, '0', '', '.') }} đ
                                    </h5>
                                </td>
                                <td>
                                    <a onclick="return delCart('{{ $cart->name }}')" href="{{ route('home.cart.remove', $cart->rowId) }}"><span style="font-size: 20px; margin-left: 10px" class="lnr lnr-cross"></span></a>
                                </td>
                            </tr>
                        @endforeach

                        <tr>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <h5>Tổng tiền:</h5>
                            </td>
                            <td>
                                <h5 id="total-price">{{ number_format($totalPrice, '0', '', '.') }} đ</h5>
                            </td>
                        </tr>



                        <tr class="out_button_area">
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <div class="checkout_btn_inner">
                                    <a class="gray_btn" href="{{ route('frontend.home.list') }}">Tiếp tục mua hàng</a>
                                    <a class="main_btn" href="{{ route('home.cart.checkout.create') }}">Thực hiện thanh toán</a>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    @else
        <div style="margin-left: 90px; margin-top: 20px">
            <h1 style="color: #323c3f; font-size: 30px; font-weight: bold">Giỏ hàng</h1>
            <p style="color: #575454">Không có sản phẩm nào trong giỏ hàng. Quay lại
                <a href="{{ route('frontend.home.list') }}" style="color: red">cửa hàng</a>
                để tiếp tục mua sắm.
            </p>
        </div>
@endif
<!--================End Cart Area =================-->
<script>
    function delCart(name)
    {
        return confirm('Bạn muốn xóa sản phẩm ' + name + ' ?');
    }

</script>
@endsection

@push('script')
<script src="frontend/js/cart/update-qty.js"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/process-order.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'process')
@section('content')
<div class="header-process-order" style="border-bottom: 1px solid #98b9ed">
    <div style="margin-bottom: 8px">
        <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
        <a href="{{ route('backend.order.list') }}"><span class="back-page">Orders</span></a> /
        <span class="title-process-order">Process</span>
    </div>
</div>
<div class="panel panel-profile">
    <div class="clearfix">
        <!-- LEFT COLUMN -->
        <div class="profile-left">
            <!-- PROFILE HEADER -->
            <div class="profile-header">
                <div class="overlay"></div>
                <div class="profile-main">
                    <h3 class="name">{{ $orderId->fullname }}</h3>
                    <span class="online-status status-available">Available</span>
                </div>
                <div class="profile-stat">
                    <div class="row">
                        <div class="col-md-6 stat-item">
                            {{ $productOrders->count() }} <span>products</span>
                        </div>
                        <div class="col-md-6 stat-item">
                            {{ $productOrders->sum('quantity') }} <span>quantity</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE HEADER -->
            <!-- PROFILE DETAIL -->
            <div class="profile-detail">
                <div class="profile-info">
                    <h4 class="heading">Info Order</h4>
                    <ul class="list-unstyled list-justify">
                        <li>Fullname <span>{{ $orderId->fullname }}</span></li>
                        <li>Mobile <span>{{ $orderId->phone }}</span></li>
                        <li>Email <span>{{ $orderId->email }}</span></li>
                        <li>Address <span>{{ $orderId->address }}</span></li>
                    </ul>
                </div>
            </div>
            <!-- END PROFILE DETAIL -->
        </div>
        <!-- END LEFT COLUMN -->
        <!-- RIGHT COLUMN -->
        <div class="profile-right">
            <h4 class="heading">{{ $orderId->fullname }}</h4>
            <!-- AWARDS -->
            <div class="table-responsive">
                <form action="#" method="post" onsubmit="return deleteConfirm();">
                    @csrf
                    <table id="table-list-order" class="table table-bordred table-striped">
                        <thead>
                            <th scope="col">Image</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                        </thead>
                        <tbody>
                            @php $totalOrder = 0; @endphp
                            @foreach ($productOrders as $productOrder)
                                @php $totalOrder += ($productOrder->price * $productOrder->quantity) @endphp
                            <tr>
                                <td><img src="/img/{{ $productOrder->img }}" width="100" height="100" alt=""></td>
                                <td>{{ $productOrder->name }}</td>
                                <td>{{ number_format($productOrder->price, 0, '', '.') }}đ</td>
                                <td>{{ $productOrder->quantity }}</td>
                                <td>{{ number_format($productOrder->price * $productOrder->quantity, 0, '', '.') }}đ</td>
                            </tr>
                            @endforeach
                            <tr>
                                <th scope="col">Tổng tiền</th>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="color: red">{{ number_format($totalOrder, '0', '', '.') }}đ</td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('backend.order.processed', $orderId) }}" class="btn btn-success">Shipping</a>
                    <a href="{{ route('backend.order.cancelProcess', $orderId) }}" class="btn btn-danger">Cancel</a>
                </form>
            </div>
            <!-- END TABBED CONTENT -->
        </div>
        <!-- END RIGHT COLUMN -->
    </div>
</div>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/process-order.css') }}">
    <link rel="stylesheet" href="{{ asset('css/backend/list-order.css') }}">
@endpush
@extends('backend.master.master')
@section('title' ,'list order')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error-warning'))
        <div class="alert alert-warning alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('error-warning') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="header-process-order" style="border-bottom: 1px solid #98b9ed">
        <div style="margin-bottom: 8px">
            <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
            <a href="{{ route('backend.order.list') }}"><span class="back-page">Orders</span></a> /
            <a href="{{ route('backend.order.listProcessed') }}"><span class="back-page">Orders Shipping</span></a> /
            <a href="{{ route('backend.order.completedOrder') }}"><span class="back-page">Complete Order</span></a>
        </div>
    </div>

{{--    <form action="{{ route('backend.order.searchOrder') }}" method="post" class="navbar-form navbar-left">--}}
{{--        <div class="input-group">--}}
{{--            <input type="text" name="key_search_order" id="key_search_order" class="form-control input-search-order" placeholder="Search order...">--}}
{{--            <div id="resultSearchOrder"></div>--}}
{{--        </div>--}}
{{--    </form>--}}

    <div class="row">
        <div class="col-md-12">
            <h4>Orders</h4><span>Kết quả tìm kiếm với từ khóa <span style="color: red">"{{ $keyOrder }}"</span></span> /
            <span>
                @if(count($resultSearchAllOrder) > 0)
                    Kết quả tìm kiếm phù hợp
                @else
                    Không tìm thấy bất kỳ kết quả nào với từ khóa trên
                @endif
            </span>
            @if(count($resultSearchAllOrder) > 0)
            <div class="table-responsive">
                <form action="{{ route('backend.order.delete') }}" method="post" onsubmit="return deleteConfirm();">
                    @csrf
                    <table id="table-list-order" class="table table-bordred table-striped">
                        <thead>
                        <th scope="col">Fullname</th>
                        <th scope="col">Address</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Order Date</th>
                        <th scope="col">Process</th>
                        <th>
                            <button type="submit" class="btn-delete">
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                        </th>
                        </thead>
                        <tbody>
                        @foreach ($resultSearchAllOrder as $searchOrder)
                            <tr>
                                <td>{{ $searchOrder->fullname }}</td>
                                <td>{{ $searchOrder->address }}</td>
                                <td>{{ $searchOrder->email }}</td>
                                <td>{{ $searchOrder->phone }}</td>
                                <td>{{ $searchOrder->updated_at }}</td>
                                <td><a href="{{ route('backend.order.process', $searchOrder->id) }}" class="btn btn-primary btn-process-order"><span class="glyphicon glyphicon-pencil icon-process-order"></span> Process</a></td>
                                <td><input class="checkbox-delete" name="deleteOrder[]" type="checkbox" value="{{ $searchOrder->id }}"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
            <nav aria-label="Page navigation example">
                {{ $resultSearchAllOrder->links() }}
            </nav>
            @endif
        </div>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/product/confirmDelete.js') }}"></script>
    <script src="{{ asset('js/order/validateSearch.js') }}"></script>
@endpush

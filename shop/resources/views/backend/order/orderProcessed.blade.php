@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/list-order.css') }}">
    <link rel="stylesheet" href="{{ asset('css/backend/process-order.css') }}">
@endpush
@extends('backend.master.master')
@section('title' ,'list order')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error-warning'))
        <div class="alert alert-warning alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('error-warning') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="header-process-order" style="border-bottom: 1px solid #98b9ed">
        <div style="margin-bottom: 8px">
            <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
            <a href="{{ route('backend.order.list') }}"><span class="back-page">Orders</span></a> /
            <span class="title-process-order">Orders Shipping</span> /
            <a href="{{ route('backend.order.completedOrder') }}"><span class="back-page">Complete Order</span></a>
        </div>
    </div>

{{--    <form action="#" method="get" class="navbar-form navbar-left">--}}
{{--        <div class="input-group">--}}
{{--            <input type="text" name="key_search" class="form-control input-search-order" placeholder="Search order...">--}}
{{--            <span class="input-group-btn"><button type="submit" class="btn btn-primary">Go</button></span>--}}
{{--        </div>--}}
{{--    </form>--}}

	<div class="row">
        <div class="col-md-12">
            <h4>Orders Shipping</h4>
            <div class="table-responsive">
                <form action="{{ route('backend.processed.destroyProcessed') }}" method="post" onsubmit="return deleteConfirm();">
                    @csrf
                    <table id="table-list-order" class="table table-bordred table-striped">
                        <thead>
                            <th scope="col">Fullname</th>
                            <th scope="col">Address</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Completed</th>
                            <th>
                                <button type="submit" class="btn-delete">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button>
                            </th>
                        </thead>
                        <tbody>
                            @foreach ($orderProcessed as $processed)
                                <tr>
                                    <td>{{ $processed->fullname }}</td>
                                    <td>{{ $processed->address }}</td>
                                    <td>{{ $processed->email }}</td>
                                    <td>{{ $processed->phone }}</td>
                                    <td><a href="{{ route('backend.order.completeOrdering', $processed->id) }}" style="margin-left: 35px"><i class="glyphicon glyphicon-ok" style="color: green"></i></a></td>
                                    <td><input class="checkbox-delete" name="deleteProcessed[]" type="checkbox" value="{{ $processed->id }}"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
            <nav aria-label="Page navigation example">
                {{ $orderProcessed->links() }}
            </nav>
        </div>
    </div>
@endsection

@push('script')
<script src="{{ asset('js/product/confirmDelete.js') }}"></script>
@endpush

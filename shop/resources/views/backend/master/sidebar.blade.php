<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="{{ route('backend.home.list') }}" class="active"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                <li><a href="{{ route('backend.user.list') }}" class=""><i class="lnr lnr-users"></i> <span>Users</span></a></li>
                {{-- <li><a href="{{ route('backend.category.list') }}" class=""><i class="lnr lnr-briefcase"></i> <span>Category</span></a></li> --}}
                <li>
                    <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-briefcase"></i> <span>Categories</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages" class="collapse">
                        <ul class="nav">
                            @foreach ($categories as $category)
                                @if ($category->parent == 0)
                                    <li>
                                        <a href="#{{ $category->name }}" data-toggle="collapse" class="collapsed"><i class="lnr lnr-tag"></i> <span>{{ $category->name }}</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                                        <div id="{{ $category->name }}" class="collapse">
                                            <ul class="nav">
                                                @foreach ($categories as $item)
                                                    @if ($item->parent == $category->id)
                                                        <li style="margin-left: 31px"><a href="{{ route('backend.product.list', $item->id) }}"><span class="lnr lnr-chevron-right"></span> {{ $item->name }}</a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </li>
                <li><a href="{{ route('backend.order.list') }}" class=""><i class="lnr lnr-cart"></i> <span>Orders</span></a></li>
                <li><a href="{{ route('backend.product.attribute.list') }}" class=""><i class="lnr  lnr-map"></i> <span>Attributes</span></a></li>
            </ul>
        </nav>
    </div>
</div>

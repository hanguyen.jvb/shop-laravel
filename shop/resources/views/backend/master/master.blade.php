{{-- <!DOCTYPE html>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>--}}
{{--<script src="{{ asset('js/bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/lumino.glyphs.js') }}"></script>--}}
{{--<script src="{{ asset('js/all.min.js') }}"></script>--}}
{{--<script src="{{ asset('js/jquery.validate.min.js') }}"></script>--}}
{{--@stack('script')--}}

<!doctype html>
<html lang="en">

<head>
	<title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/linearicons/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/chartist/css/chartist-custom.css') }}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon.png') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
    @stack('css')
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
        @include('backend.master.header')
		<!-- END NAVBAR -->
        <!-- LEFT SIDEBAR -->
        @include('backend.master.sidebar')
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					@yield('content')
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
        @include('backend.master.footer')
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery-ui.js') }}"></script>
    <script src="{{ asset('frontend/js/popper.js') }}"></script>
	<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets/scripts/klorofil-common.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    @stack('script')

</body>
</html>
{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $("#key_search_user").keyup(function () {--}}
{{--            var keySearchUser = $(this).val();--}}

{{--            if (keySearchUser != '' && keySearchUser.trim() != "") {--}}
{{--                // console.log(keySearchUser);--}}
{{--                var _token = $('input[name="_token"]').val();--}}

{{--                $.ajax({--}}
{{--                    url: "{{ route('backend.user.searchUser') }}",--}}
{{--                    method: "POST",--}}
{{--                    data: {--}}
{{--                        keySearchUser:keySearchUser,--}}
{{--                        _token:_token--}}
{{--                    },--}}
{{--                    success: function (data) {--}}
{{--                        console.log(data);--}}
{{--                        $("#resultSearchUserAjax").fadeIn();--}}
{{--                        $("#resultSearchUserAjax").html(data);--}}
{{--                    }--}}
{{--                });--}}
{{--            } else {--}}
{{--                $("#resultSearchUserAjax").fadeOut();--}}
{{--            }--}}
{{--        });--}}
{{--    });--}}
{{--</script>--}}

<script>
    $(document).ready(function () {
        $("#key_search_product").keyup(function () {
            var keySearchProduct = $(this).val();

            if (keySearchProduct != '' && keySearchProduct.trim() != "") {
                // console.log(keySearchUser);
                var _token = $('input[name="_token"]').val();

                $.ajax({
                    url: "{{ route('backend.product.searchProduct') }}",
                    method: "POST",
                    data: {
                        keySearchProduct:keySearchProduct,
                        _token:_token
                    },
                    success: function (data) {
                        console.log(data);
                        $("#resultSearchProductAjax").fadeIn();
                        $("#resultSearchProductAjax").html(data);
                    }
                });
            } else {
                $("#resultSearchProductAjax").fadeOut();
            }
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#key_search_order").keyup(function () {
            var keySearchOrder = $(this).val();
            console.log(keySearchOrder);
            if (keySearchOrder != '' && keySearchOrder.trim() != "") {
                // console.log(keySearchUser);
                var _token = $('input[name="_token"]').val();

                $.ajax({
                    url: "{{ route('backend.order.searchOrder') }}",
                    method: "POST",
                    data: {
                        keySearchOrder:keySearchOrder,
                        _token:_token
                    },
                    success: function (data) {
                        console.log(data);
                        $("#resultSearchOrder").fadeIn();
                        $("#resultSearchOrder").html(data);
                    }
                });
            } else {
                $("#resultSearchOrder").fadeOut();
            }
        });
    });
</script>

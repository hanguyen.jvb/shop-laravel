{{-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="{{ route('backend.home.list') }}">JVB Shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" href="#">
                        <svg class="glyph stroked male user icon-user-header"><use xlink:href="#stroked-male-user"/></svg>
                        <span class="fullname-header">
                            @if (Auth::check())
                                {{ Auth::user()->fullname }}
                            @endif
                        </span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" @if (Auth::user()->level == 1) href="{{ route('backend.home.setting', Auth::user()->id) }} @endif">
                            <svg class="glyph stroked gear icon-dropdown-setting">
                                <use xlink:href="#stroked-gear"/>
                            </svg>
                            <span>Setting Acount</span>
                        </a>
                        <a class="dropdown-item" href="{{ route('backend.home.admin', Auth::user()->id) }}">
                            <svg class="glyph stroked eye icon-dropdown-show">
                                <use xlink:href="#stroked-eye"/>
                            </svg>
                            <span>Profile</span>
                        </a>
                        <a class="dropdown-item" href="{{ route('backend.home.logout') }}">
                            <svg class="glyph stroked cancel icon-dropdown-logout">
                                <use xlink:href="#stroked-cancel"/>
                            </svg>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav> --}}

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="{{ route('backend.home.list') }}"><img src="{{ asset('assets/img/logo-dark.png') }}" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div id="navbar-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/img/{{ Auth::user()->img }}" class="img-circle" alt="Avatar">
                        <span>
                            @if (Auth::check())
                                {{ Auth::user()->fullname }}
                            @endif
                        </span>
                        <i class="icon-submenu lnr lnr-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('backend.home.setting', Auth::user()->id) }}"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
                        <li><a href="{{ route('backend.home.logout') }}"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

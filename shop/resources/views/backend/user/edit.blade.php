{{-- @push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/edit-user.css') }}">
@endpush

@extends('backend.master.master')
@section('title', 'edit user')
@section('content')
    <div class="main-edit-user">
        <h2>Edit User</h2>
        <form type='file' class="form" method="post" id="editUserForm" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-3">
                    <div class="text-center">
                        <div class="empty-text">
                            <img src="/img/{{ $idUser->img }}" class="avatar img-circle img-thumbnail">
                            <input type="file" name="fileUpload" id="fileToUpload" class="text-center center-block input-file-upload">
                        </div>
                        <span class="error-upload">
                            @if ($errors->has('fileToUpload'))
                                {{ $errors->first('fileToUpload') }}
                            @endif
                        </span>

                    </div><br>
                </div>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="fullname"><h4>Fullname</h4></label>
                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="enter your fullname" title="enter your fullname" value="{{  $idUser->fullname }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                        <label for="username"><h4>Username</h4></label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="enter your username" title="enter your username" value="{{  $idUser->username }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="email"><h4>Email</h4></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="enter your email" title="enter your phone email" value="{{  $idUser->email }}">
                            <span class="error-email-exists">
                                @if ($errors->has('email'))
                                    {{ $errors->first('email') }}
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="password"><h4>Update Password</h4></label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="enter your password" title="enter your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="password"><h4>Confirm Password</h4></label>
                            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="confirm password" title="confirm password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="phone"><h4>Phone</h4></label>
                            <input type="number" class="form-control" name="phone" id="phone" placeholder="enter your phone" title="enter your phone" value="{{  $idUser->phone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="address"><h4>Address</h4></label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="somewhere" title="enter a location" value="{{  $idUser->address }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="address"><h4>Role</h4></label>
                            <select class="form-control" name="role">
                                <option value="1" @if ($idUser->level == 1) {{ 'selected' }} @endif>Admin</option>
                                <option value="2" @if ($idUser->level == 2) {{ 'selected' }} @endif>User</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <br>
                            <button class="btn btn-lg btn-primary" type="submit">Update</button>
                            <button class="btn btn-lg btn-secondary" type="reset">Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/user/editValidate.js') }}"></script>
    <script src="{{ asset('js/acount/prevImage.js') }}"></script>
@endpush --}}
@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/edit-user.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'edit user')
@section('content')
<div class="header-edit-user">
    <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
    <a href="{{ route('backend.user.list') }}"><span class="back-page">User</span></a>
</div>

<form class="form" method="post" id="editUserForm" enctype="multipart/form-data">
    @csrf
        <div class="row">
  	        <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <div class="empty-text">
                        <img src="/img/{{ $idUser->img }}" width="300" height="300" class="avatar img-thumbnail">
                        <input style="margin-left: 18px; margin-top: 10px" type="file" width="200" height="200" name="fileUpload" class="text-center center-block input-file-upload">
                    </div>
                    <span class="error-upload" style="color: red">
                        @if ($errors->has('fileUpload'))
                            {{ $errors->first('fileUpload') }}
                        @endif
                    </span>

                </div><br>
            </div><!--/col-3-->

    	    <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left: 30px"><a data-toggle="tab" href="#home">Edit</a></li>
                </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="fullname"><h4>Full name</h4></label>
                                <input type="text" class="form-control" name="fullname" value="{{  $idUser->fullname }}">
                                @if ($errors->has('fullname'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('fullname') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="username"><h4>User name</h4></label>
                                <input type="text" class="form-control" name="username" value="{{  $idUser->username }}">
                                @if ($errors->has('username'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Email</h4></label>
                                <input type="text" class="form-control" name="email" value="{{  $idUser->email }}">
                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="phone"><h4>Phone</h4></label>
                                <input type="number" class="form-control" name="phone" value="{{  $idUser->phone }}">
                                @if ($errors->has('phone'))
                                <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                    {{ $errors->first('phone') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="password"><h4>Password</h4></label>
                                <input type="password" id="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="confirm password"><h4>Confirm Password</h4></label>
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                @if ($errors->has('confirm_password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('confirm_password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="address"><h4>Address</h4></label>
                                <input type="address" class="form-control" name="address" value="{{  $idUser->address }}">
                                @if ($errors->has('address'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('address') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="address"><h4>Role</h4></label>
                                <select class="form-control" name="role">
                                    <option value="1"  @if ($idUser->level == 1) {{ 'selected' }} @endif>Admin</option>
                                    <option value="2"  @if ($idUser->level == 2) {{ 'selected' }} @endif>User</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                        </div>
                    <hr>
                </div><!--/tab-pane-->
            </div><!--/col-9-->
        </div><!--/row-->
    </form>
@endsection

@push('script')
    <script src="{{ asset('js/user/editValidate.js') }}"></script>
    <script src="{{ asset('js/user/prevImg.js') }}"></script>
@endpush

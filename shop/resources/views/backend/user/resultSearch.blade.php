@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/list-user.css') }}">
@endpush

@extends('backend.master.master')
@section('title', 'result user')
@section('content')
    <div class="title-search">
        <h2>Result Search User</h2>
    </div>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Avatar</th>
                <th scope="col">Fullname</th>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Phone</th>
                <th scope="col">Address</th>
                <th scope="col">Role</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($search as $searchUser)
                <tr>
                    <th scope="row">{{ $searchUser->id }}</th>
                    <th><img src="/img/{{ $searchUser->img }}" width="50" height="50" class="rounded-circle"></th>
                    <td>{{ $searchUser->fullname }}</td>
                    <td>{{ $searchUser->username }}</td>
                    <td>{{ $searchUser->email }}</td>
                    <td>{{ $searchUser->phone }}</td>
                    <th>{{ $searchUser->address }}</th>
                    <th>
                        @if ($searchUser->level == 1)
                            <i style="color: yellow" class="glyphicon glyphicon-star"></i>
                        @elseif ($searchUser->level == 2)
                            <i style="color: black" class="glyphicon glyphicon-star"></i>
                        @endif
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

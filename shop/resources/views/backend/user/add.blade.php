{{-- @push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/add-user.css') >
@endpush

@extends('backend.master.master')
@section('title', 'add user')
@section('content')
    <div class="main-add-user">
        <h2>Add User</h2>
        <form class="form" method="post" id="addUserForm" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-3">
                    <div class="text-center">
                        <input type="file" name="fileToUpload" id="fileToUpload" data-type='image' class="text-center center-block input-file-upload">
                        <span class="error-upload">
                            @if ($errors->has('fileToUpload'))
                                {{ $errors->first('fileToUpload') }}
                            @endif
                        </span>
                        <div class="empty-text">Image previous here</div>
                    </div><br>
                </div>
                <div class="col-lg-9">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="fullname"><h4>Fullname</h4></label>
                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="enter your fullname" title="enter your fullname">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                        <label for="username"><h4>Username</h4></label>
                            <input type="text" class="form-control" name="username" id="username" placeholder="enter your username" title="enter your username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="email"><h4>Email</h4></label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="enter your email" title="enter your phone email"                           <span class="error-email-exists">
                                @if ($errors->has('email'))
                                    {{ $errors->first('email') }}
                                @endif
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="password"><h4>Password</h4></label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="enter your password" title="enter your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="password"><h4>Confirm Password</h4></label>
                            <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="confirm password" title="confirm password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="phone"><h4>Phone</h4></label>
                            <input type="number" class="form-control" name="phone" id="phone" placeholder="enter your phone" title="enter your phone"                       </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="address"><h4>Address</h4></label>
                            <input type="text" class="form-control" name="address" id="address" placeholder="somewhere" title="enter a location"
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="address"><h4>Role</h4></label>
                            <select class="form-control" name="role">
                                <option                            <option                       </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <br>
                            <button class="btn btn-lg btn-success" type="submit">Add</button>
                            <button class="btn btn-lg btn-secondary" type="reset">Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/user/addValidate.js') ></script>
    <script src="{{ asset('js/acount/prevImage.js') ></script>
@endpush --}}
@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/add-user.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'add user')
@section('content')
<div class="header-add-user">
    <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
    <a href="{{ route('backend.user.list') }}"><span class="back-page">User</span></a>
</div>

<form class="form" method="post" id="addUserForm" enctype="multipart/form-data">
    @csrf
        <div class="row">
  	        <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <input type="file" name="fileToUpload" id="fileToUpload" data-type='image' class="text-center center-block input-file-upload">
                    <span style="color: red; margin-left: -20px" class="error-upload">
                        @if ($errors->has('fileToUpload'))
                            {{ $errors->first('fileToUpload') }}
                        @endif
                    </span>
                    <div class="empty-text" style="margin-left: -15px; margin-top: 10px; color: #17a2b8">Upload Avatar</div>
                </div><br>
            </div><!--/col-3-->

    	    <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left: 30px"><a data-toggle="tab" href="#home">Add</a></li>
                </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="fullname"><h4>Full name</h4></label>
                                <input type="text" class="form-control" name="fullname" value="{{ old('fullname') }}">
                                @if ($errors->has('fullname'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('fullname') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="username"><h4>User name</h4></label>
                                <input type="text" class="form-control" name="username" value="{{ old('username') }}">
                                @if ($errors->has('username'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Email</h4></label>
                                <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="phone"><h4>Phone</h4></label>
                                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                    {{ $errors->first('phone') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="password"><h4>Password</h4></label>
                                <input type="password" id="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="confirm password"><h4>Confirm Password</h4></label>
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                @if ($errors->has('confirm_password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('confirm_password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="address"><h4>Address</h4></label>
                                <input type="address" class="form-control" name="address" value="{{ old('address') }}">
                                @if ($errors->has('address'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('address') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="address"><h4>Role</h4></label>
                                <select class="form-control" name="role">
                                    <option value="1">Admin</option>
                                    <option value="2">User</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                        </div>
                    <hr>
                </div><!--/tab-pane-->
            </div><!--/col-9-->
        </div><!--/row-->
    </form>
@endsection

@push('script')
    <script src="{{ asset('js/user/addValidate.js') }}"></script>
    <script src="{{ asset('js/user/addPrevImg.js') }}"></script>
@endpush

{{-- @push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/list-user.css') }}">
@endpush

@extends('backend.master.master')
@section('title', 'list user')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible fade show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="search-user">
        <form action="{{ route('backend.user.search') }}" method="get">
            <input type="text" placeholder="Enter your keyword" name="key_search">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
    @if (Auth::user()->level ==1)
        <div class="link-add-user">
            <a href="{{ route('backend.user.add') }}" class="btn btn-primary" style="color:#ffffff">Add User +</a>
        </div>
    @endif

    <form action="{{ route('backend.user.delete') }}" method="post" onsubmit="return deleteConfirm();">
        @csrf
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">id</th>
                    <th scope="col">Avatar</th>
                    <th scope="col">Fullname</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone</th>
                    <th scope="col">Address</th>
                    @if (Auth::user()->level == 1)
                        <th scope="col">Role</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    @elseif (Auth::user()->level == 2)
                        <th scope="col">Role</th>
                        <th scope="col">Show</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <th><img src="/img/{{ $user->img }}" width="50" height="50" class="rounded-circle"></th>
                        <td>{{ $user->fullname }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        @if (Auth::user()->level == 1)
                            <td>
                                <a href="{{ route('backend.user.setIsAdmin', $user->id) }}" class="btn btn-info">set Admin</a>
                                <a style="margin-top:5px" href="{{ route('backend.user.removeAdmin', $user->id) }}" class="btn btn-danger">remove admin</a>
                            </td>
                        @elseif (Auth::user()->level == 2)
                        <td>
                            @if ($user->level == 1)
                                <i class="fas fa-star star-icon"></i> <i class="fas fa-star star-icon"></i>
                        </td>
                            @elseif ($user->level == 2)
                                <i class="fas fa-star star-icon"></i>
                            @endif
                            <td>
                                <a href="" class="btn btn-warning">Show</a>
                            </td>
                        @endif

                        @if (Auth::user()->level == 1)
                            <td>
                                <a href="{{ route('backend.user.edit', $user->id) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                            <td><input name="deleteUser[]" type="checkbox" value="{{ $user->id }}"></td>
                        @endif
                    </tr>
                @endforeach
            </tbody>

            @if (Auth::user()->level ==1)
                <input type="submit" name="delete-student" id="delete-student" value="Delete User">
            @endif
        </table>
    </form>
    <nav aria-label="Page navigation example" style="margin-left: 200px;">
       {{ $users->links() }}
    </nav>
@endsection

@push('script')
    <script src="{{ asset('js/user/confirmDelete.js') }}"></script>
@endpush --}}
@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/list-user.css') }}">
@endpush
@extends('backend.master.master')
@section('title' ,'list user')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error-warning'))
        <div class="alert alert-warning alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('error-warning') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <div class="header-process-order">
        <div style="margin-bottom: 8px">
            <a href="{{ route('backend.home.list') }}"><span style="font-size: 19px" class="lnr lnr-home"></span></a> /
            <span class="back-page" style="font-size: 19px">Users</span>
        </div>
    </div>

{{--    <form action="{{ route('backend.user.searchUser') }}" method="post" class="navbar-form navbar-left">--}}
{{--        @csrf--}}
{{--            <input type="text" name="key_search_user" id="key_search_user" class="form-control input-search-user" placeholder="Search user...">--}}
{{--            <div id="resultSearchUserAjax"></div>--}}
{{--    </form>--}}
    @if (Auth::guard('web')->user()->level == 1)
        <a class="btn btn-info btn-add-user" href="{{ route('backend.user.add') }}">+ New</a>
    @endif
	<div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <form action="{{ route('backend.user.delete') }}" method="post" onsubmit="return deleteConfirm();">
                    @csrf
                    <table id="table-list-user" class="table table-bordred table-striped">
                        <thead>
                            <th scope="col">id</th>
                            <th scope="col">Avatar</th>
                            <th scope="col">Fullname</th>
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Address</th>
                            <th scope="col">Role</th>
                            @if (Auth::guard('web')->user()->level == 1)
                                <th scope="col">Set</th>
                                <th scope="col">Edit</th>
                                <th>
                                    <button type="submit" class="btn-delete">
                                        <i class="glyphicon glyphicon-trash"></i>
                                    </button>
                                </th>
                            @endif
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <th><img src="{{ asset('img/'.$user->img) }}" width="52" height="52" class="img-circle"></th>
                                    <td>{{ $user->fullname }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td>
                                        @if ($user->level == 1)
                                            <span style="color: red">Admin</span>
                                        @else
                                            <span style="color: #17a2b8">User</span>
                                        @endif
                                    </td>
                                    @if (Auth::guard('web')->user()->level == 1)
                                        <td>
                                            <a href="{{ route('backend.user.setIsAdmin', $user->id) }}" class="star">
                                                <i style="color:yellow" class="glyphicon glyphicon-star"></i>
                                            </a>
                                            <a href="{{ route('backend.user.removeAdmin', $user->id) }}" class="star">
                                                <i style="color:black" class="glyphicon glyphicon-star"></i>
                                            </a>
                                        </td>
                                    @endif

                                    @if (Auth::guard('web')->user()->level == 1)
                                        <td>
                                            <a href="{{ route('backend.user.edit', $user->id) }}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                        </td>
                                        <td><input class="checkbox-delete" name="deleteUser[]" type="checkbox" value="{{ $user->id }}"></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </form>
            </div>
            <nav aria-label="Page navigation example">
                {{ $users->links() }}
            </nav>
        </div>
    </div>
@endsection

@push('script')
<script src="{{ asset('js/user/confirmDelete.js') }}"></script>
@endpush

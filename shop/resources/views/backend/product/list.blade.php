@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/list-product.css') }}">
@endpush
@extends('backend.master.master')
@section('title' ,'list product')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error-warning'))
        <div class="alert alert-warning alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('error-warning') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <form action="{{ route('backend.product.searchProduct') }}" method="POST" class="navbar-form navbar-left">
        @csrf
        <input type="text" name="key_search_product" id="key_search_product" class="form-control input-search-product" placeholder="Search product...">
        <div id="resultSearchProductAjax"></div>
    </form>
    <a class="btn btn-info btn-add-product" href="{{ route('backend.product.add', $idCategory) }}">+ New</a>
	<div class="row">
        <div class="col-md-12">
            <h4><a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> / {{ $idCategory->name }}</h4>
            <div class="table-responsive">
                <form action="{{ route('backend.product.delete', $idCategory) }}" method="post" onsubmit="return deleteConfirm();">
                    @csrf
                    <table id="table-list-product" class="table table-bordred table-striped">
                        <thead>
                            <th scope="col">Image</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Promotion</th>
                            <th scope="col">Featured</th>
                            <th scope="col">Status</th>
                            {{-- <th scope="col">Category</th> --}}
                            <th scope="col">View More</th>
                            <th scope="col">Upload images</th>
                            <th scope="col">View img</th>
                            <th scope="col">Edit</th>
                            <th>
                                <button type="submit" class="btn-delete">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button>
                            </th>
                        </thead>
                        <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td><img src="/img/{{ $product->img }}" width="100" height="100" alt="img-product"></td>
                                    <td>{{ strtoupper($product->name) }}</td>
                                    <td>{{ number_format($product->price) }}</td>
                                    <td>{{ number_format($product->promotion) }}</td>
                                    <td>
                                        @if ($product->featured == 1)
                                            <span class="badge badge-primary" style="background-color: #dc3545">Hot</span>
                                        @else
                                            <span class="badge badge-secondary">Normal</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($product->status == 1)
                                            <span class="badge badge-pill badge-success" style="background-color: #28a745">Sell</span>
                                        @else
                                            <span class="badge badge-pill badge-warning" style="background-color: #ffc107">Sold out</span>
                                        @endif
                                    </td>
                                    {{-- <td>{{ $product->category->name }}</td> --}}
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#modalShowAttribute{{$product->id}}"><span class="glyphicon glyphicon-eye-open"></span></a>
                                    </td>
                                    <td><a class="btn btn-success" href="{{ route('backend.product.uploadMultipleImg', ['idCategory' => $product->category_id, 'idProduct' => $product->id]) }}" data-toggle="modal" data-target="#modalUpdateImg{{$product->id}}"><span class="glyphicon glyphicon-open"></span></a></td>
                                    <td><a href="{{ route('backend.product.showProductImages', ['idCategory' => $product->category_id, 'idProduct' => $product->id]) }}"><span class="glyphicon glyphicon-level-up"></span></a></td>
                                    <td><a href="{!! route('backend.product.edit',  ['idCategory' => $product->category_id, 'idProduct' => $product->id]) !!}" class="btn btn-primary" style="width: 25px"><span class="glyphicon glyphicon-pencil" style="margin-left: -7px"></span></a></td>
                                    <td><input class="checkbox-delete" name="deleteProduct[]" type="checkbox" value="{{ $product->id }}"></td>
                                </tr>
                            </form>
                                <div class="modal fade upload-img-modal" id="modalUpdateImg{{ $product->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ $product->name }} / Upload Image</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" enctype="multipart/form-data" action="{{ route('backend.product.uploadMultipleImg', ['idCategory' => $product->category_id, 'idProduct' => $product->id]) }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label>Choose file</label>
                                                    <input style="margin-left: 2px" type="file" name="uploadMultipleImg[]" id="files" data-type='image' class="text-center center-block" multiple>
                                                    <div class="result"></div>
                                                </div>
                                                <button type="submit" class="btn btn-success">Save changes</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </form>
                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <div class="modal fade modal-show-attribute" id="modalShowAttribute{{$product->id}}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ $product->name }} / Attribute</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body body-show-attribute">
                                            <span>Code</span> <br/>
                                            {{ $product->code }} <br/>
                                            @foreach (valueAttribute($product->valuesAttribute) as $key => $value)
                                                <span>{{ $key }}</span> <br/>
                                                @foreach ($value as $item)
                                                    {{ $item }} <br/>
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            <nav aria-label="Page navigation example">
                {{ $products->links() }}
            </nav>
        </div>
    </div>
@endsection

@push('script')
<script src="{{ asset('js/product/confirmDelete.js') }}"></script>
<script src="{{ asset('js/product/uploadImage.js') }}"></script>
<script src="{{ asset('js/product/prevMultipleImg.js') }}"></script>
<script src="{{ asset('js/product/validateSearchProduct.js') }}"></script>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/add-product.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'add product')
@section('content')
<div class="header-add-product">
    <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
    <a href="{{ route('backend.product.list', $idCategory->id) }}"><span class="back-page">{{ $idCategory->name }}</span></a>
</div>

<form method="post" id="addProductForm" enctype="multipart/form-data">
    @csrf
    {{-- <form action="" method="POST"></form> --}}
        <div class="row">
  	        <div class="col-lg-3"><!--left col-->
                <div class="text-center">
                    <input type="file" style="margin-left: 0px" name="fileToUpload" id="fileToUpload" data-type='image' class="text-center center-block input-file-upload">
                    <span style="color: red; margin-left: -10px" class="error-upload">
                        @if ($errors->has('fileToUpload'))
                            {{ $errors->first('fileToUpload') }}
                        @endif
                    </span>
                    <div class="empty-text" style="margin-left: -31px; color: #17a2b8">Image Avatar here</div>
                </div><br>
                <div>
                    <input style="margin-left: 2px" type="file" name="uploadMultipleImage[]" id="files" data-type='image' class="text-center center-block" multiple>
                    <span style="color: red; margin-left: -10px" class="error-upload">
                        @if ($errors->has('uploadMultipleImage'))
                            {{ $errors->first('uploadMultipleImage') }}
                        @endif
                    </span>
                    <div class="result" style="color: #17a2b8">Upload Multiple Images Product</div>
                </div>
            </div><!--/col-3-->

    	    <div class="col-lg-9">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left: 30px"><a data-toggle="tab" href="#home">Add Product</a></li>
                </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="fullname"><h4>Product name</h4></label>
                                <input type="text" class="form-control" name="name" placeholder="Product name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="code"><h4>Code</h4></label>
                                <input type="text" class="form-control" name="code" placeholder="Code" value="{{ old('code') }}">
                                @if ($errors->has('code'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('code') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="price"><h4>Price</h4></label>
                                <input type="number" class="form-control" name="price" placeholder="Price" value="{{ old('price') }}">
                                @if ($errors->has('price'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('price') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="promotion"><h4>Promotion</h4></label>
                                <input type="text" class="form-control" name="promotion" placeholder="Promotion" value="{{ old('promotion') }}">
                                @if ($errors->has('promotion'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('promotion') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="feature"><h4>Feature</h4></label>
                                <select class="form-control" name="featured">
                                    <option value="1">Hot</option>
                                    <option value="0">Normal</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="status"><h4>Status</h4></label>
                                <select class="form-control" name="status">
                                    <option value="1">Sell</option>
                                    <option value="0">Sold out</option>
                                </select>
                            </div>
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <div class="col-xs-6">--}}
{{--                                <label for="promotion"><h4>Start Sale</h4></label>--}}
{{--                                <input type="date" class="form-control" name="start_sale" value="{{ old('start_sale') }}">--}}
{{--                                @if ($errors->has('promotion'))--}}
{{--                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">--}}
{{--                                        {{ $errors->first('promotion') }}--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <div class="col-xs-6">--}}
{{--                                <label for="promotion"><h4>End Sale</h4></label>--}}
{{--                                <input type="date" class="form-control" name="end_sale" value="{{ old('end_sale') }}">--}}
{{--                                @if ($errors->has('promotion'))--}}
{{--                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">--}}
{{--                                        {{ $errors->first('promotion') }}--}}
{{--                                    </div>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="Attributes">Attributes</label>
                                <div class="panel panel-default">
                                    <div class="panel-body tabs">
                                        <label for="attributes">Attributes | <a href="/home/attribute">Detail Attribute</a></label>
                                        @if ($errors->has('attr_name'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('attr_name') }}
                                            </div>
                                        @endif
                                        @if ($errors->has('value_name'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ $errors->first('value_name') }}
                                            </div>
                                        @endif
                                        @if (session('notification'))
                                            <div class="alert alert-success">
                                                {{ session('notification') }}
                                            </div>
                                        @endif
                                        <ul class="nav nav-tabs">
                                           @php
                                                $i = 0;
                                            @endphp
                                            @foreach ($attributes as $attribute)
                                            <li @if($i == 0) class="active" @endif><a href="#tab{{ $attribute->id }}" data-toggle="tab">{{ $attribute->name }}</a></li>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @endforeach
                                        </ul>
                                        <div class="tab-content">
                                            @foreach ($attributes as $attribute)
                                            <div class="tab-pane fade @if($i == 1) active @endif in" id="tab{{ $attribute->id }}">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            @foreach ($attribute->values as $valueAttribute)
                                                                <td>{{ $valueAttribute->value }}</td>
                                                            @endforeach
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            @foreach ($attribute->values as $valueAttribute)
                                                            <td>
                                                                <input type="checkbox" name="attr[]" value="{{ $valueAttribute->id }}">
                                                            </td>
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <hr/>
                                                {{-- <div class="form-group">
                                                    <form action="{{ route('backend.product.attribute.value.add') }}" method="post" name="add-attribute-form">
                                                        @csrf
                                                        <input type="hidden" name="attr_id" value="{{ $attribute->id }}">
                                                        <label for="value_attribute">Value attribute</label>
                                                        <input type="text" name="value_name" class="form-control"><br>
                                                        <button type="submit" class="btn btn-info">Add</button>
                                                    </form>
                                                </div> --}}
                                            </div>
                                            @php
                                                $i = 2;
                                            @endphp
                                            @endforeach
                                                @if ($errors->has('select_attribute_value'))
                                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                                        {{ $errors->first('select_attribute_value') }}
                                                    </div>
                                                @endif
                                            {{-- <div class="tab-pane fade" id="tab-add">
                                                <form action="{{ route('backend.product.attribute.add') }}" method="post">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="">Tên thuộc tính mới</label>
                                                        <input type="text" class="form-control" name="attr_name" aria-describedby="helpId" placeholder="">
                                                    </div>
                                                    <button type="submit" name="add_attribute" class="btn btn-success"> <span class="glyphicon glyphicon-plus"></span>Thêm thuộc tính</button>
                                                </form>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" rows="3" value="{{ old('name') }}"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="">Select Category</label>
                                    <select class="form-control" name="select_category">
                                        {{showCategories($categories,0,'',0)}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                                <button type="submit" class="btn btn-lg btn-success"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <hr>
                </div><!--/tab-pane-->
            </div><!--/col-9-->
        </div><!--/row-->

@endsection

@push('script')
    <script src="{{ asset('js/product/addValidate.js') }}"></script>
    <script src="{{ asset('js/product/addPrevImg.js') }}"></script>
    <script src="{{ asset('js/product/ckeditor.js') }}"></script>
    <script src="{{ asset('js/product/prevMultipleUploadImg.js') }}"></script>
@endpush


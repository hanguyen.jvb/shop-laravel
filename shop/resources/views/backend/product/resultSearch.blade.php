@push('css')
    {{-- <link rel="stylesheet" href="{{ asset('css/backend/list-user.css') }}"> --}}
@endpush

@extends('backend.master.master')
@section('title', 'result product')
@section('content')
    <div class="title-search">
        <h2>Result Search Product</h2>
    </div>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Name</th>
                <th scope="col">Code</th>
                <th scope="col">Price</th>
                <th scope="col">Promotion</th>
                <th scope="col">Featured</th>
                <th scope="col">Status</th>
                <th scope="col">Category</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($search as $searchProduct)
                <tr>
                    <th><img src="/img/{{ $searchProduct->img }}" width="100" height="100"></th>
                    <td>{{ $searchProduct->name }}</td>
                    <td>{{ $searchProduct->code }}</td>
                    <td>{{ $searchProduct->price }}</td>
                    <td>{{ $searchProduct->promotion }}</td>
                    <td>
                        @if ($searchProduct->featured == 1)
                            <span class="badge badge-primary" style="background-color: #dc3545">Hot</span>
                        @else
                            <span class="badge badge-secondary">Normal</span>
                        @endif
                    </td>
                    <td>
                        @if ($searchProduct->status == 1)
                            <span class="badge badge-pill badge-success" style="background-color: #28a745">Sell</span>
                        @else
                            <span class="badge badge-pill badge-warning" style="background-color: #ffc107">Sold out</span>
                        @endif
                    </td>
{{--                    <td>{{ $searchProduct->category->name }}</td>--}}
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

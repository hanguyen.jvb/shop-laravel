@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/edit-images.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'Edit image')
@section('content')
<div class="header-edit-image">
    <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
    <a href="{{ URL::previous() }}"><span class="back-page">Back</span></a> /
    <span>{{ $idImage->product->name }}</span>
</div>

<form class="form" method="post" id="EditImgForm" enctype="multipart/form-data">
    @csrf
        <div class="row">
  	        <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <div class="empty-text">
                        <img src="/img/{{ $idImage->image_product }}" width="200" height="200" class="avatar img-thumbnail">
                        <input type="file" width="300" height="300" name="fileUpload" class="text-center center-block input-file-upload">
                    </div>
                    <span class="error-upload">
                        @if ($errors->has('fileUpload'))
                            {{ $errors->first('fileUpload') }}
                        @endif
                    </span>
                </div><br>

                <div class="multiple-img">

                    <span style="color: red" class="error-upload">
                </div>
            </div><!--/col-3-->

    	    <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Edit </a></li>
                </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="product_name"><h4>Image name</h4></label>
                            <input type="text" class="form-control" name="image_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label for="product"><h4>Products</h4></label>
                            <select class="form-control" name="product_name">
                                <option value="0">Select Product</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <br>
                            <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>
                            <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                        </div>
                    </div>
                    <hr>
                </div><!--/tab-pane-->
            </div><!--/col-9-->
        </div><!--/row-->
    </form>
@endsection

@push('script')
    <script src="{{ asset('js/product/editPrevImg.js') }}"></script>
    <script src="{{ asset('js/product/ckeditor.js') }}"></script>
@endpush

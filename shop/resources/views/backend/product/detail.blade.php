{{--{{ dd($productId->categoryy->name) }}--}}
@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/edit-product.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'detail user')
@section('content')
    <div class="header-edit-product">
        <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
        <a href="{{ route('backend.product.list', $productId->categoryy->id) }}"><span class="back-page">{{ $productId->categoryy->name }}</span></a> /
        <span>Detail Product</span>
    </div>

    <form class="form" method="post" id="EditProductForm" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <div class="empty-text">
                        <img src="/img/{{ $productId->img }}" width="200" height="200" class="avatar img-thumbnail">
{{--                        <input type="file" width="300" height="300" name="fileUpload" class="text-center center-block input-file-upload">--}}
                    </div>
                    <span class="error-upload" style="color: red">
                        @if ($errors->has('fileUpload'))
                            {{ $errors->first('fileUpload') }}
                        @endif
                    </span>
                </div><br>

                <div class="multiple-img">
                    <span style="color: red" class="error-upload">
                </div>
            </div><!--/col-3-->

            <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">{{ $productId->name }}</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                        <hr>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="fullname"><h4>Product name</h4></label>
                                <input type="text" class="form-control" name="name" placeholder="Product name" value="{{ $productId->name }}" disabled>
                                @if ($errors->has('name'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="code"><h4>Code</h4></label>
                                <input type="text" class="form-control" name="code" placeholder="Code" value="{{ $productId->code }}" disabled>
                                @if ($errors->has('code'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('code') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="price"><h4>Price</h4></label>
                                <input type="text" class="form-control" name="price" placeholder="Price" value="{{ number_format($productId->price, '0', '', '.') }}" disabled>
                                @if ($errors->has('price'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('price') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="promotion"><h4>Promotion</h4></label>
                                <input type="text" class="form-control" name="promotion" placeholder="Promotion" value="{{ $productId->promotion }}" disabled>
                                @if ($errors->has('promotion'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('promotion') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="feature"><h4>Feature</h4></label>
                                <select class="form-control" name="featured">
                                    <option value="1" {{ ($productId->featured == 1) ? 'selected' : '' }}>Hot</option>
                                    <option value="0" {{ ($productId->featured == 0) ? 'selected' : '' }}>Normal</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="status"><h4>Status</h4></label>
                                <select class="form-control" name="status">
                                    <option value="1" {{ ($productId->status == 1) ? 'selected' : '' }}>Sell</option>
                                    <option value="0" {{ ($productId->status == 0) ? 'selected' : '' }}>Sold out</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="status"><h4>Category</h4></label>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="price" placeholder="Price" value="{{ $productId->categoryy->name }}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="description">Description</label>
                                <textarea class="form-control" name="description" rows="3">{{ $productId->description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
{{--                                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>--}}
{{--                                <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>--}}
                            </div>
                        </div>
                        <hr>
                    </div><!--/tab-pane-->
                </div><!--/col-9-->
            </div><!--/row-->
    </form>
@endsection

@push('script')
    <script src="{{ asset('js/product/editValidate.js') }}"></script>
    <script src="{{ asset('js/product/editPrevImg.js') }}"></script>
    <script src="{{ asset('js/product/ckeditor.js') }}"></script>
@endpush

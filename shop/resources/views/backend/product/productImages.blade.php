@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/show-images.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'show product images')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error-warning'))
        <div class="alert alert-warning alert-dismissible show add-success" role="alert">
            <strong>
                {{ session('error-warning') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
<div class="row">
    <div class="col-md-12">
        <div class="header-show-images">
            <a href="{{ route('backend.home.list') }}"><span class="lnr lnr-home"></span></a> /
            <a href="{{ route('backend.product.list', $idCategory->id) }}"><span class="back-page">{{ $idCategory->name }}</span></a>
        </div>
        <h4>Product Images</h4>
        <div class="table-responsive">
            <form action="{{ route('backend.product.image.deleteImages', $idProduct) }}" method="post" onsubmit="return deleteConfirm();">
                @csrf
                <table id="table-show-images" class="table table-bordred table-striped">
                    <thead>
                        <th scope="col">Id</th>
                        <th scope="col">Img</th>
                        <th scope="col">Name</th>
                        <th scope="col">Product</th>
                        <th scope="col">Update</th>
                        <th scope="col">
                            <button type="submit" class="btn-delete">
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                        </th>
                    </thead>
                    <tbody>
                        @foreach($images as $image)
                            <tr>
                                <td scope="row">{{ $image->id }}</td>
                                <td><img src="/img/{{ $image->image_product }}" width="100" height="100"></td>
                                <td>{{ $image->name }}</td>
                                <td>{{ $image->product->name }}</td>
                                <td><a href="{{ route('backend.product.image.updateImages', ['idProduct' => $image->product_id, 'idImage' => $image->id]) }}" class="btn btn-success" data-toggle="modal" data-target="#modalEditImg{{$image->id}}"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><input class="checkbox-delete" name="deleteImages[]" type="checkbox" value="{{ $image->id }}"></td>
                            </tr>
                        </form>
                            <div class="modal fade upload-img-modal" id="modalEditImg{{ $image->id }}" tabindex="-1" role="dialog">
                                <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ $image->name }} / Edit Image</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                        <form method="post" enctype="multipart/form-data" action="{{ route('backend.product.image.updateImages', ['idProduct' => $image->product_id, 'idImage' => $image->id]) }}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-sm-3"><!--left col-->
                                                    <div class="text-center">
                                                        <div class="empty-text">
                                                            <img src="/img/{{ $image->image_product }}" width="200" height="200" class="avatar img-thumbnail">
                                                            <input type="file" width="300" height="300" name="fileUpload" class="text-center center-block input-file-upload">
                                                        </div>
                                                        <span class="error-upload">
                                                            @if ($errors->has('fileUpload'))
                                                                {{ $errors->first('fileUpload') }}
                                                            @endif
                                                        </span>
                                                    </div><br>
                                                </div><!--/col-3-->

                                                <div class="col-sm-9">
                                                    <ul class="nav nav-tabs">
                                                        <li class="active"><a data-toggle="tab" href="#home">Edit {{ $image->name }}</a></li>
                                                    </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="home">
                                                        <hr>
                                                            <div class="form-group">
                                                                <div class="col-xs-6">
                                                                    <label for="fullname"><h4>Product name</h4></label>
                                                                    <input type="text" class="form-control" name="image_name" value="{{ $image->name }}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-xs-6">
                                                                    <label for="feature"><h4>Products</h4></label>
                                                                    <select class="form-control" name="product_name">
                                                                        <option value="0">select product</option>
                                                                        @foreach ($products as $product)
                                                                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                               <div class="col-xs-12">
                                                                    <br>
                                                                        <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>
                                                                        <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                                                </div>
                                                            </div>
                                                        <hr>
                                                    </div><!--/tab-pane-->
                                                </div><!--/col-9-->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                </div>
                            </div>
                        @endforeach
                    </tbody>
                </table>
        </div>
        <nav aria-label="Page navigation example">

        </nav>
    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('js/product/editPrevImg.js') }}"></script>
<script src="{{ asset('js/product/confirmDelete.js') }}"></script>
@endpush

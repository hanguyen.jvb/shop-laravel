{{-- @push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/update-acount.css') }}">
@endpush

@extends('backend.master.master')
@section('title', 'setting acount')
@section('content')
    <div class="main-update">
            <div class="success">
                <strong>
                    @if (session('notification'))
                        {{ session('notification') }}
                    @endif
                </strong>
            </div>
        <div class="row name-admin">
            <div class="col-lg-10">
                <h2>
                    @if (Auth::check())
                        {{ Auth::user()->fullname }}
                    @endif
                </h2>
            </div>
        </div>
        <form class="form" method="post" id="updateAdminForm" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-4">

                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="fullname"><h4>Fullname</h4></label>
                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="enter your fullname" title="enter your fullname" value="{{ $idAdmin->fullname }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                        <label for="username"><h4>Username</h4></label>
                            <input type="text" class="form-control" name="username" placeholder="enter your username" title="enter your username" value="{{ $idAdmin->username }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="email"><h4>Email</h4></label>
                            <input type="email" class="form-control" name="email" placeholder="enter your email" title="enter your phone email" value="{{ $idAdmin->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="password"><h4>Password</h4></label>
                            <input type="password" class="form-control" name="password" placeholder="enter your password" title="enter your password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="phone"><h4>Phone</h4></label>
                            <input type="number" class="form-control" name="phone" placeholder="enter your phone" title="enter your phone" value="{{ $idAdmin->phone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6">
                            <label for="address"><h4>Address</h4></label>
                            <input type="text" class="form-control" name="address" placeholder="somewhere" title="enter a location" value="{{ $idAdmin->address }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <br>
                            <button class="btn btn-lg btn-success" type="submit">Save</button>
                            <button class="btn btn-lg btn-secondary" type="reset">Reset</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('script')
    <script src="{{ asset('js/acount/prevImage.js') }}"></script>
    <script src="{{ asset('js/acount/validate.js') }}"></script>
@endpush --}}

@push('css')
    <link rel="stylesheet" href="{{ asset('css/backend/update-acount.css') }}">
@endpush
@extends('backend.master.master')
@section('title', 'update profile')
@section('content')
    @if (session('notification'))
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>
                    {{ session('notification') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

<div class="container bootstrap snippet">
    {{-- <div class="row">
  		<div class="col-sm-10">
            <h1 style="margin-left: 11px;">
                @if (Auth::check())
                    {{ Auth::user()->fullname }}
                @endif
            </h1>
        </div>
    </div> --}}

    <form class="form" method="post" id="updateAdminForm" enctype="multipart/form-data">
    @csrf
        <div class="row">
  	        <div class="col-sm-3"><!--left col-->
                <div class="text-center">
                    <div class="empty-text">
                        <img src="/img/{{ $idAdmin->img }}" width="200" height="230" class="avatar" alt="avatar">
                        <h6 style="color: #17a2b8">Upload a different photo...</h6>
                        <input style="margin-left: 32px" type="file" name="fileToUpload" id="fileToUpload" class="text-center center-block file-upload">
                        <span style="color: red" class="error-upload">
                            @if ($errors->has('fileToUpload'))
                                {{ $errors->first('fileToUpload') }}
                            @endif
                        </span>
                    </div>
                </div><hr/><br>
                <div class="panel panel-default">
                    <div class="panel-heading">Social Media</div>
                    <div class="panel-body">
                        <a href="https://www.facebook.com/haviet0610"><i class="fa fa-facebook fa-2x"></i></a> <a href="https://gitlab.com/hanguyen.jvb"><i class="fa fa-github fa-2x"></i></a>
                    </div>
                </div>
            </div><!--/col-3-->

    	    <div class="col-sm-9">
                <ul class="nav nav-tabs">
                    <li class="active" style="margin-left: 30px"><a data-toggle="tab" href="#home">Setting</a></li>
                </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="home">
                    <hr>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="fullname"><h4>Full name</h4></label>
                                <input type="text" class="form-control" name="fullname" value="{{ $idAdmin->fullname }}">
                                @if ($errors->has('fullname'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('fullname') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="username"><h4>User name</h4></label>
                                <input type="text" class="form-control" name="username" value="{{ $idAdmin->username }}">
                                @if ($errors->has('username'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('username') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="email"><h4>Email</h4></label>
                                <input type="text" class="form-control" name="email" id="email" value="{{ $idAdmin->email }}">
                                @if ($errors->has('email'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="phone"><h4>Phone</h4></label>
                                <input type="number" class="form-control" name="phone" value="{{ $idAdmin->phone }}">
                                @if ($errors->has('phone'))
                                <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                    {{ $errors->first('phone') }}
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="password"><h4>Password</h4></label>
                                <input type="password" id="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="confirm password"><h4>Confirm Password</h4></label>
                                <input type="password" class="form-control" name="confirm_password" id="confirm_password">
                                @if ($errors->has('confirm_password'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('confirm_password') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="address"><h4>Address</h4></label>
                                <input type="address" class="form-control" name="address" value="{{ $idAdmin->address }}">
                                @if ($errors->has('address'))
                                    <div class="alert alert-danger" role="alert" style="margin-top: 5px;">
                                        {{ $errors->first('address') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                        </div>
                    <hr>
                </div><!--/tab-pane-->
            </div><!--/col-9-->
        </div><!--/row-->
    </form>
@endsection

@push('script')
<script src="{{ asset('js/acount/prevImage.js') }}"></script>
<script src="{{ asset('js/acount/validate.js') }}"></script>
@endpush

@if(!Auth::guard('client')->user())
    @push('css')
        <link rel="stylesheet" href="{{ asset('css/backend/list-category.css') }}">
    @endpush
    @extends('backend.master.master')
    @section('title', 'list category')
@section('content')
    @if (session('notify-category'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" style="width: 400px; margin-left: 530px; margin-top: 20px">
            <strong>
                {{ session('notify-category') }}
            </strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @endif
        </div>
        <div class="main-category">
            <div class="row">
                <div class="col-lg-5">
                    <form method="post" id="addCategoryForm" onsubmit="return deleteConfirm();">
                        @csrf
                        <div class="list-category">
                            <div class="title-list-category">
                                <h3>List category</h3>
                            </div>
                            <div class="form-group">
                                <label>Select Category</label>
                                <select class="form-control select-category" name="parent">
                                    <option value="0">Categories</option>
                                    {{showCategories($categories, 0, '', 0)}}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Category name</label>
                                <input type="text" name="name" class="form-control text-name-category" placeholder="Enter category name">
                                <span class="error-name-category">
                                @if ($errors->has('name'))
                                        {{ $errors->first('name') }}
                                    @endif
                            </span>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-add-category">Add Category</button>
                    </form>
                </div>

                <div class="col-lg-7">
                    <div class="action-category">
                        <h3>Action</h3>
                        <div class="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script')
    <script src="{{ asset('js/category/addValidate.js') }}"></script>
    <script src="{{ asset('js/user/confirmDelete.js') }}"></script>
@endpush

@endif

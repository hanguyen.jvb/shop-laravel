{{-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/backend/login.css') }}">
</head>
<body>
    <div id="login">
        <h3 class="text-center text-white pt-5">Login form</h3>
        <div class="container">
            <div id="login-row" class="row justify-content-center align-items-center">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <form id="login-form" class="form" method="post">
                            @csrf
                            <h3 class="text-center text-info">Login to Admin page</h3>
                            @if (session('notification'))
                                <div class="alert alert-danger">
                                    <strong>{{ session('notification') }}</strong>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="username" class="text-info">Username:</label><br>
                                <input type="text" name="username" id="username" class="form-control" value="{{ old('username') }}">
                            </div>
                            <div class="form-group">
                                <label for="password" class="text-info">Password:</label><br>
                                <input type="password" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" name="login" class="btn btn-info btn-md" value="login">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html> --}}

<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login to JVB Admin</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/vendor/linearicons/style.css') }}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/img/favicon.png') }}">
</head>

<body>
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content" style="margin-top: -470px">
							<div class="header">
								<div class="logo text-center"><img src="{{ asset('assets/img/logo-dark.png') }}" alt="Klorofil Logo"></div>
                                <p class="lead">Login to JVB Admin</p>
                                @if (session('notification'))
                                    <div class="alert alert-danger">
                                        <strong>{{ session('notification') }}</strong>
                                    </div>
                                @endif
                                @if (session('notifyCheckLogin'))
                                    <div class="alert alert-danger">
                                        <strong>{{ session('notifyCheckLogin') }}</strong>
                                    </div>
                                @endif
							</div>
							<form class="form-auth-small" method="POST">
                                @csrf
								<div class="form-group">
									<label for="signin-username" class="control-label sr-only">Username</label>
                                    <input type="text" name="username" class="form-control" id="signin-username" value="{{ old('username') }}" placeholder="Enter your username">
                                    @if ($errors->has('username'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 7px">
                                            {{ $errors->first('username') }}
                                        </div>
                                    @endif
								</div>
								<div class="form-group">
									<label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" name="password" class="form-control" id="signin-password" placeholder="Enter your password">
                                    @if ($errors->has('password'))
                                        <div class="alert alert-danger" role="alert" style="margin-top: 7px">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
							</form>
						</div>
					</div>
					<div class="right"></div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>

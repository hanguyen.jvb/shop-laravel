
{{--@foreach($customersOrdered as $productSell)--}}
{{--    @foreach($productSell->customerOrdering as $product)--}}

{{--    @endforeach--}}
{{--@endforeach--}}

    @push('css')
        <link rel="stylesheet" href="{{ asset('css/backend/home.css') }}">
    @endpush
    @extends('backend.master.master')
    @section('title', 'home')
@section('content')

        <h2>
            Welcome
            <span>
                @if (Auth::guard('web')->check())
                    {{ Auth::guard('web')->user()->fullname }}
                @endif
            </span>
        </h2>

        {{-- <div class="member">
            <svg class="glyph stroked male user">
                <use xlink:href="#stroked-male-user"/>
            </svg>
            <div class="title-block">
                User
            </div>
            <div class="info">{{ count($users) }} users</div>
        </div>
        <div class="category">
            <svg class="glyph stroked notepad ">
                <use xlink:href="#stroked-notepad"/>
            </svg>
            <div class="title-block">
                Category
            </div>
            <div class="info">{{ count($categories) }} Categories</div>
        </div>
        <div class="product">
            <svg class="glyph stroked clipboard with paper">
                <use xlink:href="#stroked-clipboard-with-paper"/>
            </svg>
            <div class="title-block">
                Product
            </div>
            <div class="info">{{ count($products) }} products</div>
        </div>
        <div class="order">
            <svg class="glyph stroked film">
                <use xlink:href="#stroked-film"/>
            </svg>
            <div class="title-block">
                Order
            </div>
            <div class="info">{{ count($orders) }} order</div>
        </div> --}}

        <!-- OVERVIEW -->
        <div class="panel panel-headline">
            <div class="panel-heading">
                <h3 class="panel-title">Weekly Overview</h3>
                <p class="panel-subtitle">Period: {{ $timerNow }}</p>
            </div>
            <div class="panel-body">
                <div class="row">
                    <a href="{{ route('backend.order.completedOrder') }}">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="lnr lnr-cart"></i></span>
                                <p>
                                    <span class="number">{{ count($customersOrdered) }}</span>
                                    <span class="title">Orders paid</span>
                                </p>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('backend.order.listProcessed') }}">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="lnr lnr-cart"></i></span>
                                <p>
                                    <span class="number">{{ count($customersShipping) }}</span>
                                    <span class="title">Order shipping</span>
                                </p>
                            </div>
                        </div>
                    </a>
                    <a href="{{ route('backend.order.list') }}">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="lnr lnr-cart"></i></span>
                                <p>
                                    <span class="number">{{ count($orders) }}</span>
                                    <span class="title">Orders</span>
                                </p>
                            </div>
                        </div>
                    </a>
{{--                    <div class="col-md-3">--}}
{{--                        <div class="metric">--}}
{{--                            <span class="icon"><i class="fa fa-bar-chart"></i></span>--}}
{{--                            <p>--}}
{{--                                <span class="number">{{ count($orderCustomer) }}</span>--}}
{{--                                <span class="title">Products</span>--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
                <div style="">
                    <div class="row">
{{--                        <a href="{{ route('backend.order.completedOrder') }}">--}}
{{--                            <div class="col-md-5">--}}
{{--                                <div class="metric">--}}
{{--                                    <span class="icon"><i class="lnr lnr-cart"></i></span>--}}
{{--                                    <p>--}}
{{--                                        <span class="number">{{ count(array($cusOrder)) }}</span>--}}
{{--                                        <span class="title">Orders paid</span>--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
{{--                        <a href="{{ route('backend.order.completedOrder') }}">--}}
{{--                            <div class="col-md-5">--}}
{{--                                <div class="metric">--}}
{{--                                    <span class="icon"><i class="lnr lnr-cart"></i></span>--}}
{{--                                    <p>--}}
{{--                                        <span class="number">{{number_format($orderCustomer->sum('price'), '0', '', '.')}}đ</span>--}}
{{--                                        <span class="title">Orders paid</span>--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
{{--                        <div id="headline-chart" class="ct-chart"></div>--}}
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Recent Purchases</h3>
                                <div class="right">
                                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                                </div>
                            </div>
                            <div class="panel-body no-padding">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Date &amp; Time</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($customerOrder as $customer)
                                    <tr>
                                        <td>{{ $customer->fullname }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->updated_at }}</td>
                                        <td>
                                            @if($customer->state == 2)
                                                <span class="label label-success">COMPLETED</span>
                                             @elseif($customer->state == 1)
                                                <span class="label label-warning">SHIPPING</span>
                                              @else
                                                <span class="label label-danger">ORDER</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>
                                    <div class="col-md-6 text-right"><a href="{{ route('backend.order.list') }}" class="btn btn-primary">View All Purchases</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                    <div class="col-md-3">--}}
{{--                        <div class="weekly-summary text-right">--}}
{{--                            <span class="number">{{ count($orderCustomer) }}</span>--}}
{{--                            <span class="info-label">Total Sales</span>--}}
{{--                        </div>--}}
{{--                        <div class="weekly-summary text-right">--}}
{{--                            <span class="number">{{number_format($orderCustomer->sum('price'))}}</span>--}}
{{--                            <span class="info-label">Total Money</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- END OVERVIEW -->
{{--        <div class="row">--}}
{{--            <div class="col-md-6">--}}
{{--                <!-- RECENT PURCHASES -->--}}
{{--                <div class="panel">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h3 class="panel-title">Recent Purchases</h3>--}}
{{--                        <div class="right">--}}
{{--                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
{{--                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="panel-body no-padding">--}}
{{--                        <table class="table table-striped">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th>Order No.</th>--}}
{{--                                <th>Name</th>--}}
{{--                                <th>Amount</th>--}}
{{--                                <th>Date &amp; Time</th>--}}
{{--                                <th>Status</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                            <tr>--}}
{{--                                <td><a href="#">763648</a></td>--}}
{{--                                <td>Steve</td>--}}
{{--                                <td>$122</td>--}}
{{--                                <td>Oct 21, 2016</td>--}}
{{--                                <td><span class="label label-success">COMPLETED</span></td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td><a href="#">763649</a></td>--}}
{{--                                <td>Amber</td>--}}
{{--                                <td>$62</td>--}}
{{--                                <td>Oct 21, 2016</td>--}}
{{--                                <td><span class="label label-warning">PENDING</span></td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td><a href="#">763650</a></td>--}}
{{--                                <td>Michael</td>--}}
{{--                                <td>$34</td>--}}
{{--                                <td>Oct 18, 2016</td>--}}
{{--                                <td><span class="label label-danger">FAILED</span></td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td><a href="#">763651</a></td>--}}
{{--                                <td>Roger</td>--}}
{{--                                <td>$186</td>--}}
{{--                                <td>Oct 17, 2016</td>--}}
{{--                                <td><span class="label label-success">SUCCESS</span></td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td><a href="#">763652</a></td>--}}
{{--                                <td>Smith</td>--}}
{{--                                <td>$362</td>--}}
{{--                                <td>Oct 16, 2016</td>--}}
{{--                                <td><span class="label label-success">SUCCESS</span></td>--}}
{{--                            </tr>--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                    <div class="panel-footer">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-6"><span class="panel-note"><i class="fa fa-clock-o"></i> Last 24 hours</span></div>--}}
{{--                            <div class="col-md-6 text-right"><a href="#" class="btn btn-primary">View All Purchases</a></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- END RECENT PURCHASES -->--}}
{{--            </div>--}}
{{--            <div class="col-md-6">--}}
{{--                <!-- MULTI CHARTS -->--}}
{{--                <div class="panel">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h3 class="panel-title">Projection vs. Realization</h3>--}}
{{--                        <div class="right">--}}
{{--                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
{{--                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="panel-body">--}}
{{--                        <div id="visits-trends-chart" class="ct-chart"></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- END MULTI CHARTS -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-7">--}}
{{--                <!-- TODO LIST -->--}}
{{--                <div class="panel">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h3 class="panel-title">To-Do List</h3>--}}
{{--                        <div class="right">--}}
{{--                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
{{--                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="panel-body">--}}
{{--                        <ul class="list-unstyled todo-list">--}}
{{--                            <li>--}}
{{--                                <label class="control-inline fancy-checkbox">--}}
{{--                                    <input type="checkbox"><span></span>--}}
{{--                                </label>--}}
{{--                                <p>--}}
{{--                                    <span class="title">Restart Server</span>--}}
{{--                                    <span class="short-description">Dynamically integrate client-centric technologies without cooperative resources.</span>--}}
{{--                                    <span class="date">Oct 9, 2016</span>--}}
{{--                                </p>--}}
{{--                                <div class="controls">--}}
{{--                                    <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <label class="control-inline fancy-checkbox">--}}
{{--                                    <input type="checkbox"><span></span>--}}
{{--                                </label>--}}
{{--                                <p>--}}
{{--                                    <span class="title">Retest Upload Scenario</span>--}}
{{--                                    <span class="short-description">Compellingly implement clicks-and-mortar relationships without highly efficient metrics.</span>--}}
{{--                                    <span class="date">Oct 23, 2016</span>--}}
{{--                                </p>--}}
{{--                                <div class="controls">--}}
{{--                                    <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <label class="control-inline fancy-checkbox">--}}
{{--                                    <input type="checkbox"><span></span>--}}
{{--                                </label>--}}
{{--                                <p>--}}
{{--                                    <strong>Functional Spec Meeting</strong>--}}
{{--                                    <span class="short-description">Monotonectally formulate client-focused core competencies after parallel web-readiness.</span>--}}
{{--                                    <span class="date">Oct 11, 2016</span>--}}
{{--                                </p>--}}
{{--                                <div class="controls">--}}
{{--                                    <a href="#"><i class="icon-software icon-software-pencil"></i></a> <a href="#"><i class="icon-arrows icon-arrows-circle-remove"></i></a>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- END TODO LIST -->--}}
{{--            </div>--}}
{{--            <div class="col-md-5">--}}
{{--                <!-- TIMELINE -->--}}
{{--                <div class="panel panel-scrolling">--}}
{{--                    <div class="panel-heading">--}}
{{--                        <h3 class="panel-title">Recent User Activity</h3>--}}
{{--                        <div class="right">--}}
{{--                            <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>--}}
{{--                            <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="panel-body">--}}
{{--                        <ul class="list-unstyled activity-list">--}}
{{--                            <li>--}}
{{--                                <img src="assets/img/user1.png" alt="Avatar" class="img-circle pull-left avatar">--}}
{{--                                <p><a href="#">Michael</a> has achieved 80% of his completed tasks <span class="timestamp">20 minutes ago</span></p>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <img src="assets/img/user2.png" alt="Avatar" class="img-circle pull-left avatar">--}}
{{--                                <p><a href="#">Daniel</a> has been added as a team member to project <a href="#">System Update</a> <span class="timestamp">Yesterday</span></p>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <img src="assets/img/user3.png" alt="Avatar" class="img-circle pull-left avatar">--}}
{{--                                <p><a href="#">Martha</a> created a new heatmap view <a href="#">Landing Page</a> <span class="timestamp">2 days ago</span></p>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <img src="assets/img/user4.png" alt="Avatar" class="img-circle pull-left avatar">--}}
{{--                                <p><a href="#">Jane</a> has completed all of the tasks <span class="timestamp">2 days ago</span></p>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <img src="assets/img/user5.png" alt="Avatar" class="img-circle pull-left avatar">--}}
{{--                                <p><a href="#">Jason</a> started a discussion about <a href="#">Weekly Meeting</a> <span class="timestamp">3 days ago</span></p>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <button type="button" class="btn btn-primary btn-bottom center-block">Load More</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- END TIMELINE -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
@endsection




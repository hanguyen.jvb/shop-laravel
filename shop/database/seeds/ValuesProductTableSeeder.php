<?php

use Illuminate\Database\Seeder;

class ValuesProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('values_product')->delete();
        DB::table('values_product')->insert([
            // ['product_id' => 7, 'value_id' => 1],
            // ['product_id' => 7, 'value_id' => 2],
            // ['product_id' => 7, 'value_id' => 3],
            // ['product_id' => 7, 'value_id' => 4],
            // ['product_id' => 7, 'value_id' => 5],
            // ['product_id' => 7, 'value_id' => 6],
            // ['product_id' => 7, 'value_id' => 7],
            // ['product_id' => 7, 'value_id'=> 10],

            ['product_id' => 34, 'value_id' => 1],
            ['product_id' => 34, 'value_id' => 2],
            ['product_id' => 34, 'value_id' => 3],
            ['product_id' => 34, 'value_id' => 4],
            ['product_id' => 34, 'value_id' => 5],
            ['product_id' => 34, 'value_id' => 6],
            ['product_id' => 34, 'value_id' => 7],
            ['product_id' => 34, 'value_id'=> 9],

            ['product_id' => 36, 'value_id' => 1],
            ['product_id' => 36, 'value_id' => 2],
            ['product_id' => 36, 'value_id' => 3],
            ['product_id' => 36, 'value_id' => 4],
            ['product_id' => 36, 'value_id' => 5],
            ['product_id' => 36, 'value_id' => 6],
            ['product_id' => 36, 'value_id' => 7],
            ['product_id' => 36, 'value_id' => 8],
        ]);
    }
}

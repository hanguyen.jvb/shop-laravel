<?php

use Illuminate\Database\Seeder;

class ValuesAttributeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('values_attribute')->delete();
        DB::table('values_attribute')->insert([
            ['id' => 1, 'value' => 'Size 5.5US - Size 38.0VN-24.5CM', 'attribute_id' => 1],
            ['id' => 2, 'value' => 'Size 6.0US - Size 39.0VN-24.5CM', 'attribute_id' => 1],
            ['id' => 3, 'value' => 'Size 7.0US - Size 40.0VN-25.5CM', 'attribute_id' => 1],
            ['id' => 4, 'value' => 'Size 7.5US - Size 41.0VN-26.0CM', 'attribute_id' => 1],
            ['id' => 5, 'value' => 'Size 8.5US - Size 42.0VN-27.0CM', 'attribute_id' => 1],
            ['id' => 6, 'value' => 'Size 9.5US - Size 43.0VN-28.0CM', 'attribute_id' => 1],
            ['id' => 7, 'value' => 'Size 10 US - Size 44.0VN-28.5CM', 'attribute_id' => 1],
            ['id' => 8, 'value' => 'Black/White', 'attribute_id' => 2],
            ['id' => 9, 'value' => 'White', 'attribute_id' => 2],
            ['id' => 10, 'value' => 'Red', 'attribute_id' => 2],
            ['id' => 11, 'value' => 'Black', 'attribute_id' => 2],
        ]);
    }
}

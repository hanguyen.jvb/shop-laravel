<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            ['name' => 'quần jean xanh', 'code' => 'jean-01', 'slug' => 'quan-jean-xanh', 'price' => '450.000', 'promotion' => '0', 'featured' => '1', 'status' => '1', 'description' => 'mẫu quần mới mùa hè', 'img' => 'img', 'category_id' => '2'],
            ['name' => 'T-shirt', 'code' => 'shirt-01', 'slug' => 't-shirt-xam', 'price' => '350.000', 'promotion' => '0', 'featured' => '1', 'status' => '1', 'description' => 'mẫu áo mới mùa hè', 'img' => 'img', 'category_id' => '3'],
            ['name' => 'adidas shoe', 'code' => 'jean-01', 'slug' => 'addas-shoe', 'price' => '750.000', 'promotion' => '0', 'featured' => '1', 'status' => '1', 'description' => 'mẫu giày mới mùa hè', 'img' => 'img', 'category_id' => '4'],
        ]);
    }
}

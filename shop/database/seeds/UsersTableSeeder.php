<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['fullname' => 'nguyen viet ha', 'username' => 'hanv97', 'email' => 'vietha@gmail.com', 'password' => bcrypt('123456'), 'slug' => 'nguyen-viet-ha', 'img' => 'img', 'phone' => '123', 'address' => 'bac giang', 'level' => '1'],
            ['fullname' => 'hoang tien anh', 'username' => 'anhht97', 'email' => 'anhht@gmail.com', 'password' => bcrypt('123456'), 'slug' => 'hoang-tien-anh', 'img' => 'img', 'phone' => '123', 'address' => 'ha noi', 'level' => '2'],
        ]);
    }
}

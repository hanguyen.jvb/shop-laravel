<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert(
            [
                ['fullname' => 'Truong Anh Quan', 'address' => 'Ha Dong, Ha Noi', 'email' => 'quan@gmail.com', 'phone' => '03658879942', 'total' => 100000, 'state' => 1],
                ['fullname' => 'Nguyen Van Hieu', 'address' => 'Bắc Ninh', 'email' => 'hieu@gmail.com', 'phone' => '03564478214', 'total' => 100000, 'state' => 0],
                ['fullname' => 'Vu Van Hau', 'address' => 'Hai Duong', 'email' => 'hau@gmail.com', 'phone' => '03214789547','total' => 100000,'state' => 0],
                ['fullname' => 'Nguyen Cong Minh', 'address' => 'Soc Son, Ha Noi', 'email' => 'minh@gmail.com', 'phone' => '03525246673', 'total' => 100000,'state' => 1],
                ['fullname' => 'Pham Quang Huy', 'address' => 'Hai Phong', 'email' => 'huy@gmail.com', 'phone' => '0354879500', 'total' => 100000,'state' => 0],
                ['fullname' => 'Truong Thi Hau', 'address' => 'Thai Binh', 'email' => 'hautruong@gmail.com', 'phone' => '0354879567', 'total' => 100000, 'state' => 0],
                ['fullname' => 'Nguyen Phuong Thuy', 'address' => 'Thai Nguyen', 'email' => 'thuy@gmail.com', 'phone' => '0354871237', 'total '=> 100000, 'state' => 0],
                ['fullname' => 'Bui Trong Hoan', 'address' => 'Nam Dinh', 'email' => 'hoan@gmail.com', 'phone' => '034412345', 'total' => 100000, 'state' => 0]
            ]
        );
    }
}

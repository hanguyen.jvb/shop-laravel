$(document).ready(function () {
    $("#formCustomerOrder").validate({
        rules: {
            fullname: "required",
            email: {
                required: true,
                email: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 11
            },
            address: "required",
            city: "required",
            district: "required",
        },
        messages: {
            fullname: "Vui lòng nhập họ và tên",
            email: {
                required: "Vui lòng nhập email",
                email: "Vui lòng nhập email đúng định dạng",
            },
            phone: {
                required: "Vui lòng nhập số điện thoại",
                number: "Nhập đúng định dạng số điện thoại",
                minlength: "Số điện thoại từ 10-11 chữ số",
                maxlength: "Số điện thoại từ 10-11 chữ số",
            },
            address: "Vui lòng nhập địa chỉ",
            city: "Vui lòng nhập tỉnh thành",
            district: "Vui lòng nhập quận huyện",
        }
    });
    $("#formCustomerOrder").validate({
        onsubmit: false
    });
})

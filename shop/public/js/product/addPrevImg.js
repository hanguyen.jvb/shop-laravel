$('#fileToUpload').on('change', function() {
    var file = this.files[0];
    var imagefile = file.type;
    var imageTypes = ["image/jpeg", "image/png", "image/jpg"];
    if (imageTypes.indexOf(imagefile) == -1) {
        return false;
        $(this).empty();
    } else {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(".empty-text").html('<img src="' + e.target.result + '" width="300" height="300" style="margin-left: 30px" class="avatar img-thumbnail" />');
        };
        reader.readAsDataURL(this.files[0]);
    }
});

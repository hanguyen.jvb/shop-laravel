$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    $('.upload-img').click(function() {
        $('.upload-img-modal').modal('show');
    });
});

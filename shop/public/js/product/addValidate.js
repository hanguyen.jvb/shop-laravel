$(document).ready(function () {
    $("#addProductForm").validate({
        ignore: [],
        debug: false,
        rules: {
            name: "required",
            code: "required",
            price: {
                required: true,
                number: true,
            },
            promotion: {
                required: true,
                number: true,
            },
            fileToUpload: "required",
            uploadMultipleImage: "required",
            description:{
                required: true,
            },
        },
        messages: {
            name: "Please enter product name",
            code: "Please enter code",
            price: {
                required: "Please enter price",
            },
            promotion: {
                required: "Please enter promotion",
            },
            fileToUpload: "Choose your file to upload",
            description: {
                required: "Please enter description",
            },
        }
    });
    $("#addProductForm").validate({
        onsubmit: false
    });
})

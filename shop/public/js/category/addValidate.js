$(document).ready(function () {
    $("#addCategoryForm").validate({
        rules: {
            name: "required",
        },
        messages: {
            name: "Please enter category name",
        }
    });
    $("#addCategoryForm").validate({
        onsubmit: false
    });
})

$(document).ready(function () {
    $("#editUserForm").validate({
        rules: {
            fullname: "required",
            username: "required",
            email: "required",
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                equalTo : "#password"
            },
            fileToUpload: "required",
            phone: {
                required: true,
                number: true,
                min: 0,
                minlength: 10,
                maxlength: 11
            },
            address: {
                required: true
            },
        },
        messages: {
            fullname: "Please enter your fullname",
            username: "Please enter your username",
            email: "Please enter your email",
            password: {
                required: "Please enter your password",
                minlength: "Your password is from 6 characters"
            },
            confirm_password: {
                required: "Please confirm password",
                equalTo: "Confirm password is not correct"
            },
            fileToUpload: "Choose your file to upload",
            phone: {
                required: "Please enter your phone",
                minlength: "Your phone number from 10 to 11 numbers",
                maxlength: "Your phone number from 10 to 11 numbers"
            },
            address: "Please enter your address",
        }
    });
    $("#editUserForm").validate({
        onsubmit: false
    });
})

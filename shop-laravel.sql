-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2020 at 05:38 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop-laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attributes`
--

INSERT INTO `attributes` (`id`, `name`) VALUES
(1, 'Size'),
(2, 'Color');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`) VALUES
(2, 'Converse', 0),
(3, 'Vans', 0),
(4, 'Palladium', 0),
(5, 'Classic', 2),
(6, 'Chuck-70s', 2),
(7, 'Old-Skool', 3),
(8, 'Slip-on', 3),
(9, 'Classic', 4),
(10, 'Sneaker', 4),
(11, 'All-Star', 2),
(12, 'SK8-HI', 3);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `fullname`, `username`, `email`, `password`, `slug`, `img`, `phone`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'nguyen viet nam', 'vietnam123', 'nam@gmail.com', '$2y$10$w6ywgFQN/x2zLpRksCMrBOJAmSDyIrbvKTzmRG7fhV2XsUnhbH61C', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'haviet0110197', 'haviet110', 'viethanguyenptit0110@gmail.com', '$2y$10$P4fMrJCnsrfEZAzAYJFnVuX9p9Ah2QKFejlGGt7q6HommRANev6Zu', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'hanguyen997', 'hanguyen991', 'viethanguyenptit0110@gmail.com', '$2y$10$w/GjP4hlrtsBNsJei4BkWuxs8rQhaG828z2g.o1xq3.ZEjo.D2c3G', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'ha', 'ha', 'viethanguyenptit0110@gmail.com', '$2y$10$iuQBaNjKStOowLNXB2MRdubxqL.vn3WjvyoFARHd5JP8WxbSTS5Em', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'nguyen van anh', 'anh', 'ha.nguyen.jvb@gmail.com', '$2y$10$.MCL6ciTVK6M1luIqKjcX.3POx9DFLhSWI5sr1MR177JPWm2lhFbm', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_orders`
--

CREATE TABLE `customer_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer_orders`
--

INSERT INTO `customer_orders` (`id`, `name`, `price`, `quantity`, `img`, `customer_id`, `created_at`, `updated_at`) VALUES
(1, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 2, '2020-05-15 02:49:11', '2020-05-15 02:49:11'),
(2, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 3, '2020-05-15 02:51:49', '2020-05-15 02:51:49'),
(3, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 4, '2020-05-15 02:55:02', '2020-05-15 02:55:02'),
(4, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 5, '2020-05-15 02:56:02', '2020-05-15 02:56:02'),
(5, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 6, '2020-05-15 03:00:18', '2020-05-15 03:00:18'),
(6, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 7, '2020-05-15 03:06:35', '2020-05-15 03:06:35'),
(7, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 8, '2020-05-15 03:09:32', '2020-05-15 03:09:32'),
(8, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 9, '2020-05-15 03:09:33', '2020-05-15 03:09:33'),
(9, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 10, '2020-05-15 03:09:35', '2020-05-15 03:09:35'),
(10, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 11, '2020-05-15 03:09:37', '2020-05-15 03:09:37'),
(11, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 12, '2020-05-15 03:09:39', '2020-05-15 03:09:39'),
(12, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 13, '2020-05-15 03:09:40', '2020-05-15 03:09:40'),
(13, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 14, '2020-05-15 03:09:42', '2020-05-15 03:09:42'),
(14, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 15, '2020-05-15 03:09:43', '2020-05-15 03:09:43'),
(15, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 16, '2020-05-15 03:09:45', '2020-05-15 03:09:45'),
(16, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 17, '2020-05-15 03:09:47', '2020-05-15 03:09:47'),
(17, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 18, '2020-05-15 03:09:48', '2020-05-15 03:09:48'),
(18, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 19, '2020-05-15 03:09:51', '2020-05-15 03:09:51'),
(19, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 20, '2020-05-15 03:09:54', '2020-05-15 03:09:54'),
(20, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 21, '2020-05-15 03:09:57', '2020-05-15 03:09:57'),
(21, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 22, '2020-05-15 03:09:59', '2020-05-15 03:09:59'),
(22, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 23, '2020-05-15 03:10:00', '2020-05-15 03:10:00'),
(23, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 24, '2020-05-15 03:10:02', '2020-05-15 03:10:02'),
(24, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 25, '2020-05-15 03:10:03', '2020-05-15 03:10:03'),
(25, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 26, '2020-05-15 03:10:05', '2020-05-15 03:10:05'),
(26, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 27, '2020-05-15 03:10:06', '2020-05-15 03:10:06'),
(27, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 28, '2020-05-15 03:10:09', '2020-05-15 03:10:09'),
(28, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 29, '2020-05-15 03:10:11', '2020-05-15 03:10:11'),
(29, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 30, '2020-05-15 03:10:12', '2020-05-15 03:10:12'),
(30, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 31, '2020-05-15 03:10:13', '2020-05-15 03:10:13'),
(31, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 32, '2020-05-15 03:10:14', '2020-05-15 03:10:14'),
(32, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 33, '2020-05-15 03:10:15', '2020-05-15 03:10:15'),
(33, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 34, '2020-05-15 03:10:16', '2020-05-15 03:10:16'),
(34, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 35, '2020-05-15 03:10:17', '2020-05-15 03:10:17'),
(35, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 36, '2020-05-15 03:10:20', '2020-05-15 03:10:20'),
(36, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 37, '2020-05-15 03:10:21', '2020-05-15 03:10:21'),
(37, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 38, '2020-05-15 03:10:24', '2020-05-15 03:10:24'),
(38, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 39, '2020-05-15 03:10:26', '2020-05-15 03:10:26'),
(39, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 40, '2020-05-15 03:10:28', '2020-05-15 03:10:28'),
(40, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 41, '2020-05-15 03:10:29', '2020-05-15 03:10:29'),
(41, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 42, '2020-05-15 03:10:33', '2020-05-15 03:10:33'),
(42, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 43, '2020-05-15 03:10:34', '2020-05-15 03:10:34'),
(43, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 44, '2020-05-15 03:10:36', '2020-05-15 03:10:36'),
(44, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 45, '2020-05-15 03:10:37', '2020-05-15 03:10:37'),
(45, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 46, '2020-05-15 03:10:39', '2020-05-15 03:10:39'),
(46, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 47, '2020-05-15 03:10:40', '2020-05-15 03:10:40'),
(47, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 48, '2020-05-15 03:10:44', '2020-05-15 03:10:44'),
(48, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 49, '2020-05-15 03:10:45', '2020-05-15 03:10:45'),
(49, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 50, '2020-05-15 03:10:47', '2020-05-15 03:10:47'),
(50, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 51, '2020-05-15 03:10:49', '2020-05-15 03:10:49'),
(51, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 52, '2020-05-15 03:10:51', '2020-05-15 03:10:51'),
(52, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 53, '2020-05-15 03:10:52', '2020-05-15 03:10:52'),
(53, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 54, '2020-05-15 03:10:54', '2020-05-15 03:10:54'),
(54, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 55, '2020-05-15 03:10:55', '2020-05-15 03:10:55'),
(55, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 56, '2020-05-15 03:10:56', '2020-05-15 03:10:56'),
(56, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 57, '2020-05-15 03:10:57', '2020-05-15 03:10:57'),
(57, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 58, '2020-05-15 03:11:00', '2020-05-15 03:11:00'),
(58, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 59, '2020-05-15 03:11:02', '2020-05-15 03:11:02'),
(59, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 60, '2020-05-15 03:11:03', '2020-05-15 03:11:03'),
(60, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 61, '2020-05-15 03:11:04', '2020-05-15 03:11:04'),
(61, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 62, '2020-05-15 03:11:05', '2020-05-15 03:11:05'),
(62, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 63, '2020-05-15 03:11:06', '2020-05-15 03:11:06'),
(63, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 64, '2020-05-15 03:11:08', '2020-05-15 03:11:08'),
(64, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 65, '2020-05-15 03:11:09', '2020-05-15 03:11:09'),
(65, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 66, '2020-05-15 03:11:11', '2020-05-15 03:11:11'),
(66, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 67, '2020-05-15 03:11:13', '2020-05-15 03:11:13'),
(67, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 68, '2020-05-15 03:11:44', '2020-05-15 03:11:44'),
(68, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 69, '2020-05-15 03:11:46', '2020-05-15 03:11:46'),
(69, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 70, '2020-05-15 03:11:48', '2020-05-15 03:11:48'),
(70, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 71, '2020-05-15 03:11:50', '2020-05-15 03:11:50'),
(71, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 72, '2020-05-15 03:11:53', '2020-05-15 03:11:53'),
(72, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 73, '2020-05-15 03:11:54', '2020-05-15 03:11:54'),
(73, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 74, '2020-05-15 03:11:57', '2020-05-15 03:11:57'),
(74, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 75, '2020-05-15 03:11:59', '2020-05-15 03:11:59'),
(75, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 76, '2020-05-15 03:12:10', '2020-05-15 03:12:10'),
(76, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 77, '2020-05-15 03:12:12', '2020-05-15 03:12:12'),
(77, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 78, '2020-05-15 03:12:13', '2020-05-15 03:12:13'),
(78, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 79, '2020-05-15 03:12:16', '2020-05-15 03:12:16'),
(79, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 80, '2020-05-15 03:12:17', '2020-05-15 03:12:17'),
(80, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 81, '2020-05-15 03:12:20', '2020-05-15 03:12:20'),
(81, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 82, '2020-05-15 03:12:22', '2020-05-15 03:12:22'),
(82, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 83, '2020-05-15 03:12:23', '2020-05-15 03:12:23'),
(83, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 84, '2020-05-15 03:12:25', '2020-05-15 03:12:25'),
(84, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 85, '2020-05-15 03:12:27', '2020-05-15 03:12:27'),
(85, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 86, '2020-05-15 03:12:28', '2020-05-15 03:12:28'),
(86, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 87, '2020-05-15 03:12:29', '2020-05-15 03:12:29'),
(87, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 88, '2020-05-15 03:12:30', '2020-05-15 03:12:30'),
(88, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 89, '2020-05-15 03:12:31', '2020-05-15 03:12:31'),
(89, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 90, '2020-05-15 03:12:32', '2020-05-15 03:12:32'),
(90, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 91, '2020-05-15 03:12:33', '2020-05-15 03:12:33'),
(91, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 92, '2020-05-15 03:12:34', '2020-05-15 03:12:34'),
(92, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 93, '2020-05-15 03:12:35', '2020-05-15 03:12:35'),
(93, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 94, '2020-05-15 03:12:36', '2020-05-15 03:12:36'),
(94, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 95, '2020-05-15 03:12:37', '2020-05-15 03:12:37'),
(95, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 96, '2020-05-15 03:12:42', '2020-05-15 03:12:42'),
(96, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 97, '2020-05-15 03:12:43', '2020-05-15 03:12:43'),
(97, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 98, '2020-05-15 03:12:46', '2020-05-15 03:12:46'),
(98, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 99, '2020-05-15 03:12:47', '2020-05-15 03:12:47'),
(99, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 100, '2020-05-15 03:12:48', '2020-05-15 03:12:48'),
(100, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 101, '2020-05-15 03:12:51', '2020-05-15 03:12:51'),
(101, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 102, '2020-05-15 03:12:53', '2020-05-15 03:12:53'),
(102, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 103, '2020-05-15 03:12:55', '2020-05-15 03:12:55'),
(103, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 104, '2020-05-15 03:12:58', '2020-05-15 03:12:58'),
(104, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 105, '2020-05-15 03:13:01', '2020-05-15 03:13:01'),
(105, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 106, '2020-05-15 03:13:02', '2020-05-15 03:13:02'),
(106, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 107, '2020-05-15 03:13:03', '2020-05-15 03:13:03'),
(107, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 108, '2020-05-15 03:13:05', '2020-05-15 03:13:05'),
(108, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 109, '2020-05-15 03:13:08', '2020-05-15 03:13:08'),
(109, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 110, '2020-05-15 03:13:10', '2020-05-15 03:13:10'),
(110, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 111, '2020-05-15 03:13:12', '2020-05-15 03:13:12'),
(111, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 112, '2020-05-15 03:13:14', '2020-05-15 03:13:14'),
(112, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 113, '2020-05-15 03:13:16', '2020-05-15 03:13:16'),
(113, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 114, '2020-05-15 03:13:18', '2020-05-15 03:13:18'),
(114, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 115, '2020-05-15 03:13:19', '2020-05-15 03:13:19'),
(115, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 116, '2020-05-15 03:15:29', '2020-05-15 03:15:29'),
(116, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 117, '2020-05-17 21:02:51', '2020-05-17 21:02:51'),
(117, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 117, '2020-05-17 21:02:52', '2020-05-17 21:02:52'),
(118, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 118, '2020-05-17 21:06:44', '2020-05-17 21:06:44'),
(119, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 118, '2020-05-17 21:06:44', '2020-05-17 21:06:44'),
(120, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 119, '2020-05-17 21:21:42', '2020-05-17 21:21:42'),
(121, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 119, '2020-05-17 21:21:42', '2020-05-17 21:21:42'),
(122, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 120, '2020-05-17 21:22:16', '2020-05-17 21:22:16'),
(123, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 120, '2020-05-17 21:22:16', '2020-05-17 21:22:16'),
(124, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 121, '2020-05-17 21:24:58', '2020-05-17 21:24:58'),
(125, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 121, '2020-05-17 21:24:58', '2020-05-17 21:24:58'),
(126, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 122, '2020-05-17 21:26:01', '2020-05-17 21:26:01'),
(127, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 122, '2020-05-17 21:26:02', '2020-05-17 21:26:02'),
(128, 'Vans Sk8 - Hi Label Mix', '2050000', '3', 'vans-sk8-hi-label-mix.png', 122, '2020-05-17 21:26:02', '2020-05-17 21:26:02'),
(129, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 123, '2020-05-17 23:00:15', '2020-05-17 23:00:15'),
(130, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '4', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 123, '2020-05-17 23:00:15', '2020-05-17 23:00:15'),
(131, 'Vans Sk8 - Hi Label Mix', '2050000', '3', 'vans-sk8-hi-label-mix.png', 123, '2020-05-17 23:00:15', '2020-05-17 23:00:15'),
(132, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 124, '2020-05-17 23:03:25', '2020-05-17 23:03:25'),
(133, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 125, '2020-05-17 23:11:38', '2020-05-17 23:11:38'),
(134, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 126, '2020-05-17 23:12:26', '2020-05-17 23:12:26'),
(135, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 127, '2020-05-17 23:12:34', '2020-05-17 23:12:34'),
(136, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 128, '2020-05-17 23:13:53', '2020-05-17 23:13:53'),
(137, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 129, '2020-05-17 23:14:19', '2020-05-17 23:14:19'),
(138, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 130, '2020-05-17 23:16:25', '2020-05-17 23:16:25'),
(139, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 130, '2020-05-17 23:16:25', '2020-05-17 23:16:25'),
(140, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 131, '2020-05-17 23:19:06', '2020-05-17 23:19:06'),
(141, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 131, '2020-05-17 23:19:06', '2020-05-17 23:19:06'),
(142, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 140, '2020-05-18 00:54:48', '2020-05-18 00:54:48'),
(143, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 141, '2020-05-18 00:55:32', '2020-05-18 00:55:32'),
(144, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 142, '2020-05-18 00:55:49', '2020-05-18 00:55:49'),
(145, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 143, '2020-05-18 00:56:41', '2020-05-18 00:56:41'),
(146, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 144, '2020-05-18 00:59:42', '2020-05-18 00:59:42'),
(147, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 145, '2020-05-18 01:09:43', '2020-05-18 01:09:43'),
(148, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 146, '2020-05-18 01:11:35', '2020-05-18 01:11:35'),
(149, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 147, '2020-05-18 01:12:21', '2020-05-18 01:12:21'),
(150, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 148, '2020-05-18 01:15:34', '2020-05-18 01:15:34'),
(151, 'Vans Sk8 - Hi Label Mix', '2050000', '3', 'vans-sk8-hi-label-mix.png', 152, '2020-05-18 01:21:54', '2020-05-18 01:21:54'),
(152, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 153, '2020-05-18 01:37:54', '2020-05-18 01:37:54'),
(153, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 154, '2020-05-18 01:43:01', '2020-05-18 01:43:01'),
(154, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 155, '2020-05-18 01:59:23', '2020-05-18 01:59:23'),
(155, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 158, '2020-05-18 02:12:42', '2020-05-18 02:12:42'),
(156, 'Palladium Blanc Hi - 72886-421', '1600000', '4', 'palladium-blanc-hi-72886-421.jpg', 159, '2020-05-18 02:18:07', '2020-05-18 02:18:07'),
(157, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '3', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 160, '2020-05-18 02:27:33', '2020-05-18 02:27:33'),
(158, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 161, '2020-05-18 02:31:54', '2020-05-18 02:31:54'),
(159, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 162, '2020-05-18 02:32:43', '2020-05-18 02:32:43'),
(160, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 163, '2020-05-18 02:34:53', '2020-05-18 02:34:53'),
(161, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 164, '2020-05-18 02:35:59', '2020-05-18 02:35:59'),
(162, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 165, '2020-05-18 02:36:30', '2020-05-18 02:36:30'),
(163, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 166, '2020-05-18 02:37:04', '2020-05-18 02:37:04'),
(164, 'Palladium Blanc Hi - 72886-421', '1600000', '3', 'palladium-blanc-hi-72886-421.jpg', 167, '2020-05-18 02:37:19', '2020-05-18 02:37:19'),
(165, 'Vans Sk8 - Hi Label Mix', '2050000', '4', 'vans-sk8-hi-label-mix.png', 168, '2020-05-18 02:54:54', '2020-05-18 02:54:54'),
(166, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 169, '2020-05-18 02:56:41', '2020-05-18 02:56:41'),
(167, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 170, '2020-05-18 02:59:55', '2020-05-18 02:59:55'),
(168, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 171, '2020-05-18 03:10:13', '2020-05-18 03:10:13'),
(169, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 172, '2020-05-18 03:10:58', '2020-05-18 03:10:58'),
(170, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 173, '2020-05-18 03:11:43', '2020-05-18 03:11:43'),
(171, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '3', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 174, '2020-05-18 03:19:12', '2020-05-18 03:19:12'),
(172, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 175, '2020-05-18 03:23:56', '2020-05-18 03:23:56'),
(173, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 176, '2020-05-18 03:26:57', '2020-05-18 03:26:57'),
(174, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 177, '2020-05-18 03:28:50', '2020-05-18 03:28:50'),
(175, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 178, '2020-05-18 03:33:32', '2020-05-18 03:33:32'),
(176, 'converse chuck taylor all star classic black white', '1500000', '1', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 179, '2020-05-18 09:31:12', '2020-05-18 09:31:12'),
(177, 'converse chuck taylor all star classic black white', '1500000', '1', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 180, '2020-05-18 09:54:24', '2020-05-18 09:54:24'),
(178, 'Palladium Blanc Hi - 72886-421', '1600000', '4', 'palladium-blanc-hi-72886-421.jpg', 181, '2020-05-18 09:55:11', '2020-05-18 09:55:11'),
(179, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 182, '2020-05-18 18:24:12', '2020-05-18 18:24:12'),
(180, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 183, '2020-05-18 18:40:56', '2020-05-18 18:40:56'),
(181, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 184, '2020-05-18 18:41:46', '2020-05-18 18:41:46'),
(182, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 185, '2020-05-18 18:41:56', '2020-05-18 18:41:56'),
(183, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 186, '2020-05-18 18:50:08', '2020-05-18 18:50:08'),
(184, 'Vans Sk8 - Hi Label Mix', '2050000', '3', 'vans-sk8-hi-label-mix.png', 187, '2020-05-18 18:52:15', '2020-05-18 18:52:15'),
(185, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 188, '2020-05-18 18:57:03', '2020-05-18 18:57:03'),
(188, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 191, '2020-05-18 18:59:51', '2020-05-18 18:59:51'),
(189, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 192, '2020-05-18 19:26:46', '2020-05-18 19:26:46'),
(190, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 193, '2020-05-18 19:27:33', '2020-05-18 19:27:33'),
(191, 'Vans Sk8 - Hi Label Mix', '2050000', '3', 'vans-sk8-hi-label-mix.png', 194, '2020-05-18 19:29:21', '2020-05-18 19:29:21'),
(192, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 195, '2020-05-19 02:55:11', '2020-05-19 02:55:11'),
(193, 'Vans Sk8 - Hi Label Mix', '2050000', '1', 'vans-sk8-hi-label-mix.png', 197, '2020-05-19 03:05:40', '2020-05-19 03:05:40'),
(194, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 202, '2020-05-19 03:13:58', '2020-05-19 03:13:58'),
(195, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 203, '2020-05-19 03:15:21', '2020-05-19 03:15:21'),
(196, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 204, '2020-05-19 03:19:04', '2020-05-19 03:19:04'),
(197, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 205, '2020-05-19 03:19:52', '2020-05-19 03:19:52'),
(198, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 206, '2020-05-19 03:20:13', '2020-05-19 03:20:13'),
(199, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 207, '2020-05-19 03:21:58', '2020-05-19 03:21:58'),
(200, 'converse chuck taylor all star classic black white', '1500000', '6', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 208, '2020-05-19 03:26:52', '2020-05-19 03:26:52'),
(201, 'Palladium Blanc Hi - 72886-097', '1600000', '3', 'palladium-blanc-hi-72886-097.jpg', 208, '2020-05-19 03:26:52', '2020-05-19 03:26:52'),
(202, 'Vans Sk8 - Hi Label Mix', '2050000', '2', 'vans-sk8-hi-label-mix.png', 208, '2020-05-19 03:26:52', '2020-05-19 03:26:52'),
(203, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 209, '2020-05-19 03:31:11', '2020-05-19 03:31:11'),
(204, 'Palladium Blanc Hi - 72886-421', '1600000', '2', 'palladium-blanc-hi-72886-421.jpg', 210, '2020-05-19 03:31:24', '2020-05-19 03:31:24'),
(205, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 211, '2020-05-19 03:33:30', '2020-05-19 03:33:30'),
(206, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 212, '2020-05-19 03:33:52', '2020-05-19 03:33:52'),
(207, 'Palladium Blanc Hi - 72886-421', '1600000', '1', 'palladium-blanc-hi-72886-421.jpg', 213, '2020-05-19 03:55:19', '2020-05-19 03:55:19'),
(208, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 213, '2020-05-19 03:55:19', '2020-05-19 03:55:19'),
(209, 'chuck taylor all star 1970s hoop hunter 165912c', '2000000', '3', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.jpg', 213, '2020-05-19 03:55:19', '2020-05-19 03:55:19'),
(210, 'Vans Sk8 - Hi Label Mix', '2050000', '5', 'vans-sk8-hi-label-mix.png', 214, '2020-05-19 04:10:30', '2020-05-19 04:10:30'),
(211, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 214, '2020-05-19 04:10:30', '2020-05-19 04:10:30'),
(212, 'Palladium Blanc Hi - 72886-097', '1600000', '2', 'palladium-blanc-hi-72886-097.jpg', 214, '2020-05-19 04:10:30', '2020-05-19 04:10:30'),
(213, 'converse chuck taylor all star classic black white', '1500000', '2', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 215, '2020-05-19 04:12:36', '2020-05-19 04:12:36'),
(214, 'Palladium Blanc Hi - 72886-097', '1600000', '1', 'palladium-blanc-hi-72886-097.jpg', 215, '2020-05-19 04:12:36', '2020-05-19 04:12:36'),
(215, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 216, '2020-05-19 16:05:09', '2020-05-19 16:05:09'),
(229, 'converse chuck taylor all star 1970s flight school leather silver hi', '1470000', '5', 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi.jpg', 221, '2020-05-22 18:39:06', '2020-05-22 18:39:06'),
(230, 'vans old skool 36 dx anaheim factory', '1980000', '4', 'vans-old-skool-36-dx-anaheim-factory.jpg', 221, '2020-05-22 18:39:06', '2020-05-22 18:39:06'),
(231, 'palladium pampa hi shake - 96637-665-m', '1870000', '1', 'palladium-pampa-hi-shake-96637-665-m.jpg', 221, '2020-05-22 18:39:06', '2020-05-22 18:39:06'),
(245, 'converse chuck taylor all star classic black white', '1350000', '1', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 232, '2020-05-26 01:41:54', '2020-05-26 01:41:54'),
(250, 'converse chuck taylor all star classic black white', '1350000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 237, '2020-05-26 01:56:21', '2020-05-26 01:56:21'),
(251, 'converse chuck taylor all star classic navy', '1275000', '3', 'converse-chuck-taylor-all-star-classic-navy.png', 237, '2020-05-26 01:56:21', '2020-05-26 01:56:21'),
(252, 'converse chuck taylor all star classic black white', '1350000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 238, '2020-05-26 02:01:02', '2020-05-26 02:01:02'),
(253, 'converse chuck taylor all star classic cream white hi', '1350000', '2', 'converse-chuck-taylor-all-star-classic-cream-white-hi.jpg', 238, '2020-05-26 02:01:02', '2020-05-26 02:01:02'),
(254, 'converse chuck taylor all star classic black white', '1500000', '1', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 239, '2020-05-26 10:23:26', '2020-05-26 10:23:26'),
(255, 'converse chuck taylor all star classic black white', '1500000', '1', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 240, '2020-05-26 10:26:11', '2020-05-26 10:26:11'),
(272, 'converse chuck taylor all star classic black white', '1500000', '3', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 249, '2020-06-03 08:29:12', '2020-06-03 08:29:12'),
(273, 'vans sk8 hi black white', '540000', '2', 'vans-sk8-hi-black-white.png', 249, '2020-06-03 08:29:12', '2020-06-03 08:29:12');

-- --------------------------------------------------------

--
-- Table structure for table `cutomers`
--

CREATE TABLE `cutomers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` decimal(8,2) DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cutomers`
--

INSERT INTO `cutomers` (`id`, `fullname`, `email`, `phone`, `address`, `total`, `state`, `created_at`, `updated_at`, `city`, `district`, `description`) VALUES
(1, 'tien anh', 'ha@gmail.com', '40', 'Bac Giang', NULL, 0, '2020-05-15 02:44:19', '2020-05-15 02:44:19', NULL, NULL, NULL),
(2, 'tien anh', 'ha@gmail.com', '01691234567', 'Bac Giang', NULL, 0, '2020-05-15 02:49:11', '2020-05-15 02:49:11', NULL, NULL, NULL),
(3, 'tien anh', 'ha@gmail.com', '01691234567', 'Bac Giang', NULL, 0, '2020-05-15 02:51:49', '2020-05-15 02:51:49', NULL, NULL, NULL),
(4, 'tien anh', 'ha@gmail.com', '01691234567', 'Bac Giang', NULL, 0, '2020-05-15 02:55:02', '2020-05-15 02:55:02', NULL, NULL, NULL),
(5, 'tien anh', 'ha@gmail.com', '01691234567', 'Bac Giang', NULL, 0, '2020-05-15 02:56:02', '2020-05-15 02:56:02', NULL, NULL, NULL),
(6, 'php 4-1', 'ha@gmail.com', '40', 'Bac Giang', NULL, 0, '2020-05-15 03:00:18', '2020-05-15 03:00:18', NULL, NULL, NULL),
(7, 'tien anh', 'ha@gmail.com', '01691234567', 'Tan My, Bac Giang', NULL, 0, '2020-05-15 03:06:35', '2020-05-15 03:06:35', NULL, NULL, NULL),
(8, 'tien anh', 'kien@gmail.com', '01691234567', 'hanoi', NULL, 0, '2020-05-15 03:09:32', '2020-05-15 03:09:32', NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:33', '2020-05-15 03:09:33', NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:35', '2020-05-15 03:09:35', NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:37', '2020-05-15 03:09:37', NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:39', '2020-05-15 03:09:39', NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:40', '2020-05-15 03:09:40', NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:41', '2020-05-15 03:09:41', NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:43', '2020-05-15 03:09:43', NULL, NULL, NULL),
(16, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:45', '2020-05-15 03:09:45', NULL, NULL, NULL),
(17, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:46', '2020-05-15 03:09:46', NULL, NULL, NULL),
(18, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:48', '2020-05-15 03:09:48', NULL, NULL, NULL),
(19, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:51', '2020-05-15 03:09:51', NULL, NULL, NULL),
(20, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:53', '2020-05-15 03:09:53', NULL, NULL, NULL),
(21, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:57', '2020-05-15 03:09:57', NULL, NULL, NULL),
(22, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:09:58', '2020-05-15 03:09:58', NULL, NULL, NULL),
(23, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:00', '2020-05-15 03:10:00', NULL, NULL, NULL),
(24, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:02', '2020-05-15 03:10:02', NULL, NULL, NULL),
(25, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:03', '2020-05-15 03:10:03', NULL, NULL, NULL),
(26, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:05', '2020-05-15 03:10:05', NULL, NULL, NULL),
(27, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:06', '2020-05-15 03:10:06', NULL, NULL, NULL),
(28, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:08', '2020-05-15 03:10:08', NULL, NULL, NULL),
(29, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:11', '2020-05-15 03:10:11', NULL, NULL, NULL),
(30, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:12', '2020-05-15 03:10:12', NULL, NULL, NULL),
(31, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:13', '2020-05-15 03:10:13', NULL, NULL, NULL),
(32, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:14', '2020-05-15 03:10:14', NULL, NULL, NULL),
(33, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:15', '2020-05-15 03:10:15', NULL, NULL, NULL),
(34, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:16', '2020-05-15 03:10:16', NULL, NULL, NULL),
(35, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:17', '2020-05-15 03:10:17', NULL, NULL, NULL),
(36, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:19', '2020-05-15 03:10:19', NULL, NULL, NULL),
(37, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:21', '2020-05-15 03:10:21', NULL, NULL, NULL),
(38, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:23', '2020-05-15 03:10:23', NULL, NULL, NULL),
(39, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:25', '2020-05-15 03:10:25', NULL, NULL, NULL),
(40, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:28', '2020-05-15 03:10:28', NULL, NULL, NULL),
(41, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:29', '2020-05-15 03:10:29', NULL, NULL, NULL),
(42, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:32', '2020-05-15 03:10:32', NULL, NULL, NULL),
(43, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:34', '2020-05-15 03:10:34', NULL, NULL, NULL),
(44, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:36', '2020-05-15 03:10:36', NULL, NULL, NULL),
(45, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:37', '2020-05-15 03:10:37', NULL, NULL, NULL),
(46, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:38', '2020-05-15 03:10:38', NULL, NULL, NULL),
(47, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:40', '2020-05-15 03:10:40', NULL, NULL, NULL),
(48, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:43', '2020-05-15 03:10:43', NULL, NULL, NULL),
(49, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:45', '2020-05-15 03:10:45', NULL, NULL, NULL),
(50, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:46', '2020-05-15 03:10:46', NULL, NULL, NULL),
(51, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:48', '2020-05-15 03:10:48', NULL, NULL, NULL),
(52, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:50', '2020-05-15 03:10:50', NULL, NULL, NULL),
(53, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:52', '2020-05-15 03:10:52', NULL, NULL, NULL),
(54, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:53', '2020-05-15 03:10:53', NULL, NULL, NULL),
(55, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:55', '2020-05-15 03:10:55', NULL, NULL, NULL),
(56, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:56', '2020-05-15 03:10:56', NULL, NULL, NULL),
(57, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:57', '2020-05-15 03:10:57', NULL, NULL, NULL),
(58, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:10:59', '2020-05-15 03:10:59', NULL, NULL, NULL),
(59, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:01', '2020-05-15 03:11:01', NULL, NULL, NULL),
(60, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:03', '2020-05-15 03:11:03', NULL, NULL, NULL),
(61, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:04', '2020-05-15 03:11:04', NULL, NULL, NULL),
(62, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:05', '2020-05-15 03:11:05', NULL, NULL, NULL),
(63, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:06', '2020-05-15 03:11:06', NULL, NULL, NULL),
(64, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:07', '2020-05-15 03:11:07', NULL, NULL, NULL),
(65, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:09', '2020-05-15 03:11:09', NULL, NULL, NULL),
(66, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:11', '2020-05-15 03:11:11', NULL, NULL, NULL),
(67, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:12', '2020-05-15 03:11:12', NULL, NULL, NULL),
(68, 'tien anh', 'kien@gmail.com', '01691234567', 'hanoi', NULL, 0, '2020-05-15 03:11:44', '2020-05-15 03:11:44', NULL, NULL, NULL),
(69, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:46', '2020-05-15 03:11:46', NULL, NULL, NULL),
(70, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:48', '2020-05-15 03:11:48', NULL, NULL, NULL),
(71, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:50', '2020-05-15 03:11:50', NULL, NULL, NULL),
(72, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:52', '2020-05-15 03:11:52', NULL, NULL, NULL),
(73, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:54', '2020-05-15 03:11:54', NULL, NULL, NULL),
(74, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:56', '2020-05-15 03:11:56', NULL, NULL, NULL),
(75, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:11:58', '2020-05-15 03:11:58', NULL, NULL, NULL),
(76, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:09', '2020-05-15 03:12:09', NULL, NULL, NULL),
(77, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:11', '2020-05-15 03:12:11', NULL, NULL, NULL),
(78, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:13', '2020-05-15 03:12:13', NULL, NULL, NULL),
(79, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:15', '2020-05-15 03:12:15', NULL, NULL, NULL),
(80, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:17', '2020-05-15 03:12:17', NULL, NULL, NULL),
(81, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:19', '2020-05-15 03:12:19', NULL, NULL, NULL),
(82, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:21', '2020-05-15 03:12:21', NULL, NULL, NULL),
(83, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:22', '2020-05-15 03:12:22', NULL, NULL, NULL),
(84, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:24', '2020-05-15 03:12:24', NULL, NULL, NULL),
(85, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:27', '2020-05-15 03:12:27', NULL, NULL, NULL),
(86, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:27', '2020-05-15 03:12:27', NULL, NULL, NULL),
(87, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:29', '2020-05-15 03:12:29', NULL, NULL, NULL),
(88, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:30', '2020-05-15 03:12:30', NULL, NULL, NULL),
(89, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:31', '2020-05-15 03:12:31', NULL, NULL, NULL),
(90, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:32', '2020-05-15 03:12:32', NULL, NULL, NULL),
(91, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:33', '2020-05-15 03:12:33', NULL, NULL, NULL),
(92, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:34', '2020-05-15 03:12:34', NULL, NULL, NULL),
(93, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:35', '2020-05-15 03:12:35', NULL, NULL, NULL),
(94, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:35', '2020-05-15 03:12:35', NULL, NULL, NULL),
(95, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:37', '2020-05-15 03:12:37', NULL, NULL, NULL),
(96, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:41', '2020-05-15 03:12:41', NULL, NULL, NULL),
(97, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:43', '2020-05-15 03:12:43', NULL, NULL, NULL),
(98, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:45', '2020-05-15 03:12:45', NULL, NULL, NULL),
(99, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:47', '2020-05-15 03:12:47', NULL, NULL, NULL),
(100, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:48', '2020-05-15 03:12:48', NULL, NULL, NULL),
(101, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:51', '2020-05-15 03:12:51', NULL, NULL, NULL),
(102, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:52', '2020-05-15 03:12:52', NULL, NULL, NULL),
(103, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:54', '2020-05-15 03:12:54', NULL, NULL, NULL),
(104, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:12:57', '2020-05-15 03:12:57', NULL, NULL, NULL),
(105, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:00', '2020-05-15 03:13:00', NULL, NULL, NULL),
(106, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:02', '2020-05-15 03:13:02', NULL, NULL, NULL),
(107, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:03', '2020-05-15 03:13:03', NULL, NULL, NULL),
(108, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:05', '2020-05-15 03:13:05', NULL, NULL, NULL),
(109, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:07', '2020-05-15 03:13:07', NULL, NULL, NULL),
(110, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:10', '2020-05-15 03:13:10', NULL, NULL, NULL),
(111, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:12', '2020-05-15 03:13:12', NULL, NULL, NULL),
(112, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:13', '2020-05-15 03:13:13', NULL, NULL, NULL),
(113, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:15', '2020-05-15 03:13:15', NULL, NULL, NULL),
(114, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:17', '2020-05-15 03:13:17', NULL, NULL, NULL),
(115, NULL, NULL, NULL, NULL, NULL, 0, '2020-05-15 03:13:19', '2020-05-15 03:13:19', NULL, NULL, NULL),
(116, 'haviet', 'kien@gmail.com', '01234567893', 'ha noi, ho tay', NULL, 0, '2020-05-15 03:15:29', '2020-05-15 03:15:29', NULL, NULL, NULL),
(117, 'flannel', 'ha.nguyen.jvb@gmail.com', '01234567893', 'Tan My, Bac Giang', NULL, 0, '2020-05-17 21:02:50', '2020-05-17 21:02:50', NULL, NULL, NULL),
(118, 'flannel', 'ha.nguyen.jvb@gmail.com', '01234567893', 'Tan My, Bac Giang', NULL, 0, '2020-05-17 21:06:44', '2020-05-17 21:06:44', NULL, NULL, NULL),
(119, 'flannel', 'ha.nguyen.jvb@gmail.com', '01234567893', 'Tan My, Bac Giang', NULL, 0, '2020-05-17 21:21:42', '2020-05-17 21:21:42', NULL, NULL, NULL),
(120, 'tien anh', 'ha@gmail.com', '40', 'hanoi', NULL, 0, '2020-05-17 21:22:16', '2020-05-17 21:22:16', NULL, NULL, NULL),
(121, 'nam', 'nam@gmail.com', '01691234567', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 21:24:58', '2020-05-17 21:24:58', NULL, NULL, NULL),
(122, 'nam', 'nam@gmail.com', '01691234567', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 21:26:01', '2020-05-17 21:26:01', NULL, NULL, NULL),
(123, 'tien anh', 'thao@gmail.com', '123', 'ha noi', NULL, 0, '2020-05-17 23:00:15', '2020-05-17 23:00:15', NULL, NULL, NULL),
(124, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:03:25', '2020-05-17 23:03:25', NULL, NULL, NULL),
(125, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:11:37', '2020-05-17 23:11:37', NULL, NULL, NULL),
(126, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:12:26', '2020-05-17 23:12:26', NULL, NULL, NULL),
(127, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:12:33', '2020-05-17 23:12:33', NULL, NULL, NULL),
(128, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:13:53', '2020-05-17 23:13:53', NULL, NULL, NULL),
(129, 'nam', '123@gmail.com', '0167', 'fdfdfdfdffffffffff', NULL, 0, '2020-05-17 23:14:19', '2020-05-17 23:14:19', NULL, NULL, NULL),
(130, 'nam', '123@gmail.com', '123456', 'ha noi', NULL, 0, '2020-05-17 23:16:25', '2020-05-17 23:16:25', NULL, NULL, NULL),
(131, 'tien anh', 'tienanh@gmail.com', '01691234567', 'Tan My, Bac Giang', NULL, 0, '2020-05-17 23:19:06', '2020-05-17 23:19:06', NULL, NULL, NULL),
(132, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:48:09', '2020-05-18 00:48:09', NULL, NULL, NULL),
(133, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:49:04', '2020-05-18 00:49:04', NULL, NULL, NULL),
(134, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:49:50', '2020-05-18 00:49:50', NULL, NULL, NULL),
(135, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:50:19', '2020-05-18 00:50:19', NULL, NULL, NULL),
(136, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:51:13', '2020-05-18 00:51:13', NULL, NULL, NULL),
(137, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:52:19', '2020-05-18 00:52:19', NULL, NULL, NULL),
(138, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:52:30', '2020-05-18 00:52:30', NULL, NULL, NULL),
(139, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:54:04', '2020-05-18 00:54:04', NULL, NULL, NULL),
(140, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:54:47', '2020-05-18 00:54:47', NULL, NULL, NULL),
(141, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:55:32', '2020-05-18 00:55:32', NULL, NULL, NULL),
(142, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:55:49', '2020-05-18 00:55:49', NULL, NULL, NULL),
(143, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:56:41', '2020-05-18 00:56:41', NULL, NULL, NULL),
(144, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 00:59:42', '2020-05-18 00:59:42', NULL, NULL, NULL),
(145, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:09:42', '2020-05-18 01:09:42', NULL, NULL, NULL),
(146, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:11:35', '2020-05-18 01:11:35', NULL, NULL, NULL),
(147, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:12:21', '2020-05-18 01:12:21', NULL, NULL, NULL),
(148, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:15:34', '2020-05-18 01:15:34', NULL, NULL, NULL),
(149, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:16:14', '2020-05-18 01:16:14', NULL, NULL, NULL),
(150, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:17:02', '2020-05-18 01:17:02', NULL, NULL, NULL),
(151, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-18 01:17:35', '2020-05-18 01:17:35', NULL, NULL, NULL),
(152, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-18 01:21:53', '2020-05-18 01:21:53', NULL, NULL, NULL),
(153, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 01:37:54', '2020-05-18 01:37:54', NULL, NULL, NULL),
(154, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 01:43:01', '2020-05-18 01:43:01', NULL, NULL, NULL),
(155, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 01:59:22', '2020-05-18 01:59:22', NULL, NULL, NULL),
(156, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 01:59:59', '2020-05-18 01:59:59', NULL, NULL, NULL),
(157, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 02:08:09', '2020-05-18 02:08:09', NULL, NULL, NULL),
(158, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-18 02:12:42', '2020-05-18 02:12:42', NULL, NULL, NULL),
(159, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 02:18:07', '2020-05-18 02:18:07', NULL, NULL, NULL),
(160, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-18 02:27:33', '2020-05-18 02:27:33', NULL, NULL, NULL),
(161, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:31:54', '2020-05-18 02:31:54', NULL, NULL, NULL),
(162, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:32:43', '2020-05-18 02:32:43', NULL, NULL, NULL),
(163, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:34:53', '2020-05-18 02:34:53', NULL, NULL, NULL),
(164, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:35:59', '2020-05-18 02:35:59', NULL, NULL, NULL),
(165, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:36:29', '2020-05-18 02:36:29', NULL, NULL, NULL),
(166, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:37:04', '2020-05-18 02:37:04', NULL, NULL, NULL),
(167, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 02:37:18', '2020-05-18 02:37:18', NULL, NULL, NULL),
(168, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-18 02:54:54', '2020-05-18 02:54:54', NULL, NULL, NULL),
(169, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-18 02:56:40', '2020-05-18 02:56:40', NULL, NULL, NULL),
(170, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 02:59:54', '2020-05-18 02:59:54', NULL, NULL, NULL),
(171, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 03:10:13', '2020-05-18 03:10:13', NULL, NULL, NULL),
(172, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 03:10:58', '2020-05-18 03:10:58', NULL, NULL, NULL),
(173, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 03:11:43', '2020-05-18 03:11:43', NULL, NULL, NULL),
(174, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-18 03:19:11', '2020-05-18 03:19:11', NULL, NULL, NULL),
(175, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 03:23:56', '2020-05-18 03:23:56', NULL, NULL, NULL),
(176, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'fdfdfdfdffffffffff', NULL, 0, '2020-05-18 03:26:56', '2020-05-18 03:26:56', NULL, NULL, NULL),
(177, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'fdfdfdfdffffffffff', NULL, 0, '2020-05-18 03:28:50', '2020-05-18 03:28:50', NULL, NULL, NULL),
(178, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'fdfdfdfdffffffffff', NULL, 0, '2020-05-18 03:33:32', '2020-05-18 03:33:32', NULL, NULL, NULL),
(179, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'sai gon', NULL, 0, '2020-05-18 09:31:11', '2020-05-18 09:31:11', NULL, NULL, NULL),
(180, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'sai gon', NULL, 0, '2020-05-18 09:54:24', '2020-05-18 09:54:24', NULL, NULL, NULL),
(181, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'sai gon', NULL, 0, '2020-05-18 09:55:10', '2020-05-18 09:55:10', NULL, NULL, NULL),
(182, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 18:24:12', '2020-05-18 18:24:12', NULL, NULL, NULL),
(183, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 18:40:55', '2020-05-18 18:40:55', NULL, NULL, NULL),
(184, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 18:41:46', '2020-05-18 18:41:46', NULL, NULL, NULL),
(185, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-18 18:41:56', '2020-05-18 18:41:56', NULL, NULL, NULL),
(186, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'my dinh', NULL, 0, '2020-05-18 18:50:08', '2020-05-18 18:50:08', NULL, NULL, NULL),
(187, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-18 18:52:15', '2020-05-18 18:52:15', NULL, NULL, NULL),
(188, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'my dinh', NULL, 0, '2020-05-18 18:57:02', '2020-05-18 18:57:02', NULL, NULL, NULL),
(191, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'my dinh', NULL, 0, '2020-05-18 18:59:51', '2020-05-18 18:59:51', NULL, NULL, NULL),
(192, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha dong ha noi', NULL, 0, '2020-05-18 19:26:45', '2020-05-18 19:26:45', NULL, NULL, NULL),
(193, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha dong ha noi', NULL, 0, '2020-05-18 19:27:33', '2020-05-18 19:27:33', NULL, NULL, NULL),
(194, NULL, 'khanh.nguyenngoc.37604@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-18 19:29:21', '2020-05-18 19:29:21', NULL, NULL, NULL),
(195, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-19 02:55:11', '2020-05-19 02:55:11', NULL, NULL, NULL),
(196, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'hanoi', NULL, 0, '2020-05-19 02:57:32', '2020-05-19 02:57:32', NULL, NULL, NULL),
(197, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:05:40', '2020-05-19 03:05:40', NULL, NULL, NULL),
(198, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:07:57', '2020-05-19 03:07:57', NULL, NULL, NULL),
(199, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:09:08', '2020-05-19 03:09:08', NULL, NULL, NULL),
(200, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:09:53', '2020-05-19 03:09:53', NULL, NULL, NULL),
(201, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:10:13', '2020-05-19 03:10:13', NULL, NULL, NULL),
(202, 'haviet dzzzz', '123444444', 'ha.nguyen.jvb@gmail.com', 'ha noi, ho tay', NULL, 0, '2020-05-19 03:13:58', '2020-05-19 03:13:58', NULL, NULL, NULL),
(203, 'haviet dzzzz', '123444444', 'ha.nguyen.jvb@gmail.com', 'ha noi, ho tay', NULL, 0, '2020-05-19 03:15:21', '2020-05-19 03:15:21', NULL, NULL, NULL),
(204, 'haviet dzzzz', '123444444', 'ha.nguyen.jvb@gmail.com', 'ha noi, ho tay', NULL, 0, '2020-05-19 03:19:04', '2020-05-19 03:19:04', NULL, NULL, NULL),
(205, NULL, 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 03:19:52', '2020-05-19 03:19:52', NULL, NULL, NULL),
(206, 'haviet dzzzz', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 03:20:13', '2020-05-19 03:20:13', NULL, NULL, NULL),
(207, 'ha vvvvvvvvvvvvvvvccccc', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi', NULL, 0, '2020-05-19 03:21:58', '2020-05-19 03:21:58', NULL, NULL, NULL),
(208, 'ha final', 'ha.nguyen.jvb@gmail.com', NULL, 'ha dong ha noi', NULL, 0, '2020-05-19 03:26:52', '2020-05-19 03:26:52', NULL, NULL, NULL),
(209, 'tien anh', NULL, NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-19 03:31:11', '2020-05-19 03:31:11', NULL, NULL, NULL),
(210, 'tien anh', 'ha.nguyen.jvb@gmail.com', NULL, 'Tan My, Bac Giang', NULL, 0, '2020-05-19 03:31:24', '2020-05-19 03:31:24', NULL, NULL, NULL),
(211, 'tien anh', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 03:33:30', '2020-05-19 03:33:30', NULL, NULL, NULL),
(212, 'tien anh', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 03:33:52', '2020-05-19 03:33:52', NULL, NULL, NULL),
(213, 'ha oi ha', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 03:55:19', '2020-05-19 03:55:19', NULL, NULL, NULL),
(214, 'nguyen viet ha', 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-19 04:10:30', '2020-05-19 04:10:30', NULL, NULL, NULL),
(215, 'ha final fianl', 'ha.nguyen.jvb@gmail.com', NULL, 'Bac Giang', NULL, 0, '2020-05-19 04:12:36', '2020-05-19 04:12:36', NULL, NULL, NULL),
(216, 'hanguyen', 'ha.nguyen.jvb@gmail.com', NULL, 'ha noi, ho tay', NULL, 0, '2020-05-19 16:05:09', '2020-05-19 16:05:09', NULL, NULL, NULL),
(221, 'havietnguyen123', 'ha.nguyen.jvb@gmail.com', NULL, 'TP. Bắc Giang', NULL, 2, '2020-05-22 18:39:05', '2020-06-03 08:19:32', NULL, NULL, NULL),
(232, 'vinh xuan nguyen', 'ha.nguyen.jvb@gmail.com', '0975271876', 'Bac Giang', NULL, 2, '2020-05-26 01:41:54', '2020-06-02 07:38:10', NULL, NULL, NULL),
(237, 'nguen ha', 'ha.nguyen.jvb@gmail.com', '0399888567', 'Bac Giang', NULL, 2, '2020-05-26 01:56:21', '2020-06-02 07:30:13', NULL, NULL, NULL),
(238, 'nguyen viet ha raver', 'ha.nguyen.jvb@gmail.com', '0399881175', 'Ha Dong, Ha Noi', NULL, 2, '2020-05-26 02:01:02', '2020-05-27 10:27:05', NULL, NULL, NULL),
(239, 'ha viet 0110 jvb', 'ha.nguyen.jvb@gmail.com', '0388997775', 'Bac Giang', NULL, 2, '2020-05-26 10:23:26', '2020-05-27 10:54:50', NULL, NULL, 'ssan pham dep'),
(240, 'ha viet 0110 jvb', 'ha.nguyen.jvb@gmail.com', '0388997775', 'Bac Giang', NULL, 2, '2020-05-26 10:26:11', '2020-06-02 07:27:57', NULL, NULL, 'ssan pham dep'),
(249, 'Hoang Tien Anhhyyyy', 'ha.nguyen.jvb@gmail.com', '0399888567', 'Bac Giang', NULL, 2, '2020-06-03 08:29:12', '2020-06-03 08:30:34', NULL, NULL, 'san pham dep');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_product` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `slug`, `image_product`, `product_id`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '121186-1.jpg', 36, NULL, NULL),
(2, NULL, NULL, '121186-2.jpg', 36, NULL, NULL),
(3, NULL, NULL, '121186-3.jpg', 36, NULL, NULL),
(4, NULL, NULL, '121186-4.jpg', 36, NULL, NULL),
(5, NULL, NULL, '121186-5.jpg', 36, NULL, NULL),
(6, 'chuck taylor all star 1970s hoop hunter-1', NULL, 'chuck-taylor-all-star-1970s-hoop-hunter-1.png', 35, NULL, NULL),
(7, NULL, NULL, '165912c-2.png', 35, NULL, NULL),
(8, NULL, NULL, '165912c-3.png', 35, NULL, NULL),
(9, NULL, NULL, '165912c-4.png', 35, NULL, NULL),
(10, 'chuck taylor all star 1970s hoop hunter-5', NULL, 'chuck-taylor-all-star-1970s-hoop-hunter-5.png', 35, NULL, NULL),
(11, 'Vans Sk8 Hi Black White-1', NULL, 'vans-sk8-hi-black-white-1.png', 16, NULL, NULL),
(12, 'Vans Sk8 Hi Black White-2', NULL, 'vans-sk8-hi-black-white-2.png', 16, NULL, NULL),
(13, 'Vans Sk8 Hi Black White-3', NULL, 'vans-sk8-hi-black-white-3.png', 16, NULL, NULL),
(14, NULL, NULL, 'vn000d5ib8c-6.png', 16, NULL, NULL),
(15, NULL, NULL, 'vn000d5ib8c-4.png', 16, NULL, NULL),
(16, 'converse chuck taylor all star 1970s flight school leather silver hi-1', NULL, 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi-1.jpg', 37, NULL, NULL),
(17, 'converse chuck taylor all star 1970s flight school leather silver hi-2', NULL, 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi-2.jpg', 37, NULL, NULL),
(18, NULL, NULL, '165050c-2-0f652f27-e9fa-4b6b-9294-87a6b4704b06.jpg', 37, NULL, NULL),
(19, NULL, NULL, '165050c-4-ff75df8e-b9eb-4f2a-b541-cbcf977046dd.jpg', 37, NULL, NULL),
(20, 'converse chuck taylor all star 1970s flight school leather silver hi-3', NULL, 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi-3.jpg', 37, NULL, NULL),
(21, NULL, NULL, '166815c-1.jpg', 38, NULL, NULL),
(22, NULL, NULL, '166815c-2.jpg', 38, NULL, NULL),
(23, NULL, NULL, '166815c-3.jpg', 38, NULL, NULL),
(24, NULL, NULL, '166815c-4.jpg', 38, NULL, NULL),
(25, NULL, NULL, '166815c-6.jpg', 38, NULL, NULL),
(26, 'chuck taylor all star classic 121176-1', NULL, 'chuck-taylor-all-star-classic-121176-1.jpg', 34, NULL, NULL),
(27, NULL, NULL, '121176-1.png', 34, NULL, NULL),
(28, NULL, NULL, '121176-2.png', 34, NULL, NULL),
(29, NULL, NULL, '.jpg', 34, NULL, NULL),
(30, 'Converse Chuck Taylor All Star VLTG - Back to Earth-1', NULL, 'converse-chuck-taylor-all-star-vltg-back-to-earth-1.png', 14, NULL, NULL),
(31, 'Converse Chuck Taylor All Star VLTG - Back to Earth-2', NULL, '.png', 14, NULL, NULL),
(32, NULL, NULL, '567046v-5.png', 14, NULL, NULL),
(33, NULL, NULL, '567046v-6.png', 14, NULL, NULL),
(34, NULL, NULL, '567046v-8.png', 14, NULL, NULL),
(35, 'converse chuck taylor all star classic navy-1', NULL, 'converse-chuck-taylor-all-star-classic-navy-1.png', 39, NULL, NULL),
(36, NULL, NULL, '121185-6.jpg', 39, NULL, NULL),
(37, NULL, NULL, '127440-1.png', 39, NULL, NULL),
(38, 'converse chuck taylor all star classic navy-4', NULL, 'converse-chuck-taylor-all-star-classic-navy-4.png', 39, NULL, NULL),
(44, NULL, NULL, '167070c-2.jpg', 41, NULL, NULL),
(45, 'converse chuck taylor all star cheerful-2', NULL, 'converse-chuck-taylor-all-star-cheerful-2.jpg', 41, NULL, NULL),
(46, 'converse chuck taylor all star cheerful-3', NULL, 'converse-chuck-taylor-all-star-cheerful-3.jpg', 41, NULL, NULL),
(47, NULL, NULL, '167070c-5.jpg', 41, NULL, NULL),
(48, NULL, NULL, '167070c-1.jpg', 41, NULL, NULL),
(49, NULL, NULL, 'vn0a38g2pxc-1.jpg', 10, NULL, NULL),
(50, NULL, NULL, 'vn0a38g2pxc-2.jpg', 10, NULL, NULL),
(51, NULL, NULL, 'vn0a38g2pxc-3.jpg', 10, NULL, NULL),
(52, NULL, NULL, 'vn0a38g2pxc-5.jpg', 10, NULL, NULL),
(53, NULL, NULL, 'vn0a38g2pxc-6.jpg', 10, NULL, NULL),
(54, 'vans old skool pig suede-1', NULL, 'vans-old-skool-pig-suede-1.png', 9, NULL, NULL),
(55, NULL, NULL, 'vn0a4bv5v77.png', 9, NULL, NULL),
(56, NULL, NULL, 'vn0a4bv5v77-1.png', 9, NULL, NULL),
(57, NULL, NULL, 'vn0a4bv5v77-2.png', 9, NULL, NULL),
(58, 'vans old skool pig suede-5', NULL, 'vans-old-skool-pig-suede-5.png', 9, NULL, NULL),
(59, NULL, NULL, 'vn0a3jexpu1-1.png', 15, NULL, NULL),
(60, NULL, NULL, 'vn0a3jexpu1-3.png', 15, NULL, NULL),
(61, NULL, NULL, 'vn0a3jexpu1-3-0c142b35-f212-487c-b206-656de35de9e5.png', 15, NULL, NULL),
(62, NULL, NULL, 'vn0a3jexpu1-4.png', 15, NULL, NULL),
(63, NULL, NULL, '121185-1.jpg', 42, NULL, NULL),
(64, NULL, NULL, '121185-2.jpg', 42, NULL, NULL),
(65, NULL, NULL, '121185-3.jpg', 42, NULL, NULL),
(66, NULL, NULL, '121185-4.jpg', 42, NULL, NULL),
(67, NULL, NULL, '121185-5.jpg', 42, NULL, NULL),
(68, NULL, NULL, '162054-4.png', 43, NULL, NULL),
(69, NULL, NULL, '162054-5.png', 43, NULL, NULL),
(70, NULL, NULL, '162054-1.png', 43, NULL, NULL),
(71, NULL, NULL, '162054-2.png', 43, NULL, NULL),
(72, NULL, NULL, '162054-3.png', 43, NULL, NULL),
(73, NULL, NULL, '121176-2.png', 44, NULL, NULL),
(74, NULL, NULL, '121176-3 - Copy.png', 44, NULL, NULL),
(75, NULL, NULL, '166747c-3.jpg', 45, NULL, NULL),
(76, NULL, NULL, '166747c-4.jpg', 45, NULL, NULL),
(77, NULL, NULL, '166747c-6.jpg', 45, NULL, NULL),
(80, 'convserse11', NULL, 'convserse11.jpg', 46, NULL, NULL),
(81, NULL, NULL, '121185-5.jpg', 46, NULL, NULL),
(82, NULL, NULL, '121186-4.jpg', 46, NULL, NULL),
(83, NULL, NULL, '127440-3.png', 46, NULL, NULL),
(84, NULL, NULL, '165050c-2-0f652f27-e9fa-4b6b-9294-87a6b4704b06.jpg', 46, NULL, NULL),
(85, NULL, NULL, '165050c-4-ff75df8e-b9eb-4f2a-b541-cbcf977046dd.jpg', 46, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2020_04_21_041244_create_categories_table', 1),
(8, '2020_04_21_041327_create_products_table', 1),
(9, '2020_04_22_045225_update_users_table', 2),
(10, '2020_04_22_065236_create_orders_table', 3),
(38, '2018_12_23_120000_create_shoppingcart_table', 4),
(39, '2020_05_03_161557_update_fourth_time_users_table', 5),
(40, '2020_05_04_014650_update_products_table', 5),
(41, '2020_05_04_095506_update_second_time_products_table', 5),
(42, '2020_05_05_190702_create_images_table', 6),
(43, '2020_05_06_044640_update_images_table', 6),
(44, '2020_05_07_092557_create_attributes_table', 6),
(45, '2020_05_07_093823_create_values_attribute_table', 6),
(46, '2020_05_07_093926_create_values_product_table', 6),
(47, '2020_05_07_094121_create_variants_table', 6),
(48, '2020_05_07_094218_create_variant_values_table', 6),
(49, '2020_05_15_041145_create_cutomers_table', 6),
(50, '2020_05_15_070002_customer_orders', 7),
(51, '2020_05_15_070604_order_attrs', 8),
(52, '2020_05_25_151934_update_cutomers_table', 9),
(53, '2020_05_25_153207_update_second_time_cutomers_table', 10),
(54, '2020_05_26_155126_update_third_time_products_table', 11),
(55, '2020_05_27_135921_update_fourth_time_products_table', 12),
(56, '2020_05_28_143645_update_fifth_time_users_table', 13),
(57, '2020_05_29_102208_create_clients_table', 14);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `fullname`, `address`, `phone`, `total`, `state`, `created_at`, `updated_at`) VALUES
(42, 'Nguyen Cong Minh', 'Soc Son, Ha Noi', '03525246673', '100000', 1, NULL, NULL),
(43, 'Pham Quang Huy', 'Hai Phong', '0354879500', '100000', 0, NULL, '2020-05-07 03:35:24'),
(44, 'Truong Thi Hau', 'Thai Binh', '0354879567', '100000', 0, NULL, NULL),
(45, 'Nguyen Phuong Thuy', 'Thai Nguyen', '0354871237', '100000', 0, NULL, NULL),
(46, 'Bui Trong Hoan', 'Nam Dinh', '034412345', '100000', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_attrs`
--

CREATE TABLE `order_attrs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customerorder_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_attrs`
--

INSERT INTO `order_attrs` (`id`, `name`, `value`, `customerorder_id`, `created_at`, `updated_at`) VALUES
(1, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 3, '2020-05-15 02:55:03', '2020-05-15 02:55:03'),
(2, 'Color', 'Black/White', 3, '2020-05-15 02:55:03', '2020-05-15 02:55:03'),
(3, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 4, '2020-05-15 02:56:02', '2020-05-15 02:56:02'),
(4, 'Color', 'Black/White', 4, '2020-05-15 02:56:03', '2020-05-15 02:56:03'),
(5, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 5, '2020-05-15 03:00:19', '2020-05-15 03:00:19'),
(6, 'Color', 'Black/White', 5, '2020-05-15 03:00:19', '2020-05-15 03:00:19'),
(7, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 6, '2020-05-15 03:06:36', '2020-05-15 03:06:36'),
(8, 'Color', 'Black/White', 6, '2020-05-15 03:06:36', '2020-05-15 03:06:36'),
(9, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 7, '2020-05-15 03:09:32', '2020-05-15 03:09:32'),
(10, 'Color', 'Black/White', 7, '2020-05-15 03:09:33', '2020-05-15 03:09:33'),
(11, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 8, '2020-05-15 03:09:34', '2020-05-15 03:09:34'),
(12, 'Color', 'Black/White', 8, '2020-05-15 03:09:34', '2020-05-15 03:09:34'),
(13, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 9, '2020-05-15 03:09:35', '2020-05-15 03:09:35'),
(14, 'Color', 'Black/White', 9, '2020-05-15 03:09:36', '2020-05-15 03:09:36'),
(15, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 10, '2020-05-15 03:09:37', '2020-05-15 03:09:37'),
(16, 'Color', 'Black/White', 10, '2020-05-15 03:09:38', '2020-05-15 03:09:38'),
(17, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 11, '2020-05-15 03:09:39', '2020-05-15 03:09:39'),
(18, 'Color', 'Black/White', 11, '2020-05-15 03:09:39', '2020-05-15 03:09:39'),
(19, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 12, '2020-05-15 03:09:41', '2020-05-15 03:09:41'),
(20, 'Color', 'Black/White', 12, '2020-05-15 03:09:41', '2020-05-15 03:09:41'),
(21, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 13, '2020-05-15 03:09:42', '2020-05-15 03:09:42'),
(22, 'Color', 'Black/White', 13, '2020-05-15 03:09:42', '2020-05-15 03:09:42'),
(23, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 14, '2020-05-15 03:09:44', '2020-05-15 03:09:44'),
(24, 'Color', 'Black/White', 14, '2020-05-15 03:09:44', '2020-05-15 03:09:44'),
(25, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 15, '2020-05-15 03:09:45', '2020-05-15 03:09:45'),
(26, 'Color', 'Black/White', 15, '2020-05-15 03:09:45', '2020-05-15 03:09:45'),
(27, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 16, '2020-05-15 03:09:47', '2020-05-15 03:09:47'),
(28, 'Color', 'Black/White', 16, '2020-05-15 03:09:47', '2020-05-15 03:09:47'),
(29, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 17, '2020-05-15 03:09:50', '2020-05-15 03:09:50'),
(30, 'Color', 'Black/White', 17, '2020-05-15 03:09:50', '2020-05-15 03:09:50'),
(31, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 18, '2020-05-15 03:09:52', '2020-05-15 03:09:52'),
(32, 'Color', 'Black/White', 18, '2020-05-15 03:09:52', '2020-05-15 03:09:52'),
(33, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 19, '2020-05-15 03:09:54', '2020-05-15 03:09:54'),
(34, 'Color', 'Black/White', 19, '2020-05-15 03:09:56', '2020-05-15 03:09:56'),
(35, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 20, '2020-05-15 03:09:57', '2020-05-15 03:09:57'),
(36, 'Color', 'Black/White', 20, '2020-05-15 03:09:58', '2020-05-15 03:09:58'),
(37, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 21, '2020-05-15 03:09:59', '2020-05-15 03:09:59'),
(38, 'Color', 'Black/White', 21, '2020-05-15 03:10:00', '2020-05-15 03:10:00'),
(39, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 22, '2020-05-15 03:10:01', '2020-05-15 03:10:01'),
(40, 'Color', 'Black/White', 22, '2020-05-15 03:10:01', '2020-05-15 03:10:01'),
(41, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 23, '2020-05-15 03:10:02', '2020-05-15 03:10:02'),
(42, 'Color', 'Black/White', 23, '2020-05-15 03:10:02', '2020-05-15 03:10:02'),
(43, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 24, '2020-05-15 03:10:03', '2020-05-15 03:10:03'),
(44, 'Color', 'Black/White', 24, '2020-05-15 03:10:04', '2020-05-15 03:10:04'),
(45, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 25, '2020-05-15 03:10:05', '2020-05-15 03:10:05'),
(46, 'Color', 'Black/White', 25, '2020-05-15 03:10:06', '2020-05-15 03:10:06'),
(47, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 26, '2020-05-15 03:10:07', '2020-05-15 03:10:07'),
(48, 'Color', 'Black/White', 26, '2020-05-15 03:10:07', '2020-05-15 03:10:07'),
(49, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 27, '2020-05-15 03:10:10', '2020-05-15 03:10:10'),
(50, 'Color', 'Black/White', 27, '2020-05-15 03:10:10', '2020-05-15 03:10:10'),
(51, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 28, '2020-05-15 03:10:11', '2020-05-15 03:10:11'),
(52, 'Color', 'Black/White', 28, '2020-05-15 03:10:11', '2020-05-15 03:10:11'),
(53, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 29, '2020-05-15 03:10:12', '2020-05-15 03:10:12'),
(54, 'Color', 'Black/White', 29, '2020-05-15 03:10:12', '2020-05-15 03:10:12'),
(55, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 30, '2020-05-15 03:10:13', '2020-05-15 03:10:13'),
(56, 'Color', 'Black/White', 30, '2020-05-15 03:10:13', '2020-05-15 03:10:13'),
(57, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 31, '2020-05-15 03:10:14', '2020-05-15 03:10:14'),
(58, 'Color', 'Black/White', 31, '2020-05-15 03:10:14', '2020-05-15 03:10:14'),
(59, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 32, '2020-05-15 03:10:15', '2020-05-15 03:10:15'),
(60, 'Color', 'Black/White', 32, '2020-05-15 03:10:15', '2020-05-15 03:10:15'),
(61, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 33, '2020-05-15 03:10:16', '2020-05-15 03:10:16'),
(62, 'Color', 'Black/White', 33, '2020-05-15 03:10:16', '2020-05-15 03:10:16'),
(63, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 34, '2020-05-15 03:10:18', '2020-05-15 03:10:18'),
(64, 'Color', 'Black/White', 34, '2020-05-15 03:10:18', '2020-05-15 03:10:18'),
(65, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 35, '2020-05-15 03:10:20', '2020-05-15 03:10:20'),
(66, 'Color', 'Black/White', 35, '2020-05-15 03:10:20', '2020-05-15 03:10:20'),
(67, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 36, '2020-05-15 03:10:22', '2020-05-15 03:10:22'),
(68, 'Color', 'Black/White', 36, '2020-05-15 03:10:23', '2020-05-15 03:10:23'),
(69, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 37, '2020-05-15 03:10:24', '2020-05-15 03:10:24'),
(70, 'Color', 'Black/White', 37, '2020-05-15 03:10:25', '2020-05-15 03:10:25'),
(71, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 38, '2020-05-15 03:10:27', '2020-05-15 03:10:27'),
(72, 'Color', 'Black/White', 38, '2020-05-15 03:10:27', '2020-05-15 03:10:27'),
(73, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 39, '2020-05-15 03:10:28', '2020-05-15 03:10:28'),
(74, 'Color', 'Black/White', 39, '2020-05-15 03:10:28', '2020-05-15 03:10:28'),
(75, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 40, '2020-05-15 03:10:30', '2020-05-15 03:10:30'),
(76, 'Color', 'Black/White', 40, '2020-05-15 03:10:31', '2020-05-15 03:10:31'),
(77, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 41, '2020-05-15 03:10:33', '2020-05-15 03:10:33'),
(78, 'Color', 'Black/White', 41, '2020-05-15 03:10:33', '2020-05-15 03:10:33'),
(79, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 42, '2020-05-15 03:10:35', '2020-05-15 03:10:35'),
(80, 'Color', 'Black/White', 42, '2020-05-15 03:10:35', '2020-05-15 03:10:35'),
(81, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 43, '2020-05-15 03:10:36', '2020-05-15 03:10:36'),
(82, 'Color', 'Black/White', 43, '2020-05-15 03:10:36', '2020-05-15 03:10:36'),
(83, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 44, '2020-05-15 03:10:37', '2020-05-15 03:10:37'),
(84, 'Color', 'Black/White', 44, '2020-05-15 03:10:38', '2020-05-15 03:10:38'),
(85, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 45, '2020-05-15 03:10:39', '2020-05-15 03:10:39'),
(86, 'Color', 'Black/White', 45, '2020-05-15 03:10:40', '2020-05-15 03:10:40'),
(87, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 46, '2020-05-15 03:10:40', '2020-05-15 03:10:40'),
(88, 'Color', 'Black/White', 46, '2020-05-15 03:10:40', '2020-05-15 03:10:40'),
(89, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 47, '2020-05-15 03:10:44', '2020-05-15 03:10:44'),
(90, 'Color', 'Black/White', 47, '2020-05-15 03:10:45', '2020-05-15 03:10:45'),
(91, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 48, '2020-05-15 03:10:46', '2020-05-15 03:10:46'),
(92, 'Color', 'Black/White', 48, '2020-05-15 03:10:46', '2020-05-15 03:10:46'),
(93, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 49, '2020-05-15 03:10:47', '2020-05-15 03:10:47'),
(94, 'Color', 'Black/White', 49, '2020-05-15 03:10:47', '2020-05-15 03:10:47'),
(95, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 50, '2020-05-15 03:10:49', '2020-05-15 03:10:49'),
(96, 'Color', 'Black/White', 50, '2020-05-15 03:10:49', '2020-05-15 03:10:49'),
(97, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 51, '2020-05-15 03:10:51', '2020-05-15 03:10:51'),
(98, 'Color', 'Black/White', 51, '2020-05-15 03:10:51', '2020-05-15 03:10:51'),
(99, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 52, '2020-05-15 03:10:53', '2020-05-15 03:10:53'),
(100, 'Color', 'Black/White', 52, '2020-05-15 03:10:53', '2020-05-15 03:10:53'),
(101, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 53, '2020-05-15 03:10:54', '2020-05-15 03:10:54'),
(102, 'Color', 'Black/White', 53, '2020-05-15 03:10:54', '2020-05-15 03:10:54'),
(103, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 54, '2020-05-15 03:10:55', '2020-05-15 03:10:55'),
(104, 'Color', 'Black/White', 54, '2020-05-15 03:10:56', '2020-05-15 03:10:56'),
(105, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 55, '2020-05-15 03:10:57', '2020-05-15 03:10:57'),
(106, 'Color', 'Black/White', 55, '2020-05-15 03:10:57', '2020-05-15 03:10:57'),
(107, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 56, '2020-05-15 03:10:58', '2020-05-15 03:10:58'),
(108, 'Color', 'Black/White', 56, '2020-05-15 03:10:58', '2020-05-15 03:10:58'),
(109, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 57, '2020-05-15 03:11:00', '2020-05-15 03:11:00'),
(110, 'Color', 'Black/White', 57, '2020-05-15 03:11:00', '2020-05-15 03:11:00'),
(111, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 58, '2020-05-15 03:11:02', '2020-05-15 03:11:02'),
(112, 'Color', 'Black/White', 58, '2020-05-15 03:11:02', '2020-05-15 03:11:02'),
(113, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 59, '2020-05-15 03:11:03', '2020-05-15 03:11:03'),
(114, 'Color', 'Black/White', 59, '2020-05-15 03:11:04', '2020-05-15 03:11:04'),
(115, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 60, '2020-05-15 03:11:04', '2020-05-15 03:11:04'),
(116, 'Color', 'Black/White', 60, '2020-05-15 03:11:04', '2020-05-15 03:11:04'),
(117, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 61, '2020-05-15 03:11:05', '2020-05-15 03:11:05'),
(118, 'Color', 'Black/White', 61, '2020-05-15 03:11:05', '2020-05-15 03:11:05'),
(119, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 62, '2020-05-15 03:11:06', '2020-05-15 03:11:06'),
(120, 'Color', 'Black/White', 62, '2020-05-15 03:11:07', '2020-05-15 03:11:07'),
(121, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 63, '2020-05-15 03:11:08', '2020-05-15 03:11:08'),
(122, 'Color', 'Black/White', 63, '2020-05-15 03:11:08', '2020-05-15 03:11:08'),
(123, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 64, '2020-05-15 03:11:10', '2020-05-15 03:11:10'),
(124, 'Color', 'Black/White', 64, '2020-05-15 03:11:10', '2020-05-15 03:11:10'),
(125, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 65, '2020-05-15 03:11:11', '2020-05-15 03:11:11'),
(126, 'Color', 'Black/White', 65, '2020-05-15 03:11:12', '2020-05-15 03:11:12'),
(127, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 66, '2020-05-15 03:11:13', '2020-05-15 03:11:13'),
(128, 'Color', 'Black/White', 66, '2020-05-15 03:11:13', '2020-05-15 03:11:13'),
(129, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 67, '2020-05-15 03:11:45', '2020-05-15 03:11:45'),
(130, 'Color', 'Black/White', 67, '2020-05-15 03:11:45', '2020-05-15 03:11:45'),
(131, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 68, '2020-05-15 03:11:47', '2020-05-15 03:11:47'),
(132, 'Color', 'Black/White', 68, '2020-05-15 03:11:47', '2020-05-15 03:11:47'),
(133, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 69, '2020-05-15 03:11:49', '2020-05-15 03:11:49'),
(134, 'Color', 'Black/White', 69, '2020-05-15 03:11:49', '2020-05-15 03:11:49'),
(135, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 70, '2020-05-15 03:11:51', '2020-05-15 03:11:51'),
(136, 'Color', 'Black/White', 70, '2020-05-15 03:11:52', '2020-05-15 03:11:52'),
(137, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 71, '2020-05-15 03:11:53', '2020-05-15 03:11:53'),
(138, 'Color', 'Black/White', 71, '2020-05-15 03:11:54', '2020-05-15 03:11:54'),
(139, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 72, '2020-05-15 03:11:55', '2020-05-15 03:11:55'),
(140, 'Color', 'Black/White', 72, '2020-05-15 03:11:55', '2020-05-15 03:11:55'),
(141, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 73, '2020-05-15 03:11:57', '2020-05-15 03:11:57'),
(142, 'Color', 'Black/White', 73, '2020-05-15 03:11:57', '2020-05-15 03:11:57'),
(143, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 74, '2020-05-15 03:12:00', '2020-05-15 03:12:00'),
(144, 'Color', 'Black/White', 74, '2020-05-15 03:12:00', '2020-05-15 03:12:00'),
(145, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 75, '2020-05-15 03:12:10', '2020-05-15 03:12:10'),
(146, 'Color', 'Black/White', 75, '2020-05-15 03:12:11', '2020-05-15 03:12:11'),
(147, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 76, '2020-05-15 03:12:12', '2020-05-15 03:12:12'),
(148, 'Color', 'Black/White', 76, '2020-05-15 03:12:12', '2020-05-15 03:12:12'),
(149, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 77, '2020-05-15 03:12:14', '2020-05-15 03:12:14'),
(150, 'Color', 'Black/White', 77, '2020-05-15 03:12:15', '2020-05-15 03:12:15'),
(151, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 78, '2020-05-15 03:12:16', '2020-05-15 03:12:16'),
(152, 'Color', 'Black/White', 78, '2020-05-15 03:12:16', '2020-05-15 03:12:16'),
(153, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 79, '2020-05-15 03:12:18', '2020-05-15 03:12:18'),
(154, 'Color', 'Black/White', 79, '2020-05-15 03:12:18', '2020-05-15 03:12:18'),
(155, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 80, '2020-05-15 03:12:20', '2020-05-15 03:12:20'),
(156, 'Color', 'Black/White', 80, '2020-05-15 03:12:20', '2020-05-15 03:12:20'),
(157, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 81, '2020-05-15 03:12:22', '2020-05-15 03:12:22'),
(158, 'Color', 'Black/White', 81, '2020-05-15 03:12:22', '2020-05-15 03:12:22'),
(159, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 82, '2020-05-15 03:12:23', '2020-05-15 03:12:23'),
(160, 'Color', 'Black/White', 82, '2020-05-15 03:12:24', '2020-05-15 03:12:24'),
(161, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 83, '2020-05-15 03:12:26', '2020-05-15 03:12:26'),
(162, 'Color', 'Black/White', 83, '2020-05-15 03:12:26', '2020-05-15 03:12:26'),
(163, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 84, '2020-05-15 03:12:27', '2020-05-15 03:12:27'),
(164, 'Color', 'Black/White', 84, '2020-05-15 03:12:27', '2020-05-15 03:12:27'),
(165, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 85, '2020-05-15 03:12:28', '2020-05-15 03:12:28'),
(166, 'Color', 'Black/White', 85, '2020-05-15 03:12:28', '2020-05-15 03:12:28'),
(167, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 86, '2020-05-15 03:12:30', '2020-05-15 03:12:30'),
(168, 'Color', 'Black/White', 86, '2020-05-15 03:12:30', '2020-05-15 03:12:30'),
(169, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 87, '2020-05-15 03:12:31', '2020-05-15 03:12:31'),
(170, 'Color', 'Black/White', 87, '2020-05-15 03:12:31', '2020-05-15 03:12:31'),
(171, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 88, '2020-05-15 03:12:31', '2020-05-15 03:12:31'),
(172, 'Color', 'Black/White', 88, '2020-05-15 03:12:31', '2020-05-15 03:12:31'),
(173, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 89, '2020-05-15 03:12:32', '2020-05-15 03:12:32'),
(174, 'Color', 'Black/White', 89, '2020-05-15 03:12:32', '2020-05-15 03:12:32'),
(175, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 90, '2020-05-15 03:12:33', '2020-05-15 03:12:33'),
(176, 'Color', 'Black/White', 90, '2020-05-15 03:12:33', '2020-05-15 03:12:33'),
(177, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 91, '2020-05-15 03:12:34', '2020-05-15 03:12:34'),
(178, 'Color', 'Black/White', 91, '2020-05-15 03:12:34', '2020-05-15 03:12:34'),
(179, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 92, '2020-05-15 03:12:35', '2020-05-15 03:12:35'),
(180, 'Color', 'Black/White', 92, '2020-05-15 03:12:35', '2020-05-15 03:12:35'),
(181, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 93, '2020-05-15 03:12:36', '2020-05-15 03:12:36'),
(182, 'Color', 'Black/White', 93, '2020-05-15 03:12:36', '2020-05-15 03:12:36'),
(183, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 94, '2020-05-15 03:12:38', '2020-05-15 03:12:38'),
(184, 'Color', 'Black/White', 94, '2020-05-15 03:12:38', '2020-05-15 03:12:38'),
(185, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 95, '2020-05-15 03:12:42', '2020-05-15 03:12:42'),
(186, 'Color', 'Black/White', 95, '2020-05-15 03:12:42', '2020-05-15 03:12:42'),
(187, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 96, '2020-05-15 03:12:44', '2020-05-15 03:12:44'),
(188, 'Color', 'Black/White', 96, '2020-05-15 03:12:44', '2020-05-15 03:12:44'),
(189, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 97, '2020-05-15 03:12:46', '2020-05-15 03:12:46'),
(190, 'Color', 'Black/White', 97, '2020-05-15 03:12:47', '2020-05-15 03:12:47'),
(191, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 98, '2020-05-15 03:12:47', '2020-05-15 03:12:47'),
(192, 'Color', 'Black/White', 98, '2020-05-15 03:12:48', '2020-05-15 03:12:48'),
(193, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 99, '2020-05-15 03:12:49', '2020-05-15 03:12:49'),
(194, 'Color', 'Black/White', 99, '2020-05-15 03:12:50', '2020-05-15 03:12:50'),
(195, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 100, '2020-05-15 03:12:51', '2020-05-15 03:12:51'),
(196, 'Color', 'Black/White', 100, '2020-05-15 03:12:51', '2020-05-15 03:12:51'),
(197, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 101, '2020-05-15 03:12:53', '2020-05-15 03:12:53'),
(198, 'Color', 'Black/White', 101, '2020-05-15 03:12:54', '2020-05-15 03:12:54'),
(199, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 102, '2020-05-15 03:12:55', '2020-05-15 03:12:55'),
(200, 'Color', 'Black/White', 102, '2020-05-15 03:12:55', '2020-05-15 03:12:55'),
(201, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 103, '2020-05-15 03:12:58', '2020-05-15 03:12:58'),
(202, 'Color', 'Black/White', 103, '2020-05-15 03:12:58', '2020-05-15 03:12:58'),
(203, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 104, '2020-05-15 03:13:01', '2020-05-15 03:13:01'),
(204, 'Color', 'Black/White', 104, '2020-05-15 03:13:02', '2020-05-15 03:13:02'),
(205, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 105, '2020-05-15 03:13:02', '2020-05-15 03:13:02'),
(206, 'Color', 'Black/White', 105, '2020-05-15 03:13:03', '2020-05-15 03:13:03'),
(207, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 106, '2020-05-15 03:13:04', '2020-05-15 03:13:04'),
(208, 'Color', 'Black/White', 106, '2020-05-15 03:13:04', '2020-05-15 03:13:04'),
(209, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 107, '2020-05-15 03:13:06', '2020-05-15 03:13:06'),
(210, 'Color', 'Black/White', 107, '2020-05-15 03:13:06', '2020-05-15 03:13:06'),
(211, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 108, '2020-05-15 03:13:08', '2020-05-15 03:13:08'),
(212, 'Color', 'Black/White', 108, '2020-05-15 03:13:09', '2020-05-15 03:13:09'),
(213, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 109, '2020-05-15 03:13:11', '2020-05-15 03:13:11'),
(214, 'Color', 'Black/White', 109, '2020-05-15 03:13:11', '2020-05-15 03:13:11'),
(215, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 110, '2020-05-15 03:13:13', '2020-05-15 03:13:13'),
(216, 'Color', 'Black/White', 110, '2020-05-15 03:13:13', '2020-05-15 03:13:13'),
(217, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 111, '2020-05-15 03:13:14', '2020-05-15 03:13:14'),
(218, 'Color', 'Black/White', 111, '2020-05-15 03:13:14', '2020-05-15 03:13:14'),
(219, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 112, '2020-05-15 03:13:16', '2020-05-15 03:13:16'),
(220, 'Color', 'Black/White', 112, '2020-05-15 03:13:17', '2020-05-15 03:13:17'),
(221, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 113, '2020-05-15 03:13:18', '2020-05-15 03:13:18'),
(222, 'Color', 'Black/White', 113, '2020-05-15 03:13:18', '2020-05-15 03:13:18'),
(223, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 114, '2020-05-15 03:13:19', '2020-05-15 03:13:19'),
(224, 'Color', 'Black/White', 114, '2020-05-15 03:13:20', '2020-05-15 03:13:20'),
(225, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 115, '2020-05-15 03:15:29', '2020-05-15 03:15:29'),
(226, 'Color', 'Black/White', 115, '2020-05-15 03:15:29', '2020-05-15 03:15:29'),
(227, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 116, '2020-05-17 21:02:51', '2020-05-17 21:02:51'),
(228, 'Color', 'Black/White', 116, '2020-05-17 21:02:52', '2020-05-17 21:02:52'),
(229, 'Size', 'Size 5.5US - Size 38.0VN-24.5CM', 124, '2020-05-17 21:24:58', '2020-05-17 21:24:58'),
(230, 'Color', 'Black/White', 124, '2020-05-17 21:24:58', '2020-05-17 21:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `productorders`
--

CREATE TABLE `productorders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productorders`
--

INSERT INTO `productorders` (`id`, `name`, `code`, `price`, `quantity`, `img`, `order_id`) VALUES
(1, 'converse-blue-cao-co', 'SKU: 127440C', '1275000', '2', 'converse-blue.png', 43),
(2, 'converse-blue-co-thap', 'SKU: 126196C', '1260000', '3', 'converse-blue-co-thap.png', 43);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promotion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_sale` datetime DEFAULT NULL,
  `end_sale` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `code`, `slug`, `price`, `promotion`, `featured`, `status`, `description`, `img`, `category_id`, `created_at`, `updated_at`, `start_sale`, `end_sale`) VALUES
(9, 'vans old skool pig suede', 'SKU: VN0A4BV5V77', 'vans-old-skool-pig-suede', '2200000', '0', '1', '1', '<p><em><strong>Vans Old Skool Pig Suede</strong></em>&nbsp;c&ugrave;ng phối m&agrave;u v&agrave;ng hoa hướng dương tạo điểm nhấn đặc biệt cho đ&ocirc;i gi&agrave;y</p>', 'vans-old-skool-pig-suede.png', 7, NULL, '2020-05-21 02:37:21', NULL, NULL),
(10, 'vans old skool 36 dx anaheim factory', 'SKU: VN0A38G2PXC', 'vans-old-skool-36-dx-anaheim-factory', '2200000', '0', '1', '1', '<p>Kiểu d&aacute;ng Old Skool cổ điển với l&oacute;t&nbsp;gi&agrave;y được n&acirc;ng cấp c&ocirc;ng nghệ Đệm l&oacute;t UltraCush mang đến một cảm nhận kh&aacute;c biệt với d&ograve;ng gi&agrave;y cao cấp n&agrave;y của nh&agrave; Vans mang lại sự thoải m&aacute;i &amp; &ecirc;m &aacute;i cho đ&ocirc;i ch&acirc;n. Anaheim Factory 36DX Vintage với chất liệu kết hợp giữa Suede v&agrave; Canvas. Đặc biệt t&ocirc;ng đen classic&nbsp;được nhiều người t&igrave;m kiếm với khả năng phối đồ cực đỉnh. Đệm l&oacute;t UltraCush mang đến một cảm nhận kh&aacute;c biệt với d&ograve;ng gi&agrave;y cao cấp n&agrave;y của nh&agrave; Vans</p>', 'vans-old-skool-36-dx-anaheim-factory.jpg', 7, NULL, '2020-05-21 02:34:49', NULL, NULL),
(11, 'Palladium Blanc Hi - 72886-097', 'SKU: 72886-421', 'palladium-blanc-hi-72886-097', '1600000', '0', '1', '1', '<p>Tone xanh thời thượng gi&uacute;p bạn dễ d&agrave;ng mix&amp;match với những bộ outfit y&ecirc;u th&iacute;ch. Thiết kế lớp cao su phần mũi gi&agrave;y gi&uacute;p bảo vệ c&aacute;c ng&oacute;n ch&acirc;n, giảm t&aacute;c động b&ecirc;n ngo&agrave;i. Chất liệu vải Textile cao cấp tạo cảm gi&aacute;c thoải m&aacute;i khi di chuyển.</p>', 'palladium-blanc-hi-72886-097.jpg', 9, NULL, '2020-05-12 23:42:40', NULL, NULL),
(12, 'Palladium Blanc Hi - 72886-421', 'SKU: 72886-097', 'palladium-blanc-hi-72886-421', '1600000', '0', '1', '1', '<p>Palladium Blanc Hi với thiết kế đơn giản c&ugrave;ng tone m&agrave;u x&aacute;m c&aacute; t&iacute;nh, logo phần lưỡi g&agrave; được may gia c&ocirc;ng chi tiết đến từng đường kim mũi chỉ. Phần th&acirc;n gi&agrave;y sử dụng chất liệu vải Textile cao cấp khiến bạn cảm thấy thoải m&aacute;i trong từng bước đi.</p>', 'palladium-blanc-hi-72886-421.jpg', 9, NULL, '2020-05-12 23:42:25', NULL, NULL),
(14, 'Converse Chuck Taylor All Star VLTG - Back to Earth', 'SKU: 567046V', 'converse-chuck-taylor-all-star-vltg-back-to-earth', '1600000', '15', '0', '1', '<p>Mẫu gi&agrave;y cao cổ Chuck Taylor All Star VLTG gi&uacute;p cho người sử dụng c&oacute; vẻ ngo&agrave;i si&ecirc;u chất v&agrave; c&aacute; t&iacute;nh. Điểm nhấn nổi bật kh&aacute;c của sản phẩm ngo&agrave;i họa tiết chữ V xếp chồng đ&oacute; ch&iacute;nh l&agrave; &nbsp;icon h&igrave;nh ng&ocirc;i sao được c&aacute;ch điệu ở phần foxing tạo điểm nhấn v&agrave; sự kh&aacute;c biệt so với c&aacute;c sản phẩm kh&aacute;c.</p>', 'converse-chuck-taylor-all-star-vltg-back-to-earth.png', 11, NULL, '2020-05-19 17:05:05', NULL, NULL),
(15, 'vans slip on anaheim factory checkerboard', 'SKU: VN0A3JEXPU', 'vans-slip-on-anaheim-factory-checkerboard', '1900000', '10', '0', '1', '<p>TH&Ocirc;NG TIN SẢN PHẨM Thương hiệu Vans Xuất xứ thương hiệu Mỹ Sản xuất tại Việt Nam/ Ấn...</p>', 'vans-slip-on-anaheim-factory-checkerboard.png', 8, NULL, '2020-05-21 02:42:01', NULL, NULL),
(16, 'vans sk8 hi black white', 'SKU: VN000D5IB8C', 'vans-sk8-hi-black-white', '1800000', '70', '0', '1', '<p>Sk8 l&agrave; d&ograve;ng được ưa chuộng của Vans.&nbsp;</p>\r\n\r\n<p>Si&ecirc;u năng động, c&aacute; t&iacute;nh với mẫu cao cổ. Sắc đen - trắng cổ điển lu&ocirc;n thời thượng, kh&ocirc;ng bao giờ lỗi mốt.&nbsp;</p>', 'vans-sk8-hi-black-white.png', 12, NULL, '2020-05-19 08:56:13', NULL, NULL),
(17, 'palladium pampa hi shake - 96637-665-m', 'SKU: 96637-665-M', 'palladium-pampa-hi-shake-96637-665-m', '2200000', '15', '0', '1', '<p>Gợi &yacute; cho c&aacute;c bạn nữ đ&ocirc;i gi&agrave;y Palla Pampa Hi Shake &quot;ngọt như kẹo&quot; với t&ocirc;ng m&agrave;u hồng nữ t&iacute;nh v&agrave; c&aacute;c chi tiết trang tr&iacute; với h&igrave;nh ảnh của Ice-cream, Cherry, Milk shake, ... Khuấy động kh&ocirc;ng kh&iacute; m&ugrave;a h&egrave; năm nay với item &quot;so cute&quot; như thế n&agrave;y nh&eacute;!</p>', 'palladium-pampa-hi-shake-96637-665-m.jpg', 10, NULL, '2020-05-12 23:43:08', NULL, NULL),
(34, 'chuck taylor all star classic 121176', 'SKU: 121176', 'chuck-taylor-all-star-classic-121176', '1400000', '10', '0', '1', '<p><em><strong>gi&agrave;y Converse classic</strong></em>&nbsp;thấp cổ với thiết kế cổ điển được ưa chuộng qua bao thế hệ&nbsp;đi k&egrave;m với chất liệu vải Canvas c&ugrave;ng với phần đế&nbsp;cao su bền chắc c&oacute; đường viền đỏ&nbsp;- xanh v&ocirc; c&ugrave;ng nổi bật. Phi&ecirc;n bản m&agrave;u trắng của&nbsp;Converse Classic cổ thấp chắc chắn l&agrave; item đơn giản ph&ugrave; hợp với c&aacute;c bạn trẻ. Mang lại sự c&aacute; t&iacute;nh, năng động, trẻ trung</p>', 'chuck-taylor-all-star-classic-121176.png', 5, '2020-05-12 21:29:00', '2020-05-19 17:01:11', NULL, NULL),
(35, 'chuck taylor all star 1970s hoop hunter 165912c', 'SKU: 165912C', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c', '2000000', '0', '1', '1', '<p>Chất liệu vải dệt Ripstop Poly si&ecirc;u bền với c&aacute;c phi&ecirc;n bản Camo được mix c&oacute; t&ocirc;ng m&agrave;u Xanh Olive chủ đạo. Kiểu d&aacute;ng Chuck 70s mạnh mẽ với đế gi&agrave;y đen gi&uacute;p bạn c&oacute; được một item tr&ocirc;ng ngầu hơn khi kết hợp với c&aacute;c outfit.</p>', 'chuck-taylor-all-star-1970s-hoop-hunter-165912c.png', 6, '2020-05-12 21:36:09', '2020-05-19 08:38:41', NULL, NULL),
(36, 'converse chuck taylor all star classic black white', 'SKU: 121186', 'converse-chuck-taylor-all-star-classic-black-white', '1500000', '0', '1', '1', '<p>Nhắc đến t&ecirc;n d&ograve;ng sản phẩm n&agrave;y -&nbsp;<em><strong>gi&agrave;y&nbsp;Converse classic</strong></em>&nbsp;đ&atilde; đủ để thấy được sự basic của n&oacute;. V&agrave; BST Chuck Taylor All Star Classic đ&atilde; chứng tỏ được vị thế của những đ&ocirc;i gi&agrave;y chất lượng - đơn giản - gi&aacute; cả phải chăng của m&igrave;nh khi n&oacute; lọt top những sản phẩm đ&aacute;ng sở hữu nhất m&agrave; ai cũng n&ecirc;n c&oacute; một đ&ocirc;i</p>', 'converse-chuck-taylor-all-star-classic-black-white.jpg', 5, '2020-05-12 23:33:17', '2020-05-12 23:34:24', NULL, NULL),
(37, 'converse chuck taylor all star 1970s flight school leather silver hi', 'SKU: 165050C', 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi', '2100000', '30', '0', '1', '<p>Lần n&agrave;y, với&nbsp;<em><strong>Converse &quot;Flight School&quot;</strong></em>, h&atilde;ng tiếp tục remix h&igrave;nh b&oacute;ng ng&ocirc;i sao của m&igrave;nh với một bộ đồ đ&ocirc;i h&agrave;ng kh&ocirc;ng , được lấy cảm hứng từ gi&agrave;y da.&nbsp;<em><strong>Converse &quot;Flight School&quot;</strong></em>&nbsp;được&nbsp;reworks phong&nbsp;c&aacute;ch chữ k&yacute; với da s&aacute;ng b&oacute;ng theo phong c&aacute;ch của &aacute;o kho&aacute;c bomber v&agrave; spacesuits.</p>', 'converse-chuck-taylor-all-star-1970s-flight-school-leather-silver-hi.jpg', 6, '2020-05-19 16:18:05', '2020-05-19 16:18:05', NULL, NULL),
(38, 'converse chuck 70 breaking down barriers knicks', 'SKU: 166815C', 'converse-chuck-70-breaking-down-barriers-knicks', '2200000', '10', '0', '1', '<p><strong><em>Gi&agrave;y&nbsp;Converse</em></strong>&nbsp;mang dấu ấn của huyền thoại Nat &quot;Sweetwater&quot; Clifton, người đ&atilde; trở th&agrave;nh cầu thủ người Mỹ gốc Phi đầu ti&ecirc;n k&yacute; hợp đồng với New York Knickerbockers v&agrave; gi&uacute;p Knicks lọt v&agrave;o Chung kết NBA ngay trong năm t&acirc;n binh của anh ấy. Sản phẩm được thiết kế theo t&ocirc;ng m&agrave;u xanh đầy nổi bật,&nbsp; lấy cảm hứng từ New York Knicks</p>', 'converse-chuck-70-breaking-down-barriers-knicks.jpg', 6, '2020-05-19 16:31:10', '2020-05-19 16:31:10', NULL, NULL),
(39, 'converse chuck taylor all star classic navy', 'SKU: 127440C', 'converse-chuck-taylor-all-star-classic-navy', '1500000', '15', '0', '1', '<p><em><strong>gi&agrave;y&nbsp;</strong></em><em><strong>Converse classic</strong></em>&nbsp;l&agrave; d&ograve;ng b&aacute;n chạy số 1 của Converse. Với 6 sắc m&agrave;u cơ bản th&igrave; đen trắng l&agrave; m&agrave;u dễ phối được nhiều bạn tin chọn. Đ&ocirc;i gi&agrave;y m&agrave; ai cũng n&ecirc;n c&oacute; v&igrave; độ bền, độ đẹp v&agrave; si&ecirc;u dễ phối đồ, hợp với tất cả thể loại trang phục</p>', 'converse-chuck-taylor-all-star-classic-navy.png', 5, '2020-05-20 04:59:48', '2020-05-20 05:00:50', NULL, NULL),
(41, 'converse chuck taylor all star cheerful', 'SKU: 167070C', 'converse-chuck-taylor-all-star-cheerful', '1500000', '0', '1', '1', '<p>Với sự trở lại của sản phẩm&nbsp;<em><strong>gi&agrave;y Converse</strong></em>&nbsp;Chuck Taylor All Star Cheerful&nbsp;đầu năm 2020, c&aacute;c bạn c&oacute; thể nhận ra rằng một sản phẩm &ldquo;b&igrave;nh mới rượu cũ&rdquo; kh&ocirc;ng hẳn l&agrave; k&eacute;m chất lượng. Minh chứng cho thấy, chỉ thay đổi một v&agrave;i chi tiết nhấn nh&aacute; đặc biệt l&agrave; gắn v&agrave;o đ&oacute; những c&acirc;u chuyện độc đ&aacute;o thu h&uacute;t người d&ugrave;ng l&agrave; sản phẩm đ&oacute; của Converse ngay lập tức sẽ trở th&agrave;nh xu hướng</p>', 'converse-chuck-taylor-all-star-cheerful.jpg', 11, '2020-05-20 07:27:21', '2020-05-26 06:13:40', NULL, NULL),
(42, 'converse chuck taylor all star classic cream white hi', 'SKU: 121185', 'converse-chuck-taylor-all-star-classic-cream-white-hi', '1500000', '10', '0', '1', '<p><em><strong>gi&agrave;y&nbsp;</strong></em><em><strong>Converse classic</strong></em>&nbsp;c&oacute; m&agrave;u sắc đơn giản nhưng kh&ocirc;ng g&acirc;y ra sự đơn điệu, c&aacute;c t&ocirc;ng m&agrave;u từ đen, đỏ, trắng, be, hồng&hellip; đều kh&ocirc;ng qu&aacute; lố m&agrave; với t&ocirc;ng cực trẻ trung, hợp thời trang v&agrave; hợp với nhiều phong c&aacute;ch thời trang kh&aacute;c nhau. D&ugrave; bạn l&agrave; những người c&aacute; t&iacute;nh hay những người c&oacute; phong c&aacute;ch thời trang đơn giản, tinh tế đề sử dụng được.</p>', 'converse-chuck-taylor-all-star-classic-cream-white-hi.jpg', 5, '2020-05-22 06:31:21', '2020-05-22 06:31:21', NULL, '2020-06-06 07:30:08'),
(43, 'converse chuck taylor all star 1970s sunflower hi', 'SKU: 162054C', 'converse-chuck-taylor-all-star-1970s-sunflower-hi', '2000000', '10', '0', '1', '<p><em><strong>Converse 1970s</strong></em>&nbsp;l&agrave; 1 trong những d&ograve;ng sản phẩm b&aacute;n chạy nhất của Converse.<br />\r\n<strong>Sunflower</strong>&nbsp;l&agrave; một trong những phối m&agrave;u hot nhất của d&ograve;ng&nbsp;<em><strong>Converse 1970s</strong></em>, rất đẹp v&agrave; dễ phối đồ, đồng thời c&oacute; 2 bản l&agrave; cao cổ v&agrave; thấp cổ,</p>', 'converse-chuck-taylor-all-star-1970s-sunflower-hi.png', 6, '2020-05-26 09:25:19', '2020-05-26 09:25:19', '2020-05-28 00:00:00', '2020-06-12 00:00:00'),
(44, 'dsada', 'SP09', 'dsada', '444', '10', '1', '1', '<p>dasdsa</p>', 'dsada.jpg', 5, '2020-06-03 07:14:22', '2020-06-03 07:14:22', NULL, NULL),
(45, 'converse chuck 70 logo play', 'SKU: 166747C', 'converse-chuck-70-logo-play', '2000000', '0', '1', '1', '<p>TH&Ocirc;NG TIN SẢN PHẨM Thương hiệu Converse Xuất xứ thương hiệu Mỹ Sản xuất tại Việt Nam/ Ấn...</p>', 'converse-chuck-70-logo-play.jpg', 5, '2020-06-03 07:22:01', '2020-06-03 07:22:01', NULL, NULL),
(46, 'converse123', 'SKU: 12118555', 'converse123', '1500000', '0', '1', '1', '<p>san pham converse</p>', 'converse123.jpg', 6, '2020-06-03 08:11:53', '2020-06-03 08:12:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `parent` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `password`, `slug`, `img`, `phone`, `address`, `level`, `parent`) VALUES
(19, 'nguyen thu thao', 'thaont97', 'thao@gmail.com', '$2y$10$iqVvV5nlTgQyIcmIXfs8/.zgjlTGIJvSm5yXLKaBS41SBoAO.qq9G', 'nguyen-thu-thao', 'nguyen-thu-thao.jpg', '0962484652', 'hanoi', 2, NULL),
(22, 'ngo phuong linh', 'linhnp97', 'linh@gmail.com', '$2y$10$FX/XXNj0TFCuIrN.FUcgw.E8BQkDj.Sxhx233v0yfrJd6DtIjhAdO', 'ngo-phuong-linh', 'ngo-phuong-linh.jpg', '0167427167', 'hanoi', 1, NULL),
(26, 'Nguyen Thu Thao', 'thaont97', 'thaont@gmail.com', '$2y$10$Z0sYuQyDSoUlLwbyDb3X8O9alSvK3EZj8lmaA28awxwQ2IeLuFXjq', 'nguyen-thu-thao', 'nguyen-thu-thao.jpg', '0399888567', 'hanoi', 1, NULL),
(35, 'imraver', 'imraver', 'imraver@gmail.com', '$2y$10$hTytCnYEHsUnPxTb4PAj9OVE4POSW1CR88vbOYoTkrOrmsFbG4UVO', 'imraver', 'imraver.jpg', '0344556665', 'ha noi, ho tay', 1, NULL),
(36, 'nguyen viet ha raver', 'hanv97', 'ha@gmail.com', '$2y$10$RoCrgBAnYodU9d7Ia1C00.2.I76wuvz6g9XMVCfmA6ebQJjPxEP06', 'nguyen-viet-ha-raver', 'nguyen-viet-ha-raver.jpg', '0399881175', 'ha dong ha noi', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `values_attribute`
--

CREATE TABLE `values_attribute` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `values_attribute`
--

INSERT INTO `values_attribute` (`id`, `value`, `attribute_id`) VALUES
(1, 'Size 5.5US - Size 38.0VN-24.5CM', 1),
(2, 'Size 6.0US - Size 39.0VN-24.5CM', 1),
(3, 'Size 7.0US - Size 40.0VN-25.5CM', 1),
(4, 'Size 7.5US - Size 41.0VN-26.0CM', 1),
(5, 'Size 8.5US - Size 42.0VN-27.0CM', 1),
(6, 'Size 9.5US - Size 43.0VN-28.0CM', 1),
(7, 'Size 10 US - Size 44.0VN-28.5CM', 1),
(8, 'Black/White', 2),
(9, 'White', 2),
(10, 'Red', 2),
(11, 'Black', 2);

-- --------------------------------------------------------

--
-- Table structure for table `values_product`
--

CREATE TABLE `values_product` (
  `value_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `values_product`
--

INSERT INTO `values_product` (`value_id`, `product_id`) VALUES
(1, 34),
(2, 34),
(3, 34),
(4, 34),
(5, 34),
(6, 34),
(7, 34),
(9, 34),
(1, 36),
(2, 36),
(3, 36),
(4, 36),
(5, 36),
(6, 36),
(7, 36),
(8, 36),
(1, 44),
(2, 44),
(8, 44),
(9, 44),
(1, 45),
(2, 45),
(3, 45),
(4, 45),
(5, 45),
(6, 45),
(7, 45),
(10, 45),
(1, 46),
(2, 46),
(3, 46),
(9, 46);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_orders`
--
ALTER TABLE `customer_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_orders_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `cutomers`
--
ALTER TABLE `cutomers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_product_id_foreign` (`product_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_attrs`
--
ALTER TABLE `order_attrs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_attrs_customerorder_id_foreign` (`customerorder_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `productorders`
--
ALTER TABLE `productorders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productorders_order_id_foreign` (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `values_attribute`
--
ALTER TABLE `values_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `values_attribute_attribute_id_foreign` (`attribute_id`);

--
-- Indexes for table `values_product`
--
ALTER TABLE `values_product`
  ADD KEY `values_product_value_id_foreign` (`value_id`),
  ADD KEY `values_product_product_id_foreign` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `customer_orders`
--
ALTER TABLE `customer_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;

--
-- AUTO_INCREMENT for table `cutomers`
--
ALTER TABLE `cutomers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `order_attrs`
--
ALTER TABLE `order_attrs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT for table `productorders`
--
ALTER TABLE `productorders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `values_attribute`
--
ALTER TABLE `values_attribute`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_orders`
--
ALTER TABLE `customer_orders`
  ADD CONSTRAINT `customer_orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `cutomers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_attrs`
--
ALTER TABLE `order_attrs`
  ADD CONSTRAINT `order_attrs_customerorder_id_foreign` FOREIGN KEY (`customerorder_id`) REFERENCES `customer_orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productorders`
--
ALTER TABLE `productorders`
  ADD CONSTRAINT `productorders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `values_attribute`
--
ALTER TABLE `values_attribute`
  ADD CONSTRAINT `values_attribute_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `values_product`
--
ALTER TABLE `values_product`
  ADD CONSTRAINT `values_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `values_product_value_id_foreign` FOREIGN KEY (`value_id`) REFERENCES `values_attribute` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
